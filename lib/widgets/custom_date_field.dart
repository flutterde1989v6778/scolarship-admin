import 'package:flutter/material.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class CustomDateField extends StatelessWidget {
  CustomDateField({
    @required this.width,
    @required this.height,
    this.fixedHeightState = false,
    this.onChanged,
    this.onSaved,
    this.validateHandler,
    this.initValue,
    this.textFontSize = 20,
    this.textColor = Colors.black,
    this.readOnly = false,
    this.hintText = "",
    this.hintTextColor = Colors.grey,
    this.hintTextFontSize = 20,
    this.borderType = 1,
    this.borderWidth = 1,
    this.borderRadius = 5,
    this.borderColor = Colors.black,
    this.enableBorderColor,
    this.errorBoarderColor = Colors.red,
    this.disabledBorderColor = Colors.grey,
    this.focusBorderColor,
    this.contentPadding = 5,
    this.fillColor = Colors.white,
  });
  final bool fixedHeightState;
  final double width;
  final double height;
  final Function onChanged;
  final Function onSaved;
  final Function validateHandler;
  final format = DateFormat("yyyy-MM-dd");
  final DateTime initValue;
  final double textFontSize;
  final Color textColor;
  final bool readOnly;
  final int borderType;
  final double borderWidth;
  final double borderRadius;
  final Color borderColor;
  Color enableBorderColor;
  final Color errorBoarderColor;
  final Color disabledBorderColor;
  Color focusBorderColor;
  final String hintText;
  final double hintTextFontSize;
  final Color hintTextColor;
  final double contentPadding;
  final Color fillColor;

  @override
  Widget build(BuildContext context) {
    focusBorderColor = focusBorderColor ?? borderColor;
    enableBorderColor = enableBorderColor ?? borderColor;

    BorderSide borderside = BorderSide(width: borderWidth, color: borderColor);
    BorderSide errorBorderside = BorderSide(width: borderWidth, color: errorBoarderColor);
    BorderSide hideBorderSide = BorderSide(width: 0, color: borderColor);

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => CustomCustomDateFieldProvider()),
      ],
      child: Consumer<CustomCustomDateFieldProvider>(
        builder: (context, customDateFormFieldProvider, _) {
          return Container(
            width: width,
            height: height + ((fixedHeightState) ? textFontSize * 0.8 + 5 : 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: width,
                  height: height,
                  alignment: Alignment.center,
                  decoration: (borderType == 1)
                      ? BoxDecoration(
                          color: fillColor,
                          border: Border(bottom: (customDateFormFieldProvider.errorState) ? errorBorderside : borderside),
                        )
                      : BoxDecoration(
                          color: fillColor,
                          border: (customDateFormFieldProvider.errorState)
                              ? Border.all(width: borderWidth, color: errorBoarderColor)
                              : Border.all(width: borderWidth, color: borderColor),
                          borderRadius: BorderRadius.circular(borderRadius),
                        ),
                  child: DateTimeField(
                    style: TextStyle(fontSize: textFontSize, color: textColor),
                    initialValue: initValue,
                    format: format,
                    decoration: InputDecoration(
                      errorStyle: TextStyle(fontSize: 0, color: Colors.white),
                      hintText: hintText,
                      isDense: false,
                      hintStyle: TextStyle(fontSize: hintTextFontSize, color: hintTextColor),
                      border: UnderlineInputBorder(borderSide: hideBorderSide),
                      enabledBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                      focusedBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                      focusedErrorBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                      errorBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                      disabledBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                      contentPadding: EdgeInsets.symmetric(horizontal: contentPadding, vertical: (height - textFontSize) / 2),
                      fillColor: fillColor,
                    ),
                    onShowPicker: (context, currentValue) async {
                      return await showDatePicker(
                          context: context, firstDate: DateTime(1900), initialDate: currentValue ?? DateTime.now(), lastDate: DateTime(2100));
                    },
                    resetIcon: (!readOnly) ? Icon(Icons.delete, color: textColor) : null,
                    readOnly: readOnly,
                    enableInteractiveSelection: !readOnly,
                    enabled: !readOnly,
                    validator: (input) {
                      if (input == null) customDateFormFieldProvider.setErrorState(true, "Please enter available Date");
                      if (validateHandler != null) {
                        validateHandler((input == null) ? "Please enter available Date" : null);
                      }
                      return (input == null) ? "Please enter available Date" : null;
                    },
                    onChanged: (date) {
                      customDateFormFieldProvider.setErrorState(false, "");
                      if (!readOnly && onChanged != null) onChanged(DateTimeField.tryFormat(date, format));
                    },
                    onSaved: (date) {
                      if (onSaved != null) onSaved(DateTimeField.tryFormat(date, format));
                    },
                  ),
                ),
                (customDateFormFieldProvider.errorState)
                    ? Container(
                        height: textFontSize * 0.8 + 5,
                        alignment: Alignment.bottomLeft,
                        child: Text(
                          customDateFormFieldProvider.validatorText,
                          style: TextStyle(fontSize: textFontSize * 0.8, color: Colors.red),
                        ),
                      )
                    : (fixedHeightState) ? SizedBox(height: textFontSize * 0.8) : SizedBox(),
              ],
            ),
          );
        },
      ),
    );
  }
}

class CustomCustomDateFieldProvider extends ChangeNotifier {
  static CustomCustomDateFieldProvider of(BuildContext context, {bool listen = false}) =>
      Provider.of<CustomCustomDateFieldProvider>(context, listen: listen);

  bool _errorState = false;
  String _validatorText = "";

  bool get errorState => _errorState;
  String get validatorText => _validatorText;

  void setErrorState(bool errorState, String validatorText) {
    if (_errorState != errorState) {
      _errorState = errorState;
      _validatorText = validatorText;
      notifyListeners();
    }
  }
}
