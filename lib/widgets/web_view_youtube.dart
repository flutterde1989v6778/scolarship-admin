import "package:universal_html/html.dart";
import 'dart:ui' as ui;

import 'package:flutter/material.dart';

class WebViewYoutube extends StatefulWidget {
  final String youtubeUrl;
  final double size;
  final String element;

  WebViewYoutube({@required this.youtubeUrl, @required this.size, @required this.element});

  @override
  _WebViewYoutubeState createState() => _WebViewYoutubeState();
}

class _WebViewYoutubeState extends State<WebViewYoutube> {
  Widget iframeWidget;
  final IFrameElement iframeElement = new IFrameElement();

  @override
  void initState() {
    _initializeState();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _initializeState() {
    iframeElement.src = widget.youtubeUrl;
    iframeElement.width = '16';
    iframeElement.height = '9';
    iframeElement.style.border = 'none';
    iframeElement.allowFullscreen = true;

    // ignore: undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory(
      widget.element,
      (int viewId) => iframeElement,
    );

    iframeWidget = HtmlElementView(
      key: UniqueKey(),
      viewType: widget.element,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(width: widget.size, height: widget.size * 9 / 16, child: iframeWidget);
  }
}
