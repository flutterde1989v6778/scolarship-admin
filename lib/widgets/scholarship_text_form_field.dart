import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';

class ScholarshipTextFormField extends StatelessWidget {
  ScholarshipTextFormField({
    Key key,
    @required this.width,
    @required this.height,
    this.initialValue,
    this.controller,
    this.fixedHeightState = true,
    this.labelText = "",
    this.labelFontSize,
    this.labelColor = Colors.blueAccent,
    this.labelSpacing = 5,
    this.textFontSize = 20,
    this.textColor = Colors.black,
    this.hintText = "",
    this.hintTextFontSize,
    this.hintTextColor = Colors.grey,
    this.borderType = 1, // 1: UnderlineInputBorder 2: OutlineInputBorder
    this.borderWidth = 1,
    this.borderColor = Colors.black,
    this.borderRadius = 0.0,
    this.enableBorderColor,
    this.focusBorderColor,
    this.errorBoarderColor = Colors.red,
    this.disabledBorderColor = const Color(0x33ffffff),
    this.textAlign = TextAlign.left,
    this.keyboardType = TextInputType.text,
    this.fillColor = Colors.white,
    this.autovalidate = false,
    this.obscureText = false,
    this.autofocus = false,
    this.contentHorizontalPadding = 5,
    this.contentVerticalPadding = 5,
    this.maxLines = 1,
    this.textInputAction = TextInputAction.done,
    this.readOnly = false,
    this.errorStringFontSize,
    this.onChangeHandler,
    this.validatorHandler,
    this.onSaveHandler,
    this.onTapHandler,
    this.onFieldSubmittedHandler,
    this.focusNode,
    this.inputFormatters,
    this.validateIconColor = Colors.green,
    this.unValidateIconColor = Colors.green,
    this.iconSize,
    this.autoHeight = false,
    this.lineSpacing = 3,
    this.isRequired = true,
  }) : super(key: key);
  final double width;
  final double height;
  final String initialValue;
  final TextEditingController controller;
  final bool fixedHeightState;
  final String labelText;
  double labelFontSize;
  final Color labelColor;
  final double labelSpacing;
  final double textFontSize;
  final Color textColor;
  final String hintText;
  double hintTextFontSize;
  Color hintTextColor;
  final int borderType;
  final double borderWidth;
  final Color borderColor;
  final double borderRadius;
  Color enableBorderColor;
  Color focusBorderColor;
  Color errorBoarderColor;
  Color disabledBorderColor;
  final Color fillColor;
  final TextAlign textAlign;
  final TextInputType keyboardType;
  final bool autovalidate;
  final bool obscureText;
  final double contentHorizontalPadding;
  final double contentVerticalPadding;
  final bool autofocus;
  final int maxLines;
  final TextInputAction textInputAction;
  final bool readOnly;
  final inputFormatters;
  final Function onChangeHandler;
  final Function validatorHandler;
  final Function onSaveHandler;
  final Function onTapHandler;
  final Function onFieldSubmittedHandler;
  double errorStringFontSize;
  final focusNode;
  final Color validateIconColor;
  final Color unValidateIconColor;
  final double iconSize;
  final bool autoHeight;
  final double lineSpacing;
  GlobalKey globalkey = GlobalKey();

  final bool isRequired;
  void setHeight(ScholarshipTextFormFieldProvider scholarshipTextFormFieldProvider) async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      if (globalkey != null && globalkey.currentContext.size.height != scholarshipTextFormFieldProvider.height)
        scholarshipTextFormFieldProvider.setHeight(globalkey.currentContext.size.height / scholarshipTextFormFieldProvider.lines);
    });
  }

  @override
  Widget build(BuildContext context) {
    double fieldHeight;
    hintTextFontSize = hintTextFontSize ?? textFontSize;
    hintTextColor = hintTextColor ?? textColor;
    focusBorderColor = focusBorderColor ?? borderColor;
    enableBorderColor = enableBorderColor ?? borderColor;
    if (errorStringFontSize == null) errorStringFontSize = textFontSize * 0.8;
    BorderSide borderside = BorderSide(width: borderWidth, color: borderColor);
    BorderSide errorBorderside = BorderSide(width: borderWidth, color: errorBoarderColor);
    BorderSide hideBorderSide = BorderSide(width: 0, color: fillColor);
    // TextEditingController controller = TextEditingController();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ScholarshipTextFormFieldProvider(initialValue)),
      ],
      child: Consumer<ScholarshipTextFormFieldProvider>(
        builder: (context, scholarshipTextFormFieldProvider, _) {
          if (autoHeight) {
            if (scholarshipTextFormFieldProvider.height == null || scholarshipTextFormFieldProvider.textFontSize != textFontSize) {
              scholarshipTextFormFieldProvider.setTextfontSize(textFontSize);
              setHeight(scholarshipTextFormFieldProvider);
              fieldHeight = null;
            } else {
              fieldHeight = scholarshipTextFormFieldProvider.height * scholarshipTextFormFieldProvider.lines;
            }
          } else {
            fieldHeight = height;
          }

          // controller.text = scholarshipTextFormFieldProvider.initialValue;
          // controller.selection = TextSelection.fromPosition(TextPosition(offset: controller.text.length));

          return Container(
            width: width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    (scholarshipTextFormFieldProvider.validate != null && scholarshipTextFormFieldProvider.validate)
                        ? Icon(Icons.check_circle_outline, size: iconSize, color: validateIconColor)
                        : Icon(Icons.panorama_fish_eye, size: iconSize, color: unValidateIconColor),
                    SizedBox(height: labelSpacing * 2),
                    Icon(Icons.edit, size: iconSize, color: unValidateIconColor),
                  ],
                ),
                SizedBox(width: labelSpacing * 2),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      (labelText != "")
                          ? Row(
                              children: <Widget>[
                                Text(labelText, style: TextStyle(fontSize: labelFontSize, color: labelColor)),
                                SizedBox(width: 10),
                                (isRequired) ? Text("*", style: TextStyle(fontSize: labelFontSize, color: Colors.red)) : SizedBox(),
                              ],
                            )
                          : SizedBox(),
                      (labelText != "") ? SizedBox(height: labelSpacing) : SizedBox(),
                      Container(
                        width: double.maxFinite,
                        height: (fieldHeight != null) ? fieldHeight + borderWidth * borderType + contentVerticalPadding * 2 : null,
                        alignment: (keyboardType == TextInputType.multiline) ? Alignment.topLeft : Alignment.center,
                        decoration: (borderType == 1)
                            ? BoxDecoration(
                                color: fillColor,
                                border: Border(bottom: (scholarshipTextFormFieldProvider.errorState) ? errorBorderside : borderside),
                              )
                            : BoxDecoration(
                                color: fillColor,
                                border: (scholarshipTextFormFieldProvider.errorState)
                                    ? Border.all(width: borderWidth, color: errorBoarderColor)
                                    : Border.all(width: borderWidth, color: borderColor),
                                borderRadius: BorderRadius.circular(borderRadius),
                              ),
                        padding: EdgeInsets.symmetric(horizontal: contentHorizontalPadding, vertical: contentVerticalPadding),
                        child: TextFormField(
                          key: globalkey,
                          focusNode: focusNode,
                          maxLines: (autoHeight) ? null : maxLines,
                          textInputAction: textInputAction,
                          autofocus: (autoHeight) ? scholarshipTextFormFieldProvider.autoFocus : autofocus,
                          initialValue: scholarshipTextFormFieldProvider.initialValue,
                          readOnly: readOnly,
                          textAlign: textAlign,
                          style: TextStyle(color: textColor, fontSize: textFontSize),
                          keyboardType: keyboardType,
                          autovalidate: autovalidate,
                          controller: controller,
                          decoration: InputDecoration(
                            errorStyle: TextStyle(fontSize: 0, color: fillColor),
                            isDense: true,
                            hintText: hintText,
                            hintStyle: TextStyle(fontSize: hintTextFontSize, color: hintTextColor),
                            border: UnderlineInputBorder(borderSide: hideBorderSide),
                            enabledBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                            focusedBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                            focusedErrorBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                            errorBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                            disabledBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                            filled: true,
                            fillColor: fillColor,
                            contentPadding: EdgeInsets.symmetric(
                              horizontal: 0,
                              vertical: 0,
                            ),
                          ),
                          inputFormatters: inputFormatters,
                          obscureText: obscureText,
                          onTap: onTapHandler,
                          onEditingComplete: () {
                            scholarshipTextFormFieldProvider.setAutoFocus(false);
                          },
                          onChanged: (input) {
                            scholarshipTextFormFieldProvider.setAutoFocus(true);
                            scholarshipTextFormFieldProvider.setInitValue(input);
                            if (autoHeight) {
                              scholarshipTextFormFieldProvider.setLines((input != "") ? input.split("\n").length : 1);
                            }
                            scholarshipTextFormFieldProvider.setErrorState(false, "");
                            if (onChangeHandler != null) onChangeHandler(input);
                          },
                          validator: (input) {
                            if (validatorHandler == null) return null;
                            var result = validatorHandler(input);
                            if (result != null) {
                              scholarshipTextFormFieldProvider.setErrorState(true, result, validate: false);
                            } else {
                              scholarshipTextFormFieldProvider.setErrorState(false, "", validate: true);
                            }
                            return result;
                          },
                          onSaved: (input) {
                            if (onSaveHandler == null) return null;
                            onSaveHandler(input.trim());
                          },
                          onFieldSubmitted: onFieldSubmittedHandler,
                        ),
                      ),
                      (scholarshipTextFormFieldProvider.errorState)
                          ? Container(
                              width: double.maxFinite,
                              height: errorStringFontSize + 5,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                scholarshipTextFormFieldProvider.validatorText,
                                style: TextStyle(fontSize: errorStringFontSize, color: Colors.red),
                              ),
                            )
                          : (fixedHeightState)
                              ? Container(
                                  width: double.maxFinite,
                                  height: errorStringFontSize + 5,
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "",
                                    style: TextStyle(fontSize: errorStringFontSize, color: fillColor),
                                  ),
                                )
                              : SizedBox(
                                  width: double.maxFinite,
                                ),
                    ],
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}

class ScholarshipTextFormFieldProvider extends ChangeNotifier {
  static ScholarshipTextFormFieldProvider of(BuildContext context, {bool listen = false}) =>
      Provider.of<ScholarshipTextFormFieldProvider>(context, listen: listen);
  ScholarshipTextFormFieldProvider(this._initialValue);

  bool _autoFocus = false;
  bool get autoFocus => _autoFocus;
  void setAutoFocus(bool autoFocus) {
    _autoFocus = autoFocus;
  }

  double _textFontSize;
  double get textFontSize => _textFontSize;
  void setTextfontSize(double textFontSize) {
    _textFontSize = textFontSize;
  }

  double _height;
  double get height => _height;
  void setHeight(double height) {
    if (_height != height) {
      _height = height;
      notifyListeners();
    }
  }

  String _initialValue;
  String get initialValue => _initialValue;
  void setInitValue(String initialValue) {
    if (_initialValue != initialValue) {
      _initialValue = initialValue;
    }
  }

  int _lines = 1;
  int get lines => _lines;
  void setLines(int lines) {
    if (_lines != lines) {
      _lines = lines;
      notifyListeners();
    }
  }

  bool _errorState = false;
  bool _validate;
  String _validatorText = "";

  bool get errorState => _errorState;
  bool get validate => _validate;
  String get validatorText => _validatorText;

  void setErrorState(bool errorState, String validatorText, {bool validate}) {
    if (_errorState != errorState || (validate != null && _validate != validate)) {
      _errorState = errorState;
      _validatorText = validatorText;
      _validate = validate;
      notifyListeners();
    }
  }
}
