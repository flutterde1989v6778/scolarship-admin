import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'index.dart';

@immutable
class CustomNetworkImage extends StatelessWidget {
  const CustomNetworkImage({
    Key key,
    @required this.url,
    @required this.width,
    @required this.height,
    this.boxFit = BoxFit.cover,
    this.progressIndicatorColor = Colors.blue,
    this.errorTextFontSize = 15,
    this.borderRadius = 0,
    this.brightness = Brightness.light,
    this.unknownImageAsset = "lib/Assets/Images/small_man.png",
  }) : super(key: key);
  final String url;
  final double width;
  final double height;
  final Color progressIndicatorColor;
  final BoxFit boxFit;
  final errorTextFontSize;
  final borderRadius;
  final unknownImageAsset;
  final Brightness brightness;

  @override
  Widget build(BuildContext context) {
    if (url != "" && url != null) {
      return Container(
        width: width,
        height: height,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(borderRadius),
          child: Container(
            width: width,
            height: height,
            child: Image.network(
              url,
              width: width,
              height: height,
              fit: boxFit,
              loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent loadingProgress) {
                if (loadingProgress == null) return child;
                return Container(
                  width: width,
                  height: height,
                  child: Center(
                    child: CustomCupertinoIndicator(
                      size: height / 5,
                      brightness: brightness,
                    ),
                  ),
                );
              },
              errorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                return Container(
                  width: width,
                  height: height,
                  decoration: BoxDecoration(border: Border.all(color: progressIndicatorColor)),
                  child: Center(
                    child: Text("No exist Image", style: TextStyle(fontSize: errorTextFontSize, color: progressIndicatorColor)),
                  ),
                );
              },
            ),
          ),
        ),
      );
    } else {
      return Container(
        width: width,
        height: height,
        decoration: BoxDecoration(border: Border.all(color: progressIndicatorColor)),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(borderRadius),
          child: Icon(
            Icons.not_interested,
            size: height / 2,
            color: progressIndicatorColor,
          ),
        ),
      );
    }
  }
}
