import 'package:easy_localization/easy_localization.dart';

class SettingMenuString {
  static String darkMode = "SettingMenu.darkMode".tr();
  static String lightMode = "SettingMenu.lightMode".tr();
}
