import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart' as EL;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:scholarship/pages/Scholarship/Styles/index.dart';

import 'package:scholarship/utils/index.dart';
import 'package:scholarship/widgets/index.dart';

import 'index.dart';

class Footer extends StatelessWidget {
  FooterStyles _footerStyles;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth >= 900) {
          _footerStyles = FooterDesktopStyles(context);
        } else if (constraints.maxWidth >= 600 && constraints.maxWidth < 900) {
          ////  tablet
          _footerStyles = FooterTabletStyles(context);
        } else if (constraints.maxWidth < 600) {
          _footerStyles = FooterMobileStyles(context);
        }
        return Container(
          padding: EdgeInsets.symmetric(
            horizontal: _footerStyles.primaryHorizontalPadding,
            vertical: _footerStyles.primaryVerticalPadding,
          ),
          color: FooterColors.backgroundColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                FooterString.title,
                style: TextStyle(fontSize: _footerStyles.titleFontSize, color: FooterColors.textColor),
              ),
              SizedBox(height: _footerStyles.shareWidthDp * 10),
              Text(
                FooterString.description,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: _footerStyles.textFontSize,
                  color: FooterColors.textColor,
                  fontWeight: FontWeight.w200,
                ),
              ),
              SizedBox(height: _footerStyles.shareWidthDp * 20),
              Wrap(
                spacing: _footerStyles.shareWidthDp * 10,
                runSpacing: _footerStyles.shareWidthDp * 10,
                alignment: WrapAlignment.center,
                runAlignment: WrapAlignment.center,
                crossAxisAlignment: WrapCrossAlignment.center,
                children: <Widget>[
                  CustomTextFormField(
                    width: _footerStyles.textFieldWidth,
                    height: _footerStyles.textFieldHeight,
                    textFontSize: _footerStyles.textFontSize,
                    hintText: FooterString.emailHitText,
                    fillColor: FooterColors.textFieldFillColor,
                    borderColor: FooterColors.backgroundColor,
                    borderRadius: 0,
                    onChangeHandler: (input) {},
                    validatorHandler: (input) => (!Validators.emailRegExp.hasMatch(input)) ? ScholarshipString.emailValidateString : null,
                    onSaveHandler: (input) {},
                  ),
                  CustomRaisedButton(
                    width: _footerStyles.buttonWidth,
                    height: _footerStyles.buttonHeigt,
                    color: FooterColors.buttonColor,
                    borderColor: FooterColors.backgroundColor,
                    borderRadius: 0,
                    child: Text(
                      FooterString.signupButtonText,
                      style: TextStyle(fontSize: _footerStyles.textFontSize, color: FooterColors.textColor),
                    ),
                  ),
                ],
              ),
              SizedBox(height: _footerStyles.shareWidthDp * 20),
              (_footerStyles.runtimeType == FooterMobileStyles)
                  ? Container(
                      padding: EdgeInsets.symmetric(horizontal: _footerStyles.primaryHorizontalPadding),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(Icons.phone, size: _footerStyles.textFontSize * 1.2, color: FooterColors.textColor),
                                  Text(
                                    FooterString.phoneNumber,
                                    style: TextStyle(fontSize: _footerStyles.textFontSize, color: FooterColors.textColor),
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Icon(Icons.email, size: _footerStyles.textFontSize * 1.2, color: FooterColors.textColor),
                                  Text(
                                    FooterString.email,
                                    style: TextStyle(fontSize: _footerStyles.textFontSize, color: FooterColors.textColor),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(height: _footerStyles.textFontSize * 1.2),
                          Row(
                            children: <Widget>[
                              Icon(Icons.location_on, size: _footerStyles.textFontSize * 1.2, color: FooterColors.textColor),
                              Text(
                                FooterString.location,
                                style: TextStyle(fontSize: _footerStyles.textFontSize, color: FooterColors.textColor),
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Icon(Icons.phone, size: _footerStyles.textFontSize * 1.2, color: FooterColors.textColor),
                            SizedBox(width: _footerStyles.shareWidthDp * 5),
                            Text(
                              FooterString.phoneNumber,
                              style: TextStyle(fontSize: _footerStyles.textFontSize, color: FooterColors.textColor),
                            ),
                          ],
                        ),
                        SizedBox(width: _footerStyles.shareWidthDp * 20),
                        Row(
                          children: <Widget>[
                            Icon(Icons.email, size: _footerStyles.textFontSize * 1.2, color: FooterColors.textColor),
                            SizedBox(width: _footerStyles.shareWidthDp * 5),
                            Text(
                              FooterString.email,
                              style: TextStyle(fontSize: _footerStyles.textFontSize, color: FooterColors.textColor),
                            ),
                          ],
                        ),
                        SizedBox(width: _footerStyles.shareWidthDp * 20),
                        Row(
                          children: <Widget>[
                            Icon(Icons.location_on, size: _footerStyles.textFontSize * 1.2, color: FooterColors.textColor),
                            SizedBox(width: _footerStyles.shareWidthDp * 5),
                            Text(
                              FooterString.location,
                              style: TextStyle(fontSize: _footerStyles.textFontSize, color: FooterColors.textColor),
                            ),
                          ],
                        ),
                      ],
                    ),
              SizedBox(height: _footerStyles.shareWidthDp * 40),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    FontAwesomeIcons.facebookSquare,
                    size: _footerStyles.textFontSize * 1.5,
                    color: FooterColors.textColor,
                  ),
                  SizedBox(width: _footerStyles.shareWidthDp * 15),
                  FaIcon(
                    FontAwesomeIcons.instagram,
                    size: _footerStyles.textFontSize * 1.5,
                    color: FooterColors.textColor,
                  ),
                  SizedBox(width: _footerStyles.shareWidthDp * 15),
                  FaIcon(
                    FontAwesomeIcons.twitter,
                    size: _footerStyles.textFontSize * 1.5,
                    color: FooterColors.textColor,
                  ),
                ],
              ),
              SizedBox(height: _footerStyles.shareWidthDp * 15),
              Text(
                FooterString.copyright,
                style: TextStyle(fontSize: _footerStyles.copyrightFontSize, color: FooterColors.textColor),
              )
            ],
          ),
        );
      },
    );
  }
}
