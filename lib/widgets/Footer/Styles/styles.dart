import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:meta/meta.dart';
import 'package:scholarship/Config/resposible_settings.dart';

class FooterStyles {
  double devicePixelRatio;
  double deviceWidth;
  double deviceHeight;
  double fontUnit;
  double statusbarHeight;
  double bottombarHeight;
  double appbarHeight;
  double safeAreaHeight;
  double shareWidth;
  double shareHeight;
  double shareWidthDp;
  double shareHeightDp;

  double primaryHorizontalPadding;
  double primaryVerticalPadding;

  double titleFontSize;
  double textFontSize;
  double copyrightFontSize;
  double textFieldWidth;
  double textFieldHeight;
  double buttonWidth;
  double buttonHeigt;

  FooterStyles(BuildContext context);
}

class FooterDesktopStyles extends FooterStyles {
  FooterDesktopStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(30);
    primaryVerticalPadding = ScreenUtil().setWidth(27);

    titleFontSize = ScreenUtil().setSp(24, allowFontScalingSelf: false);
    textFontSize = ScreenUtil().setSp(14, allowFontScalingSelf: false);
    copyrightFontSize = ScreenUtil().setSp(20, allowFontScalingSelf: false);
    textFieldWidth = ScreenUtil().setWidth(400);
    textFieldHeight = ScreenUtil().setWidth(32);
    buttonWidth = ScreenUtil().setWidth(130);
    buttonHeigt = ScreenUtil().setWidth(32);
  }
}

class FooterTabletStyles extends FooterStyles {
  FooterTabletStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(30);
    primaryVerticalPadding = ScreenUtil().setWidth(27);

    titleFontSize = ScreenUtil().setSp(24, allowFontScalingSelf: false);
    textFontSize = ScreenUtil().setSp(14, allowFontScalingSelf: false);
    copyrightFontSize = ScreenUtil().setSp(20, allowFontScalingSelf: false);
    textFieldWidth = ScreenUtil().setWidth(300);
    textFieldHeight = ScreenUtil().setWidth(32);
    buttonWidth = ScreenUtil().setWidth(130);
    buttonHeigt = ScreenUtil().setWidth(32);
  }
}

class FooterMobileStyles extends FooterStyles {
  FooterMobileStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.mobileDesignWidth,
      height: ResponsibleDesignSettings.mobileDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(30);
    primaryVerticalPadding = ScreenUtil().setWidth(27);

    titleFontSize = ScreenUtil().setSp(18, allowFontScalingSelf: false);
    textFontSize = ScreenUtil().setSp(12, allowFontScalingSelf: false);
    copyrightFontSize = ScreenUtil().setSp(12, allowFontScalingSelf: false);
    textFieldWidth = deviceWidth - primaryHorizontalPadding * 2;
    textFieldHeight = ScreenUtil().setWidth(32);
    buttonWidth = ScreenUtil().setWidth(110);
    buttonHeigt = ScreenUtil().setWidth(32);
  }
}
