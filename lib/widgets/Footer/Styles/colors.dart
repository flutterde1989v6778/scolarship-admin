import 'package:flutter/material.dart';

class FooterColors {
  static Color backgroundColor = Color(0xFF232323);
  static Color textColor = Colors.white;
  static Color buttonColor = Color(0xFFB58970);
  static Color textFieldFillColor = Color(0xFFC4C4C4);
}
