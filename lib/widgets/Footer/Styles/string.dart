import 'package:easy_localization/easy_localization.dart';

class FooterString {
  static String title = "Footer.title".tr();
  static String description = "Footer.description".tr();
  static String emailHitText = "Footer.emailHitText".tr();
  static String signupButtonText = "Footer.signupButtonText".tr();
  static String phoneNumber = "Footer.phoneNumber".tr();
  static String email = "Footer.email".tr();
  static String location = "Footer.location".tr();
  static String copyright = "Footer.copyright".tr();
}
