import 'dart:convert';
import 'dart:html' as html;
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:scholarship/widgets/index.dart';

class CustomFilePickerForWeb extends FormField<bool> {
  CustomFilePickerForWeb({
    Key key,
    @required context,
    @required width,
    @required height,
    fit = BoxFit.cover,
    borderWidth = 1,
    borderColor = Colors.black,
    selectWidget = const Icon(Icons.add, size: 15, color: Colors.black),
    multiple = false,
    draggable = true,
    fileTypes = '.png,.jpg',
    onChangeHandler,
    FormFieldValidator<bool> validator,
    initialValue: false,
    autovalidate: false,
    deleteWidget = const Icon(Icons.delete_forever, size: 15, color: Colors.black),
    validateTextFontSize = 15,
    validateTextColor = Colors.red,
    fileByteData,
    file,
    url = "",
    onSaved,
  }) : super(
          key: key,
          initialValue: initialValue,
          autovalidate: autovalidate,
          validator: validator,
          onSaved: onSaved,
          builder: (FormFieldState<bool> state) {
            return MultiProvider(
              providers: [
                ChangeNotifierProvider(
                  create: (_) => CustomFilePickerForWebProvider(onChangeHandler: onChangeHandler, file: file, fileByteData: fileByteData, url: url),
                ),
              ],
              child: Consumer<CustomFilePickerForWebProvider>(builder: (context, customFilePickerForWebProvider, _) {
                WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
                  if (customFilePickerForWebProvider.file != null && customFilePickerForWebProvider.fileByteData != null && !state.value)
                    state.didChange(true);
                  else if ((customFilePickerForWebProvider.file == null || customFilePickerForWebProvider.fileByteData == null) && state.value)
                    state.didChange(false);
                });

                return Container(
                  width: width,
                  height: height + validateTextFontSize + 20,
                  child: (customFilePickerForWebProvider.file != null && customFilePickerForWebProvider.fileByteData != null)
                      ? Container(
                          width: width,
                          height: height,
                          child: Column(
                            children: <Widget>[
                              Image.memory(
                                customFilePickerForWebProvider.fileByteData,
                                width: width,
                                height: height,
                                fit: fit,
                              ),
                              Container(
                                height: validateTextFontSize + 20,
                                child: Center(
                                  child: GestureDetector(
                                    onTap: () => customFilePickerForWebProvider.deleteImageData(),
                                    child: deleteWidget,
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      : (customFilePickerForWebProvider.url != "" && customFilePickerForWebProvider.url != null)
                          ? Container(
                              width: width,
                              height: height,
                              child: Column(
                                children: <Widget>[
                                  CustomNetworkImage(
                                    url: customFilePickerForWebProvider.url,
                                    width: width,
                                    height: height,
                                    boxFit: fit,
                                  ),
                                  Container(
                                    height: validateTextFontSize + 20,
                                    child: Center(
                                      child: GestureDetector(
                                        onTap: () => customFilePickerForWebProvider.deleteImageData(),
                                        child: deleteWidget,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          : Container(
                              width: width,
                              height: height + validateTextFontSize + 20,
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    width: width,
                                    height: height,
                                    decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                    child: Center(
                                      child: GestureDetector(
                                        child: selectWidget,
                                        onTap: () {
                                          _startWebFilePicker(customFilePickerForWebProvider, multiple, draggable, fileTypes);
                                        },
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: validateTextFontSize + 20,
                                    alignment: Alignment.center,
                                    child: Center(
                                      child: Text(state.errorText ?? "", style: TextStyle(fontSize: validateTextFontSize, color: validateTextColor)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                );
              }),
            );
          },
        );
}

void _startWebFilePicker(CustomFilePickerForWebProvider customFilePickerForWebProvider, bool multiple, bool draggable, String fileTypes) async {
  html.InputElement fileInput = html.FileUploadInputElement();
  fileInput.multiple = multiple;
  fileInput.draggable = draggable;
  fileInput.accept = fileTypes;
  fileInput.click();

  fileInput.onChange.listen((e) async {
    // read file content as dataURL
    List<html.File> files = fileInput.files;
    if (files.length != 0) {
      // for (var i = 0; i < files.length; i++) {
      html.File file = files[0];
      html.FileReader reader = html.FileReader();

      reader.onError.listen((fileEvent) {});

      reader.onLoadEnd.listen((e) {
        Uint8List _bytesData;
        _bytesData = Base64Decoder().convert(reader.result.toString().split(",").last);
        _imageLoadHandler(customFilePickerForWebProvider, file, _bytesData);
      });

      reader.onProgress.listen((event) {
        event.preventDefault();
      });

      reader.readAsDataUrl(file);
    }
    // }
  });
}

void _imageLoadHandler(CustomFilePickerForWebProvider customFilePickerForWebProvider, html.File file, Uint8List bytesData) {
  customFilePickerForWebProvider.setImageData(file, bytesData);
}

class CustomFilePickerForWebProvider extends ChangeNotifier {
  static CustomFilePickerForWebProvider of(BuildContext context, {bool listen = false}) =>
      Provider.of<CustomFilePickerForWebProvider>(context, listen: listen);

  CustomFilePickerForWebProvider({onChangeHandler, file, fileByteData, url}) {
    this.onChangeHandler = onChangeHandler;
    this._fileByteData = fileByteData;
    this._file = file;
    this._url = url;
  }

  String _url;
  String get url => _url;
  void setUrl(String url) {
    _url = url;
  }

  bool _initState = false;
  bool get initState => _initState;
  void setInitState() {
    _initState = true;
  }

  Function onChangeHandler;

  html.File _file;
  html.File get file => _file;
  Uint8List _fileByteData;
  Uint8List get fileByteData => _fileByteData;

  void setImageData(html.File file, Uint8List fileByteData) {
    setInitState();
    if (_file != file || _fileByteData != fileByteData) {
      _file = file;
      _fileByteData = fileByteData;
      if (onChangeHandler != null) onChangeHandler(file, fileByteData);
      notifyListeners();
    }
  }

  void deleteImageData() {
    setInitState();
    if (_file != null || _fileByteData != null || url != null) {
      _file = null;
      _fileByteData = null;
      _url = null;
      if (onChangeHandler != null) onChangeHandler(file, fileByteData);
      notifyListeners();
    }
  }
}
