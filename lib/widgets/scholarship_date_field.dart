import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:scholarship/widgets/custom_date_field.dart';

class ScholarshipDateField extends StatelessWidget {
  ScholarshipDateField({
    Key key,
    @required this.width,
    @required this.height,
    this.initialValue,
    this.controller,
    this.fixedHeightState = true,
    this.labelText = "",
    this.labelFontSize,
    this.labelColor = Colors.blueAccent,
    this.labelSpacing = 5,
    this.textFontSize = 20,
    this.textColor = Colors.black,
    this.hintText = "",
    this.hintTextFontSize,
    this.hintTextColor = Colors.grey,
    this.borderType = 1, // 1: UnderlineInputBorder 2: OutlineInputBorder
    this.borderWidth = 1,
    this.borderColor = Colors.black,
    this.borderRadius = 4.0,
    this.enableBorderColor,
    this.focusBorderColor,
    this.errorBoarderColor = Colors.red,
    this.disabledBorderColor = const Color(0x33ffffff),
    this.textAlign = TextAlign.left,
    this.keyboardType = TextInputType.text,
    this.fillColor = Colors.white,
    this.autovalidate = false,
    this.obscureText = false,
    this.autofocus = false,
    this.contentHorizontalPadding = 5,
    this.contentVerticalPadding = 5,
    this.maxLines = 1,
    this.textInputAction = TextInputAction.done,
    this.readOnly = false,
    this.errorStringFontSize,
    this.onChangeHandler,
    this.validatorHandler,
    this.onSaveHandler,
    this.onTapHandler,
    this.onFieldSubmittedHandler,
    this.focusNode,
    this.inputFormatters,
    this.validateIconColor = Colors.green,
    this.unValidateIconColor = Colors.grey,
    this.iconSize,
  }) : super(key: key);
  final double width;
  final double height;
  final DateTime initialValue;
  final TextEditingController controller;
  final bool fixedHeightState;
  final String labelText;
  double labelFontSize;
  final Color labelColor;
  final double labelSpacing;
  final double textFontSize;
  final Color textColor;
  final String hintText;
  double hintTextFontSize;
  Color hintTextColor;
  final int borderType;
  final double borderWidth;
  final Color borderColor;
  final double borderRadius;
  Color enableBorderColor;
  Color focusBorderColor;
  Color errorBoarderColor;
  Color disabledBorderColor;
  final format = DateFormat("yyyy-MM-dd");
  final Color fillColor;
  final TextAlign textAlign;
  final TextInputType keyboardType;
  final bool autovalidate;
  final bool obscureText;
  final double contentHorizontalPadding;
  final double contentVerticalPadding;
  final bool autofocus;
  final int maxLines;
  final TextInputAction textInputAction;
  final bool readOnly;
  final inputFormatters;
  final Function onChangeHandler;
  final Function validatorHandler;
  final Function onSaveHandler;
  final Function onTapHandler;
  final Function onFieldSubmittedHandler;
  double errorStringFontSize;
  final focusNode;
  final Color validateIconColor;
  final Color unValidateIconColor;
  final double iconSize;

  @override
  Widget build(BuildContext context) {
    hintTextFontSize = hintTextFontSize ?? textFontSize;
    hintTextColor = hintTextColor ?? textColor;
    focusBorderColor = focusBorderColor ?? borderColor;
    enableBorderColor = enableBorderColor ?? borderColor;
    if (errorStringFontSize == null) errorStringFontSize = textFontSize * 0.8;

    BorderSide borderside = BorderSide(width: borderWidth, color: borderColor);
    BorderSide errorBorderside = BorderSide(width: borderWidth, color: errorBoarderColor);
    BorderSide hideBorderSide = BorderSide(width: 0, color: fillColor);

    return Material(
      color: fillColor,
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => ScholarshipDateFieldProvider()),
        ],
        child: Consumer<ScholarshipDateFieldProvider>(
          builder: (context, scholarshipDateFieldProvider, _) {
            return Container(
              width: width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      (scholarshipDateFieldProvider.validate != null && scholarshipDateFieldProvider.validate)
                          ? Icon(Icons.check_circle_outline, size: iconSize, color: validateIconColor)
                          : Icon(Icons.panorama_fish_eye, size: iconSize, color: unValidateIconColor),
                      SizedBox(height: labelSpacing * 2),
                      Icon(Icons.edit, size: iconSize, color: unValidateIconColor),
                    ],
                  ),
                  SizedBox(width: labelSpacing * 2),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(labelText, style: TextStyle(fontSize: labelFontSize, color: labelColor)),
                        SizedBox(height: labelSpacing),
                        Container(
                          width: double.maxFinite,
                          height: height + ((fixedHeightState) ? textFontSize * 0.8 + 5 : 0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: double.maxFinite,
                                height: height,
                                alignment: Alignment.center,
                                decoration: (borderType == 1)
                                    ? BoxDecoration(
                                        color: fillColor,
                                        border: Border(bottom: (scholarshipDateFieldProvider.errorState) ? errorBorderside : borderside),
                                      )
                                    : BoxDecoration(
                                        color: fillColor,
                                        border: (scholarshipDateFieldProvider.errorState)
                                            ? Border.all(width: borderWidth, color: errorBoarderColor)
                                            : Border.all(width: borderWidth, color: borderColor),
                                        borderRadius: BorderRadius.circular(borderRadius),
                                      ),
                                child: DateTimeField(
                                  style: TextStyle(fontSize: textFontSize, color: textColor),
                                  initialValue: initialValue,
                                  format: format,
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(fontSize: 0, color: Colors.white),
                                    hintText: hintText,
                                    isDense: false,
                                    hintStyle: TextStyle(fontSize: hintTextFontSize, color: hintTextColor),
                                    border: UnderlineInputBorder(borderSide: hideBorderSide),
                                    enabledBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                                    focusedBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                                    focusedErrorBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                                    errorBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                                    disabledBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                                    contentPadding: EdgeInsets.symmetric(
                                      horizontal: contentHorizontalPadding,
                                      vertical: contentVerticalPadding,
                                    ),
                                    fillColor: fillColor,
                                  ),
                                  onShowPicker: (context, currentValue) async {
                                    return await showDatePicker(
                                        context: context,
                                        firstDate: DateTime(1900),
                                        initialDate: currentValue ?? DateTime.now(),
                                        lastDate: DateTime(2100));
                                  },
                                  resetIcon: (!readOnly) ? Icon(Icons.delete, color: textColor) : null,
                                  readOnly: readOnly,
                                  enableInteractiveSelection: !readOnly,
                                  enabled: !readOnly,
                                  validator: (input) {
                                    if (input == null)
                                      scholarshipDateFieldProvider.setErrorState(true, "Please enter available Date", validate: false);
                                    else
                                      scholarshipDateFieldProvider.setErrorState(false, "", validate: true);

                                    return (input == null) ? "Please enter available Date" : null;
                                  },
                                  onChanged: (date) {
                                    scholarshipDateFieldProvider.setErrorState(
                                      false,
                                      "",
                                    );
                                    if (!readOnly && onChangeHandler != null) onChangeHandler(DateTimeField.tryFormat(date, format));
                                  },
                                  onSaved: (date) {
                                    if (onSaveHandler != null) onSaveHandler(DateTimeField.tryFormat(date, format));
                                  },
                                ),
                              ),
                              (scholarshipDateFieldProvider.errorState)
                                  ? Container(
                                      width: double.maxFinite,
                                      height: textFontSize * 0.8 + 5,
                                      alignment: Alignment.bottomLeft,
                                      child: Text(
                                        scholarshipDateFieldProvider.validatorText,
                                        style: TextStyle(fontSize: textFontSize * 0.8, color: Colors.red),
                                      ),
                                    )
                                  : (fixedHeightState) ? SizedBox(width: double.maxFinite, height: textFontSize * 0.8) : SizedBox(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

class ScholarshipDateFieldProvider extends ChangeNotifier {
  static ScholarshipDateFieldProvider of(BuildContext context, {bool listen = false}) =>
      Provider.of<ScholarshipDateFieldProvider>(context, listen: listen);

  bool _errorState = false;
  bool _validate = false;
  String _validatorText = "";

  bool get errorState => _errorState;
  bool get validate => _validate;
  String get validatorText => _validatorText;

  void setErrorState(bool errorState, String validatorText, {bool validate}) {
    if (_errorState != errorState || _validate != validate) {
      _errorState = errorState;
      _validatorText = validatorText;
      _validate = validate;
      notifyListeners();
    }
  }
}
