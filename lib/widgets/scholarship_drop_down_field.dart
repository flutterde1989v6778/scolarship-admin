import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomDropDownFormField extends FormField<dynamic> {
  CustomDropDownFormField({
    Key key,
    @required BuildContext context,
    @required double width,
    @required double height,
    @required List<Map<String, dynamic>> items,
    @required Function(dynamic) onChangeHandler,
    bool isDense: true,
    bool isExpanded: false,
    dynamic value,
    bool autovalidate: false,
    FormFieldValidator<dynamic> validatorHandler,
    Function onSaveHandler,

    /// label
    bool isRequired = false,
    String label = "",
    double labelFontSize,
    Color labelColor,
    double labelSpacing = 5,

    /// icons
    List<Widget> prefixIcons = const [],
    List<Widget> suffixIcons = const [],
    bool isPrefixIconOutofField = false,
    bool isSuffixIconOutofField = false,
    double iconSpacing = 10,

    /// border
    Color fillColor = Colors.white,
    Border border = const Border(bottom: BorderSide(width: 1, color: Colors.black)),
    Border errorBorder = const Border(bottom: BorderSide(width: 1, color: Colors.red)),
    double borderRadius = 0,
    // textfield
    double contentHorizontalPadding = 5,
    double contentVerticalPadding = 5,
    bool isfixedHeight = true,
    double itemTextFontSize = 15,
    Color itemTextColor = Colors.black,
    double selectedItemTextFontSize,
    Color selectedItemTextColor,
    Color dropdownColor = Colors.white,
    String hintText = "",
    double hintTextFontSize,
    Color hintTextColor = Colors.grey,
    bool fixedHeightState = true,
    bool isDoneValidate = false,
  }) : super(
          key: key,
          initialValue: value,
          autovalidate: autovalidate,
          validator: (value) {
            isDoneValidate = true;
            return validatorHandler(value);
          },
          onSaved: onSaveHandler,
          builder: (FormFieldState<dynamic> state) {
            return MultiProvider(
              providers: [ChangeNotifierProvider(create: (context) => CustomDropDownFormFieldProvider(value))],
              child: Consumer<CustomDropDownFormFieldProvider>(builder: (context, customDropDownFormFieldProvider, _) {
                WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
                  if (!customDropDownFormFieldProvider.isDoneValidate && customDropDownFormFieldProvider.isDoneValidate != isDoneValidate)
                    customDropDownFormFieldProvider.setIsDoneValidate(true);
                });
                Widget prefixIcon = SizedBox();
                Widget suffixIcon = SizedBox();
                if (prefixIcons.length != 0 && !state.hasError && customDropDownFormFieldProvider.isDoneValidate)
                  prefixIcon = prefixIcons[0];
                else if (prefixIcons.length != 0) prefixIcon = prefixIcons[1] ?? prefixIcons[0];

                if (suffixIcons.length != 0 && !state.hasError && customDropDownFormFieldProvider.isDoneValidate)
                  suffixIcon = suffixIcons[0];
                else if (suffixIcons.length != 0) suffixIcon = suffixIcons[1] ?? suffixIcons[0];

                return Container(
                  width: width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      (isPrefixIconOutofField) ? prefixIcon : SizedBox(),
                      (isPrefixIconOutofField && prefixIcons.length != 0) ? SizedBox(width: iconSpacing) : SizedBox(),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text(
                                  label,
                                  style: TextStyle(
                                    fontSize: labelFontSize ?? itemTextFontSize,
                                    color: labelColor ?? selectedItemTextColor ?? itemTextColor,
                                  ),
                                ),
                                SizedBox(width: 10),
                                (isRequired) ? Text("*", style: TextStyle(fontSize: labelFontSize, color: Colors.red)) : SizedBox(),
                              ],
                            ),
                            (label != "") ? SizedBox(height: labelSpacing) : SizedBox(),
                            Container(
                              width: double.maxFinite,
                              height: height,
                              decoration: BoxDecoration(
                                color: fillColor,
                                border: (state.hasError) ? errorBorder : border,
                                borderRadius: border.isUniform && errorBorder.isUniform ? BorderRadius.circular(borderRadius) : null,
                              ),
                              padding: EdgeInsets.symmetric(horizontal: contentHorizontalPadding, vertical: contentVerticalPadding),
                              child: Row(
                                children: <Widget>[
                                  (!isPrefixIconOutofField) ? prefixIcon : SizedBox(),
                                  (!isPrefixIconOutofField && prefixIcons.length != 0) ? SizedBox(width: iconSpacing) : SizedBox(),
                                  Expanded(
                                    child: DropdownButton(
                                      underline: SizedBox(),
                                      items: items
                                          .map((item) => DropdownMenuItem(
                                                child: new Text(item["text"], style: TextStyle(fontSize: itemTextFontSize, color: itemTextColor)),
                                                value: item["value"],
                                              ))
                                          .toList(),
                                      selectedItemBuilder: (BuildContext context) {
                                        return items.map<Widget>((item) {
                                          return Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                item["text"],
                                                style: TextStyle(
                                                  fontSize: selectedItemTextFontSize ?? itemTextFontSize,
                                                  color: selectedItemTextColor ?? itemTextColor,
                                                ),
                                              )
                                            ],
                                          );
                                        }).toList();
                                      },
                                      dropdownColor: dropdownColor,
                                      hint: Text(
                                        hintText,
                                        style: TextStyle(
                                            fontSize: hintTextFontSize ?? selectedItemTextFontSize ?? itemTextFontSize, color: hintTextColor),
                                      ),
                                      isDense: isDense,
                                      isExpanded: isExpanded,
                                      value: customDropDownFormFieldProvider.value,
                                      onChanged: (value) {
                                        onChangeHandler(value);
                                        customDropDownFormFieldProvider.setValue(value);
                                        state.didChange(value);
                                      },
                                    ),
                                  ),
                                  (!isSuffixIconOutofField && suffixIcons.length != 0) ? SizedBox(width: iconSpacing) : SizedBox(),
                                  (!isSuffixIconOutofField) ? suffixIcon : SizedBox(),
                                ],
                              ),
                            ),
                            (state.hasError)
                                ? Container(
                                    height: selectedItemTextFontSize ?? itemTextFontSize + 5,
                                    child: Text(
                                      (state.errorText ?? ""),
                                      style: TextStyle(fontSize: (selectedItemTextFontSize ?? itemTextFontSize) * 0.8, color: Colors.red),
                                    ),
                                  )
                                : (fixedHeightState) ? SizedBox(height: selectedItemTextFontSize ?? itemTextFontSize + 5) : SizedBox(),
                          ],
                        ),
                      ),
                      (isSuffixIconOutofField && suffixIcons.length != 0) ? SizedBox(width: iconSpacing) : SizedBox(),
                      (isSuffixIconOutofField) ? suffixIcon : SizedBox(),
                    ],
                  ),
                );
              }),
            );
          },
        );
}

class CustomDropDownFormFieldProvider extends ChangeNotifier {
  static CustomDropDownFormFieldProvider of(BuildContext context, {bool listen = false}) =>
      Provider.of<CustomDropDownFormFieldProvider>(context, listen: listen);

  CustomDropDownFormFieldProvider(value) {
    _value = value;
  }

  dynamic _value;
  dynamic get value => _value;
  void setValue(dynamic value) {
    if (_value != value) {
      _value = value;
    }
  }

  bool _isDoneValidate = false;
  bool get isDoneValidate => _isDoneValidate;
  void setIsDoneValidate(bool isDoneValidate) {
    if (_isDoneValidate != isDoneValidate) {
      _isDoneValidate = isDoneValidate;
      notifyListeners();
    }
  }
}
