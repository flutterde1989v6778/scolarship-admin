import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

import 'mobile_view_youtube.dart';
import 'web_view_youtube.dart';

class Youtube extends StatelessWidget {
  final String url;
  final double size;
  final String element;

  Youtube({
    @required this.url,
    @required this.size,
    @required this.element
  });

  @override
  Widget build(BuildContext context) {
    return kIsWeb 
        ? WebViewYoutube(
          youtubeUrl: url,
          size: size,
          element: element,
        )
        : MobileViewYoutube(
          url: url,
          size: size,
        );
  }
}