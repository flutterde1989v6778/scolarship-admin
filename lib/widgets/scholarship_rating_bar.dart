import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:scholarship/widgets/custom_rating_bar.dart';

class ScholarshipRatingBar extends StatelessWidget {
  final double rating;
  final double spacing;
  final double evaluatorSize;
  final double evaluatorLingHeight;
  final Color defaultColor;
  final Color defaultBackgroundColor;
  final double borderWidth;
  final Color selectedColor;
  final Function onRatedHandler;
  final String topLabel;
  final Color topLabelColor;
  final double topLabelFontSize;
  final String bottomLabel;
  final Color bottomLabelColor;
  final double bottomLabelFontSize;
  final bool isReadOnly;

  ScholarshipRatingBar({
    @required this.rating,
    @required this.spacing,
    @required this.evaluatorSize,
    this.evaluatorLingHeight = 0,
    @required this.borderWidth,
    @required this.defaultColor,
    @required this.defaultBackgroundColor,
    @required this.selectedColor,
    this.isReadOnly = false,
    this.onRatedHandler,
    this.topLabel = "",
    this.topLabelColor,
    this.topLabelFontSize,
    this.bottomLabel = "",
    this.bottomLabelColor,
    this.bottomLabelFontSize,
  });
  @override
  Widget build(BuildContext context) {
    return CustomRatingBar(
      starCount: 5,
      spacing: spacing,
      rating: rating,
      allowHalfRating: false,
      isReadOnly: isReadOnly,
      defaultIconData: Column(
        children: <Widget>[
          (evaluatorLingHeight != 0)
              ? Container(
                  width: evaluatorSize,
                  height: evaluatorLingHeight,
                  decoration: BoxDecoration(
                    color: defaultColor,
                    borderRadius: BorderRadius.circular(evaluatorLingHeight / 2),
                  ),
                )
              : SizedBox(),
          (evaluatorLingHeight != 0) ? SizedBox(height: spacing) : SizedBox(),
          Container(
            width: evaluatorSize,
            height: evaluatorSize,
            decoration: BoxDecoration(
              color: defaultBackgroundColor,
              borderRadius: BorderRadius.circular(evaluatorSize / 2),
              border: Border.all(
                width: borderWidth,
                color: defaultColor,
              ),
            ),
            alignment: Alignment.center,
            child: Icon(
              Icons.person,
              size: evaluatorSize * 0.7,
              color: defaultColor,
            ),
          )
        ],
      ),
      filledIconData: Column(
        children: <Widget>[
          (evaluatorLingHeight != 0)
              ? Container(
                  width: evaluatorSize,
                  height: evaluatorLingHeight,
                  decoration: BoxDecoration(
                    color: selectedColor,
                    borderRadius: BorderRadius.circular(evaluatorLingHeight / 2),
                  ),
                )
              : SizedBox(),
          (evaluatorLingHeight != 0) ? SizedBox(height: spacing) : SizedBox(),
          Container(
            width: evaluatorSize,
            height: evaluatorSize,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(evaluatorSize / 2),
              border: Border.all(
                width: borderWidth,
                color: selectedColor,
              ),
            ),
            alignment: Alignment.center,
            // child: Icon(
            //   Icons.person,
            //   size: _editApplicationPageStyles.evaluatorSize * 0.7,
            //   color: _scholarshipColors.secondaryColor,
            // ),
          )
        ],
      ),
      size: evaluatorSize,
      topLabel: topLabel,
      topLabelColor: topLabelColor,
      topLabelFontSize: topLabelFontSize,
      bottomLabel: bottomLabel,
      bottomLabelColor: bottomLabelColor,
      bottomLabelFontSize: bottomLabelFontSize,
      onRated: onRatedHandler,
    );
  }
}
