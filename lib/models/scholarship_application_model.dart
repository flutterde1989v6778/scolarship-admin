class ScholarshipApplicationModel {
  String id;
  String userID;
  String scholarshipID;
  String name;
  String email;
  dynamic discipline;
  String phoneNumber;
  String location;
  int age;
  String twitter;
  String instagram;
  String facebook;
  String videoUrl;
  String letter1Url;
  String letter2Url;
  String evaluatorName;
  bool minimumQualifications;
  String backgroundFits;
  int score;
  String comments;
  double evaluation;
  bool isRead;
  bool isReviewed;
  int ts;

  ScholarshipApplicationModel({
    id = "",
    userID = "",
    scholarshipID = "",
    name = "",
    email = "",
    discipline,
    phoneNumber = "",
    location = "",
    isOpend = false,
    age = 0,
    twitter = "",
    instagram = "",
    facebook = "",
    videoUrl = "",
    letter1Url = "",
    letter2Url = "",
    evaluatorName = "",
    minimumQualifications,
    backgroundFits = "",
    score,
    comments = "",
    evaluation = 0,
    isRead = false,
    isReviewed = false,
    ts = 0,
  }) {
    this.id = id;
    this.userID = userID;
    this.scholarshipID = scholarshipID;
    this.name = name;
    this.email = email;
    this.discipline = discipline;
    this.phoneNumber = phoneNumber;
    this.location = location;
    this.age = age;
    this.twitter = twitter;
    this.instagram = instagram;
    this.facebook = facebook;
    this.videoUrl = videoUrl;
    this.letter1Url = letter1Url;
    this.letter2Url = letter2Url;
    this.evaluatorName = evaluatorName;
    this.minimumQualifications = minimumQualifications;
    this.backgroundFits = backgroundFits;
    this.score = score;
    this.comments = comments;
    this.evaluation = evaluation;
    this.isRead = isRead;
    this.isReviewed = isReviewed;
    this.ts = ts;
  }

  ScholarshipApplicationModel.fromJson(Map<String, dynamic> json)
      : id = (json['id'] != null) ? json['id'] : "",
        userID = (json['userID'] != null) ? json['userID'] : "",
        scholarshipID = (json['scholarshipID'] != null) ? json['scholarshipID'] : "",
        name = (json['name'] != null) ? json['name'] : "",
        email = (json['email'] != null) ? json['email'] : "",
        discipline = (json['discipline'] != null) ? json['discipline'] : null,
        phoneNumber = (json['phoneNumber'] != null) ? json['phoneNumber'] : "",
        location = (json['location'] != null) ? json['location'] : "",
        age = (json['age'] != null) ? json['age'] : 0,
        twitter = (json['twitter'] != null) ? json['twitter'] : "",
        instagram = (json['instagram'] != null) ? json['instagram'] : "",
        facebook = (json['facebook'] != null) ? json['facebook'] : "",
        videoUrl = (json['videoUrl'] != null) ? json['videoUrl'] : "",
        letter1Url = (json['letter1Url'] != null) ? json['letter1Url'] : "",
        letter2Url = (json['letter2Url'] != null) ? json['letter2Url'] : "",
        evaluatorName = (json['evaluatorName'] != null) ? json['evaluatorName'] : "",
        minimumQualifications = (json['minimumQualifications'] != null) ? json['minimumQualifications'] : null,
        backgroundFits = (json['backgroundFits'] != null) ? json['backgroundFits'] : "",
        score = (json['score'] != null) ? json['score'] : null,
        comments = (json['comments'] != null) ? json['comments'] : "",
        evaluation = (json['evaluation'] != null) ? json['evaluation'] : 0,
        isRead = (json['isRead'] != null) ? json['isRead'] : false,
        isReviewed = (json['isReviewed'] != null) ? json['isReviewed'] : false,
        ts = (json['ts'] != null) ? json['ts'] : 0;

  Map<String, dynamic> toJson() {
    return {
      "id": (id == null) ? "" : id,
      "scholarshipID": (scholarshipID == null) ? "" : scholarshipID,
      "name": (name == null) ? "" : name,
      "email": (email == null) ? "" : email,
      "discipline": (discipline == null) ? null : discipline,
      "phoneNumber": (phoneNumber == null) ? "" : phoneNumber,
      "location": (location == null) ? "" : location,
      "age": (age == null) ? 0 : age,
      "twitter": (twitter == null) ? "" : twitter,
      "instagram": (instagram == null) ? "" : instagram,
      "facebook": (facebook == null) ? "" : facebook,
      "videoUrl": (videoUrl == null) ? "" : videoUrl,
      "letter1Url": (letter1Url == null) ? "" : letter1Url,
      "letter2Url": (letter2Url == null) ? "" : letter2Url,
      "evaluatorName": (evaluatorName == null) ? "" : evaluatorName,
      "minimumQualifications": (minimumQualifications == null) ? null : minimumQualifications,
      "backgroundFits": (backgroundFits == null) ? "" : backgroundFits,
      "score": (score == null) ? null : score,
      "comments": (comments == null) ? "" : comments,
      "evaluation": (evaluation == null) ? 0 : evaluation,
      "isRead": (isRead == null) ? false : isRead,
      "isReviewed": (isReviewed == null) ? false : isReviewed,
      "ts": (ts == null) ? 0 : ts,
    };
  }
}
