import 'index.dart';

class ScholarshipModel extends BaseModel {
  String id;
  String userID;
  String name;
  String description;
  String imageUrl;
  bool isOpened;
  int deadlines;

  /// about
  String videoUrlForAbout;
  String subTitle1ForAbout;
  String subDescription1ForAbout;
  String subTitle2ForAbout;
  String subDescription2ForAbout;

  /// eligibility
  String awardBrief;
  String awardDetails;

  String requirementBrief;
  String requirementDetails;

  String dateTimesBrief;
  String dateTimesDetails;
  int ts;

  ScholarshipModel({
    id = "",
    userID = "",
    name = "",
    description = "",
    imageUrl = "",
    isOpened = false,
    deadlines = 0,
    videoUrlForAbout = "",
    subTitle1ForAbout = "",
    subDescription1ForAbout = "",
    subTitle2ForAbout = "",
    subDescription2ForAbout = "",
    awardBrief = "",
    awardDetails = "",
    detailDescriptionOfAwardSectionForEligibility = "",
    requirementBrief = "",
    requirementDetails = "",
    detailDescriptionOfRequirementSectionForEligibility = "",
    dateTimesBrief = "",
    dateTimesDetails = "",
    detailDescriptionOfDateTimesForEligibility = "",
    ts = 0,
  }) {
    this.id = id;
    this.userID = userID;
    this.name = name;
    this.description = description;
    this.imageUrl = imageUrl;
    this.isOpened = isOpened;
    this.deadlines = deadlines;
    this.videoUrlForAbout = videoUrlForAbout;
    this.subTitle1ForAbout = subTitle1ForAbout;
    this.subDescription1ForAbout = subDescription1ForAbout;
    this.subTitle2ForAbout = subTitle2ForAbout;
    this.subDescription2ForAbout = subDescription2ForAbout;
    this.awardBrief = awardBrief;
    this.awardDetails = awardDetails;
    this.requirementBrief = requirementBrief;
    this.requirementDetails = requirementDetails;
    this.dateTimesBrief = dateTimesBrief;
    this.dateTimesDetails = dateTimesDetails;
    this.ts = ts;
  }

  ScholarshipModel.fromJson(Map<String, dynamic> json)
      : id = (json['id'] != null) ? json['id'] : "",
        userID = (json['userID'] != null) ? json['userID'] : "",
        name = (json['name'] != null) ? json['name'] : "",
        description = (json['description'] != null) ? json['description'] : "",
        imageUrl = (json['imageUrl'] != null) ? json['imageUrl'] : "",
        isOpened = (json['isOpened'] != null) ? json['isOpened'] : false,
        deadlines = (json['deadlines'] != null) ? json['deadlines'] : 0,
        videoUrlForAbout = (json['videoUrlForAbout'] != null) ? json['videoUrlForAbout'] : "",
        subTitle1ForAbout = (json['subTitle1ForAbout'] != null) ? json['subTitle1ForAbout'] : "",
        subDescription1ForAbout = (json['subDescription1ForAbout'] != null) ? json['subDescription1ForAbout'] : "",
        subTitle2ForAbout = (json['subTitle2ForAbout'] != null) ? json['subTitle2ForAbout'] : "",
        subDescription2ForAbout = (json['subDescription2ForAbout'] != null) ? json['subDescription2ForAbout'] : "",
        awardBrief = (json['awardBrief'] != null) ? json['awardBrief'] : "",
        awardDetails = (json['awardDetails'] != null) ? json['awardDetails'] : "",
        requirementBrief = (json['requirementBrief'] != null) ? json['requirementBrief'] : "",
        requirementDetails = (json['requirementDetails'] != null) ? json['requirementDetails'] : "",
        dateTimesBrief = (json['dateTimesBrief'] != null) ? json['dateTimesBrief'] : "",
        dateTimesDetails = (json['dateTimesDetails'] != null) ? json['dateTimesDetails'] : "",
        ts = (json['ts'] != null) ? json['ts'] : 0;

  Map<String, dynamic> toJson() {
    return {
      "id": (id == null) ? "" : id,
      "userID": (userID == null) ? "" : userID,
      "name": (name == null) ? "" : name,
      "description": (description == null) ? "" : description,
      "imageUrl": (imageUrl == null) ? "" : imageUrl,
      "isOpened": (isOpened == null) ? false : isOpened,
      "deadlines": (deadlines == null) ? 0 : deadlines,
      "videoUrlForAbout": (videoUrlForAbout == null) ? "" : videoUrlForAbout,
      "subTitle1ForAbout": (subTitle1ForAbout == null) ? "" : subTitle1ForAbout,
      "subDescription1ForAbout": (subDescription1ForAbout == null) ? "" : subDescription1ForAbout,
      "subTitle2ForAbout": (subTitle2ForAbout == null) ? "" : subTitle2ForAbout,
      "subDescription2ForAbout": (subDescription2ForAbout == null) ? "" : subDescription2ForAbout,
      "awardBrief": (awardBrief == null) ? "" : awardBrief,
      "awardDetails": (awardDetails == null) ? "" : awardDetails,
      "requirementBrief": (requirementBrief == null) ? "" : requirementBrief,
      "requirementDetails": (requirementDetails == null) ? "" : requirementDetails,
      "dateTimesBrief": (dateTimesBrief == null) ? "" : dateTimesBrief,
      "dateTimesDetails": (dateTimesDetails == null) ? "" : dateTimesDetails,
      "ts": (ts == null) ? 0 : ts,
    };
  }
}
