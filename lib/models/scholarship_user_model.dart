import 'index.dart';

class ScholarshipUserModel extends BaseModel {
  String id;
  String name;
  String avatarimageUrl;
  int ts;

  ScholarshipUserModel({
    id = "",
    name = "",
    avatarimageUrl = "",
    ts = 0,
  }) {
    this.id = id;
    this.name = name;
    this.avatarimageUrl = avatarimageUrl;
    this.ts = ts;
  }

  ScholarshipUserModel.fromJson(Map<String, dynamic> json)
      : id = (json['id'] != null) ? json['id'] : "",
        name = (json['name'] != null) ? json['name'] : "",
        avatarimageUrl = (json['avatarimageUrl'] != null) ? json['avatarimageUrl'] : "",
        ts = (json['ts'] != null) ? json['ts'] : 0;

  Map<String, dynamic> toJson() {
    return {
      "id": (id == null) ? "" : id,
      "name": (name == null) ? "" : name,
      "avatarimageUrl": (avatarimageUrl == null) ? "" : avatarimageUrl,
      "ts": (ts == null) ? 0 : ts,
    };
  }
}
