export './base_model.dart';
export './scholarship_application_model.dart';
export './scholarship_model.dart';
export './scholarship_user_model.dart';
