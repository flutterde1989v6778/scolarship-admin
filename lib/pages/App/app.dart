import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:scholarship/blocs/index.dart';
import 'package:scholarship/pages/Scholarship/Pages/EditApplicationsPage/index.dart';
import 'package:scholarship/pages/Scholarship/Pages/EditScholarshipPage/edit_scholarship_page.dart';

import './index.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AppRoutes.configureRoutes();
    return MultiProvider(
      providers: [
        BlocProvider<ThemeBloc>(create: (context) => ThemeBloc()),
      ],
      child: MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          EasyLocalization.of(context).delegate,
          const FallbackCupertinoLocalisationsDelegate(),
        ],
        supportedLocales: EasyLocalization.of(context).supportedLocales,
        locale: EasyLocalization.of(context).locale,
        debugShowCheckedModeBanner: false,
        theme: buildThemeData(context),
        onGenerateRoute: AppRoutes.router.generator,
        routes: {
          AppRoutes.newScholarship: (context) => EditScholarshipPage(category: "new"),
          AppRoutes.updateScholarship: (context) => EditScholarshipPage(category: "update"),
          AppRoutes.updateApplication: (context) => EditApplicationPage(),
        },
      ),
    );
  }
}
