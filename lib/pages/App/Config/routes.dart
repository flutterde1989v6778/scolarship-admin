/*
 * fluro
 * Created by Yakka
 * https://theyakka.com
 * 
 * Copyright (c) 2019 Yakka, LLC. All rights reserved.
 * See LICENSE for distribution and usage details.
 */
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:scholarship/models/index.dart';

import 'package:scholarship/pages/Scholarship/Pages/LoginPage/index.dart';
import 'package:scholarship/pages/Scholarship/Pages/ManageApplicationsPage/index.dart';
import 'package:scholarship/pages/Scholarship/Pages/EditScholarshipPage/index.dart';
import 'package:scholarship/pages/Scholarship/Pages/ManageScholarshipsPage/index.dart';

class AppRoutes {
  static Router router = Router();
  static String root = "/";
  static String scholarshipLogin = "/scholarship/login";
  static String manageApplications = "/scholarship/manage_applications";
  static String newScholarship = '/scholarship/new_scholarship';
  static String updateScholarship = '/scholarship/update_scholarship';
  static String manageScholarships = '/scholarship/manage_scholarhips';
  static String updateApplication = '/scholarship/update_application';

  static void configureRoutes() {
    router.notFoundHandler = Handler(handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return Center(child: Text("ROUTE WAS NOT FOUND !!!", style: TextStyle(fontSize: 30)));
    });
    router.define(root, handler: scholarshipLoginHandler);
    router.define(scholarshipLogin, handler: scholarshipLoginHandler);
    router.define(manageApplications, handler: manageApplicationsHandler);
    router.define(manageScholarships, handler: manageScholarshipsHandler);
  }
}

var scholarshipLoginHandler = Handler(handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return ScholarshipLoginPage();
});

var manageApplicationsHandler = Handler(handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return ManageApplicationsPage();
});

var manageScholarshipsHandler = Handler(handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return ManageScholarshipsPage();
});
