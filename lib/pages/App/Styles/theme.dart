import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

ThemeData buildThemeData(BuildContext context) {
  // final baseTheme = ThemeData(fontFamily: "Roboto");
  final baseTheme = ThemeData();

  return baseTheme.copyWith(
    textTheme: GoogleFonts.robotoTextTheme(
      Theme.of(context).textTheme,
    ),
  );
}
