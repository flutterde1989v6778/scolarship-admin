import 'dart:typed_data';
import 'dart:ui';
import 'package:scholarship/pages/App/index.dart';
import 'package:scholarship/utils/url_launcher.dart';
import "package:universal_html/html.dart" as html;
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart' as EL;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'package:scholarship/blocs/index.dart';
import 'package:scholarship/models/index.dart';
import 'package:scholarship/pages/Scholarship/Components/index.dart';
import 'package:scholarship/pages/Scholarship/Styles/index.dart';
import 'package:scholarship/utils/index.dart';
import 'package:scholarship/widgets/index.dart';

import 'index.dart';

class EditApplicationView extends StatefulWidget {
  EditApplicationView({@required this.scholarshipPageColors});

  ScholarshipColors scholarshipPageColors;
  String category;

  State<EditApplicationView> createState() => _EditApplicationViewState();
}

class _EditApplicationViewState extends State<EditApplicationView> with TickerProviderStateMixin {
  EditApplicationPageStyles _editApplicationPageStyles;
  ScholarshipColors _scholarshipColors;
  EditApplicationBloc _editApplicationBloc;
  GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  ScholarshipApplicationModel _scholarshipApplicationModel = ScholarshipApplicationModel();
  GlobalKey globalKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _editApplicationBloc = BlocProvider.of<EditApplicationBloc>(context)..add(ReadApplicationEvent());
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("___________EditApplicationView__build______________");
    _scholarshipColors = widget.scholarshipPageColors;

    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth >= 900) {
          _editApplicationPageStyles = EditApplicationPageDesktopStyles(context);
        } else if (constraints.maxWidth >= 600 && constraints.maxWidth < 900) {
          ////  tablet
          _editApplicationPageStyles = EditApplicationPageTabletStyles(context);
        } else if (constraints.maxWidth < 600) {
          _editApplicationPageStyles = EditApplicationPageMobileStyles(context);
        }
        print("___EditApplicationView__LayoutBuilder___${_editApplicationPageStyles.deviceWidth}_______");

        return SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Topbar(loginState: false, returnText: EditApplicationPageString.returnText),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: _editApplicationPageStyles.primaryHorizontalPadding,
                    vertical: _editApplicationPageStyles.primaryVerticalPadding,
                  ),
                  child: _containerBody(context),
                ),
                Footer(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _containerBody(BuildContext context) {
    return BlocConsumer<EditApplicationBloc, EditApplicationState>(
      listener: (context, state) async {
        if (state.saveState != 1) await CustomProgressDialog.of(context: context).hide();
      },
      builder: (context, state) {
        _scholarshipApplicationModel = state.scholarshipApplicationModel;

        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            (_editApplicationPageStyles.runtimeType != EditApplicationPageMobileStyles)
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      _containerApplicationInfo(context, state),
                      _containerEvaluationBar(context, state),
                    ],
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      _containerApplicationInfo(context, state),
                      SizedBox(height: _editApplicationPageStyles.shareWidthDp * 20),
                      _containerEvaluationBar(context, state),
                    ],
                  ),
            SizedBox(height: _editApplicationPageStyles.shareWidthDp * 50),
            Row(
              children: <Widget>[
                _containerSelectFile(context, state),
              ],
            ),
            SizedBox(height: _editApplicationPageStyles.shareWidthDp * 20),
            _form(context, state)
          ],
        );
      },
    );
  }

  Widget _containerApplicationInfo(BuildContext context, EditApplicationState editApplicationState) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          _scholarshipApplicationModel.name,
          style: TextStyle(fontSize: _editApplicationPageStyles.applicationNameFontSize, color: _scholarshipColors.secondaryColor),
        ),
        SizedBox(height: _editApplicationPageStyles.shareWidthDp * 10),
        Text(
          EditApplicationPageString.ageLabel.tr(namedArgs: {"age": _scholarshipApplicationModel.age.toString()}),
          style: TextStyle(fontSize: _editApplicationPageStyles.textFontSize, color: _scholarshipColors.textColor),
        ),
        SizedBox(height: _editApplicationPageStyles.shareWidthDp * 5),
        Text(
          EditApplicationPageString.disciplineLabel.tr(namedArgs: {"discipline": _scholarshipApplicationModel.discipline}),
          style: TextStyle(fontSize: _editApplicationPageStyles.textFontSize, color: _scholarshipColors.textColor),
        ),
        SizedBox(height: _editApplicationPageStyles.shareWidthDp * 5),
        Text(
          EditApplicationPageString.emailLabel.tr(namedArgs: {"email": _scholarshipApplicationModel.email}),
          style: TextStyle(fontSize: _editApplicationPageStyles.textFontSize, color: _scholarshipColors.textColor),
        ),
        SizedBox(height: _editApplicationPageStyles.shareWidthDp * 5),
        Text(
          EditApplicationPageString.locationLabel.tr(namedArgs: {"location": _scholarshipApplicationModel.location}),
          style: TextStyle(fontSize: _editApplicationPageStyles.textFontSize, color: _scholarshipColors.textColor),
        ),
        SizedBox(height: _editApplicationPageStyles.shareWidthDp * 5),
        Text(
          EditApplicationPageString.phoneNumberLabel.tr(namedArgs: {"phoneNumber": _scholarshipApplicationModel.phoneNumber}),
          style: TextStyle(fontSize: _editApplicationPageStyles.textFontSize, color: _scholarshipColors.textColor),
        ),
      ],
    );
  }

  Widget _containerEvaluationBar(BuildContext context, EditApplicationState editApplicationState) {
    return ScholarshipRatingBar(
      rating: _scholarshipApplicationModel.evaluation,
      spacing: _editApplicationPageStyles.shareWidthDp * 8,
      evaluatorSize: _editApplicationPageStyles.evaluatorSize,
      evaluatorLingHeight: _editApplicationPageStyles.evaluatorLingHeight,
      borderWidth: _editApplicationPageStyles.shareWidthDp * 2,
      defaultColor: _scholarshipColors.secondaryColor.withAlpha(50),
      defaultBackgroundColor: _scholarshipColors.panelBackgroundColor,
      selectedColor: _scholarshipColors.secondaryColor,
      topLabel: EditApplicationPageString.evaluationLabel,
      topLabelColor: _scholarshipColors.textColor,
      topLabelFontSize: _editApplicationPageStyles.textFontSize,
      bottomLabel: EditApplicationPageString.evaluationView,
      bottomLabelColor: _scholarshipColors.secondaryColor,
      bottomLabelFontSize: _editApplicationPageStyles.textFontSize,
      onRatedHandler: (value) {
        _scholarshipApplicationModel.evaluation = value;
      },
    );
  }

  Widget _containerSelectFile(BuildContext context, EditApplicationState editApplicationState) {
    return Container(
      width: _editApplicationPageStyles.fileSelectFieldWidth,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.video,
                color: _scholarshipColors.secondaryColor,
                size: _editApplicationPageStyles.fileIconSize,
              ),
              SizedBox(width: _editApplicationPageStyles.shareWidthDp * 10),
              Icon(
                Icons.event_note,
                color: _scholarshipColors.secondaryColor,
                size: _editApplicationPageStyles.fileIconSize,
              ),
              SizedBox(width: _editApplicationPageStyles.shareWidthDp * 10),
              FaIcon(
                FontAwesomeIcons.filePdf,
                color: _scholarshipColors.secondaryColor,
                size: _editApplicationPageStyles.fileIconSize,
              ),
              SizedBox(width: _editApplicationPageStyles.shareWidthDp * 20),
              Text(
                EditApplicationPageString.additionalFileLabel,
                style: TextStyle(fontSize: _editApplicationPageStyles.textFontSize, color: _scholarshipColors.secondaryColor),
              ),
            ],
          ),
          SizedBox(height: _editApplicationPageStyles.shareWidthDp * 10),
          GestureDetector(
            onTap: () {
              openURL(_scholarshipApplicationModel.videoUrl);
            },
            child: Container(
              width: _editApplicationPageStyles.fileSelectFieldWidth,
              padding: EdgeInsets.symmetric(
                horizontal: _editApplicationPageStyles.fileSelectFieldHorizontalPadding,
                vertical: _editApplicationPageStyles.fileSelectFieldVerticalPadding,
              ),
              decoration: BoxDecoration(
                  border: Border.all(width: 2, color: _scholarshipColors.secondaryColor),
                  borderRadius: BorderRadius.circular(3),
                  color: _scholarshipColors.panelBackgroundColor),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      FaIcon(
                        FontAwesomeIcons.video,
                        color: _scholarshipColors.secondaryColor,
                        size: _editApplicationPageStyles.fileIconSize,
                      ),
                      SizedBox(width: _editApplicationPageStyles.shareWidthDp * 10),
                      Text(
                        _scholarshipApplicationModel.videoUrl.split(Validators.storageFileName)[1],
                        style: TextStyle(fontSize: _editApplicationPageStyles.textFontSize, color: Colors.white),
                        maxLines: 1,
                        overflow: TextOverflow.clip,
                      )
                    ],
                  ),
                  Icon(Icons.cloud_download, size: _editApplicationPageStyles.fileIconSize * 0.8, color: _scholarshipColors.secondaryColor)
                ],
              ),
            ),
          ),
          SizedBox(height: _editApplicationPageStyles.shareWidthDp * 10),
          GestureDetector(
            onTap: () {
              openURL(_scholarshipApplicationModel.letter1Url);
            },
            child: Container(
              width: _editApplicationPageStyles.fileSelectFieldWidth,
              padding: EdgeInsets.symmetric(
                horizontal: _editApplicationPageStyles.fileSelectFieldHorizontalPadding,
                vertical: _editApplicationPageStyles.fileSelectFieldVerticalPadding,
              ),
              decoration: BoxDecoration(
                  border: Border.all(width: 2, color: _scholarshipColors.secondaryColor),
                  borderRadius: BorderRadius.circular(3),
                  color: _scholarshipColors.panelBackgroundColor),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.event_note,
                        color: _scholarshipColors.secondaryColor,
                        size: _editApplicationPageStyles.fileIconSize,
                      ),
                      SizedBox(width: _editApplicationPageStyles.shareWidthDp * 10),
                      Text(
                        _scholarshipApplicationModel.letter1Url.split(Validators.storageFileName)[1],
                        style: TextStyle(fontSize: _editApplicationPageStyles.textFontSize, color: Colors.white),
                        maxLines: 1,
                        overflow: TextOverflow.clip,
                      )
                    ],
                  ),
                  Icon(Icons.cloud_download, size: _editApplicationPageStyles.fileIconSize * 0.8, color: _scholarshipColors.secondaryColor)
                ],
              ),
            ),
          ),
          SizedBox(height: _editApplicationPageStyles.shareWidthDp * 10),
          GestureDetector(
            onTap: () {
              openURL(_scholarshipApplicationModel.letter2Url);
            },
            child: Container(
              width: _editApplicationPageStyles.fileSelectFieldWidth,
              padding: EdgeInsets.symmetric(
                horizontal: _editApplicationPageStyles.fileSelectFieldHorizontalPadding,
                vertical: _editApplicationPageStyles.fileSelectFieldVerticalPadding,
              ),
              decoration: BoxDecoration(
                  border: Border.all(width: 2, color: _scholarshipColors.secondaryColor),
                  borderRadius: BorderRadius.circular(3),
                  color: _scholarshipColors.panelBackgroundColor),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      FaIcon(
                        FontAwesomeIcons.filePdf,
                        color: _scholarshipColors.secondaryColor,
                        size: _editApplicationPageStyles.fileIconSize,
                      ),
                      SizedBox(width: _editApplicationPageStyles.shareWidthDp * 10),
                      Text(
                        _scholarshipApplicationModel.letter2Url.split(Validators.storageFileName)[1],
                        style: TextStyle(fontSize: _editApplicationPageStyles.textFontSize, color: Colors.white),
                        maxLines: 1,
                        overflow: TextOverflow.clip,
                      )
                    ],
                  ),
                  Icon(Icons.cloud_download, size: _editApplicationPageStyles.fileIconSize * 0.8, color: _scholarshipColors.secondaryColor)
                ],
              ),
            ),
          ),
          SizedBox(height: _editApplicationPageStyles.shareWidthDp * 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text(
                EditApplicationPageString.downloadAllLabel,
                style: TextStyle(fontSize: _editApplicationPageStyles.textFontSize, color: _scholarshipColors.secondaryColor),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _form(BuildContext context, EditApplicationState editApplicationState) {
    return Container(
      child: Form(
        key: _formkey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            ScholarshipTextFormField(
              initialValue: _scholarshipApplicationModel.evaluatorName,
              width: _editApplicationPageStyles.textFieldWidth,
              height: _editApplicationPageStyles.textFontSize,
              autoHeight: true,
              lineSpacing: _editApplicationPageStyles.shareWidthDp * 3,
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              maxLines: null,
              fillColor: _scholarshipColors.backgroundColor,
              labelFontSize: _editApplicationPageStyles.textFontSize,
              labelColor: _scholarshipColors.textColor,
              textFontSize: _editApplicationPageStyles.textFontSize,
              textColor: _scholarshipColors.textColor,
              borderType: 1,
              borderColor: _scholarshipColors.textColor,
              hintTextFontSize: _editApplicationPageStyles.textFontSize,
              hintTextColor: _scholarshipColors.hintTextColor,
              labelSpacing: _editApplicationPageStyles.shareWidthDp * 5,
              iconSize: _editApplicationPageStyles.validateIconSize,
              validateIconColor: _scholarshipColors.formValidatedColor,
              unValidateIconColor: _scholarshipColors.textColor,
              labelText: EditApplicationPageString.evaluationLabel,
              hintText: EditApplicationPageString.evaluatorNameHintText,
              onChangeHandler: (input) => _scholarshipApplicationModel.evaluatorName,
              validatorHandler: (input) => (input.length < 3) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "3"}) : null,
              onSaveHandler: (input) => _scholarshipApplicationModel.evaluatorName = input,
            ),
            //////
            //////
            SizedBox(height: _editApplicationPageStyles.shareWidthDp * 20),
            CustomDropDownFormField(
              context: context,
              width: _editApplicationPageStyles.textFieldWidth,
              height: _editApplicationPageStyles.textFontSize * 2,
              border: Border(bottom: BorderSide(color: _scholarshipColors.textColor)),
              errorBorder: Border(bottom: BorderSide(color: Colors.red)),
              items: [
                {"value": true, "text": "Yes"},
                {"value": false, "text": "No"},
              ],
              isPrefixIconOutofField: true,
              iconSpacing: _editApplicationPageStyles.shareWidthDp * 10,
              prefixIcons: <Widget>[
                Column(
                  children: <Widget>[
                    Icon(
                      Icons.check_circle_outline,
                      size: _editApplicationPageStyles.validateIconSize,
                      color: _scholarshipColors.formValidatedColor,
                    ),
                    SizedBox(height: _editApplicationPageStyles.shareWidthDp * 10),
                    Icon(
                      Icons.edit,
                      size: _editApplicationPageStyles.validateIconSize,
                      color: _scholarshipColors.textColor,
                    )
                  ],
                ),
                Column(
                  children: <Widget>[
                    Icon(
                      Icons.check_circle_outline,
                      size: _editApplicationPageStyles.validateIconSize,
                      color: _scholarshipColors.textColor,
                    ),
                    SizedBox(height: _editApplicationPageStyles.shareWidthDp * 10),
                    Icon(
                      Icons.edit,
                      size: _editApplicationPageStyles.validateIconSize,
                      color: _scholarshipColors.textColor,
                    )
                  ],
                )
              ],
              fillColor: _scholarshipColors.backgroundColor,
              isRequired: true,
              label: EditApplicationPageString.minimunQualificationsLabel,
              labelFontSize: _editApplicationPageStyles.textFontSize,
              labelColor: _scholarshipColors.textColor,
              labelSpacing: _editApplicationPageStyles.shareWidthDp * 5,
              dropdownColor: _scholarshipColors.panelBackgroundColor,
              itemTextFontSize: _editApplicationPageStyles.textFontSize,
              itemTextColor: _scholarshipColors.textColor,
              hintText: EditApplicationPageString.minimunQualificationsHintText,
              hintTextColor: _scholarshipColors.hintTextColor,
              value: _scholarshipApplicationModel.minimumQualifications,
              onChangeHandler: (input) => _scholarshipApplicationModel.minimumQualifications,
              validatorHandler: (input) {
                print("validatorHandler: $input");
                return input == null ? ScholarshipString.normalValidateString : null;
              },
              onSaveHandler: (input) => _scholarshipApplicationModel.minimumQualifications = input,
            ),
            //////
            //////
            SizedBox(height: _editApplicationPageStyles.shareWidthDp * 20),
            ScholarshipTextFormField(
              initialValue: _scholarshipApplicationModel.backgroundFits,
              width: _editApplicationPageStyles.textFieldWidth,
              height: _editApplicationPageStyles.textFontSize,
              autoHeight: true,
              lineSpacing: _editApplicationPageStyles.shareWidthDp * 3,
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              maxLines: null,
              fillColor: _scholarshipColors.backgroundColor,
              labelFontSize: _editApplicationPageStyles.textFontSize,
              labelColor: _scholarshipColors.textColor,
              textFontSize: _editApplicationPageStyles.textFontSize,
              textColor: _scholarshipColors.textColor,
              borderType: 1,
              borderColor: _scholarshipColors.textColor,
              hintTextFontSize: _editApplicationPageStyles.textFontSize,
              hintTextColor: _scholarshipColors.hintTextColor,
              labelSpacing: _editApplicationPageStyles.shareWidthDp * 5,
              iconSize: _editApplicationPageStyles.validateIconSize,
              validateIconColor: _scholarshipColors.formValidatedColor,
              unValidateIconColor: _scholarshipColors.textColor,
              labelText: EditApplicationPageString.backgroundFitsLabel,
              hintText: EditApplicationPageString.backgroundFitsHintText,
              onChangeHandler: (input) => _scholarshipApplicationModel.backgroundFits,
              validatorHandler: (input) => (input.length < 10) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "10"}) : null,
              onSaveHandler: (input) => _scholarshipApplicationModel.backgroundFits = input,
            ),
            //////
            //////
            SizedBox(height: _editApplicationPageStyles.shareWidthDp * 20),
            CustomDropDownFormField(
              context: context,
              width: _editApplicationPageStyles.textFieldWidth,
              height: _editApplicationPageStyles.textFontSize * 2,
              border: Border(bottom: BorderSide(color: _scholarshipColors.textColor)),
              errorBorder: Border(bottom: BorderSide(color: Colors.red)),
              items: [
                {"value": 1, "text": "1"},
                {"value": 2, "text": "2"},
                {"value": 3, "text": "3"},
                {"value": 4, "text": "4"},
                {"value": 5, "text": "5"},
              ],
              isPrefixIconOutofField: true,
              iconSpacing: _editApplicationPageStyles.shareWidthDp * 10,
              prefixIcons: <Widget>[
                Column(
                  children: <Widget>[
                    Icon(
                      Icons.check_circle_outline,
                      size: _editApplicationPageStyles.validateIconSize,
                      color: _scholarshipColors.formValidatedColor,
                    ),
                    SizedBox(height: _editApplicationPageStyles.shareWidthDp * 10),
                    Icon(
                      Icons.edit,
                      size: _editApplicationPageStyles.validateIconSize,
                      color: _scholarshipColors.textColor,
                    )
                  ],
                ),
                Column(
                  children: <Widget>[
                    Icon(
                      Icons.check_circle_outline,
                      size: _editApplicationPageStyles.validateIconSize,
                      color: _scholarshipColors.textColor,
                    ),
                    SizedBox(height: _editApplicationPageStyles.shareWidthDp * 10),
                    Icon(
                      Icons.edit,
                      size: _editApplicationPageStyles.validateIconSize,
                      color: _scholarshipColors.textColor,
                    )
                  ],
                )
              ],
              fillColor: _scholarshipColors.backgroundColor,
              labelFontSize: _editApplicationPageStyles.textFontSize,
              labelColor: _scholarshipColors.textColor,
              labelSpacing: _editApplicationPageStyles.shareWidthDp * 5,
              dropdownColor: _scholarshipColors.panelBackgroundColor,
              itemTextFontSize: _editApplicationPageStyles.textFontSize,
              itemTextColor: _scholarshipColors.textColor,
              isRequired: true,
              label: EditApplicationPageString.scoreLabel,
              hintText: EditApplicationPageString.scoreHintText,
              hintTextColor: _scholarshipColors.hintTextColor,
              value: _scholarshipApplicationModel.score,
              onChangeHandler: (input) => _scholarshipApplicationModel.score,
              validatorHandler: (input) {
                print("validatorHandler: $input");
                return input == null ? ScholarshipString.normalValidateString : null;
              },
              onSaveHandler: (input) => _scholarshipApplicationModel.score = input,
            ),
            //////
            //////
            SizedBox(height: _editApplicationPageStyles.shareWidthDp * 20),
            ScholarshipTextFormField(
              initialValue: _scholarshipApplicationModel.comments,
              width: _editApplicationPageStyles.textFieldWidth,
              height: _editApplicationPageStyles.textFontSize,
              autoHeight: true,
              lineSpacing: _editApplicationPageStyles.shareWidthDp * 3,
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              maxLines: null,
              fillColor: _scholarshipColors.backgroundColor,
              labelFontSize: _editApplicationPageStyles.textFontSize,
              labelColor: _scholarshipColors.textColor,
              textFontSize: _editApplicationPageStyles.textFontSize,
              textColor: _scholarshipColors.textColor,
              borderType: 1,
              borderColor: _scholarshipColors.textColor,
              hintTextFontSize: _editApplicationPageStyles.textFontSize,
              hintTextColor: _scholarshipColors.hintTextColor,
              labelSpacing: _editApplicationPageStyles.shareWidthDp * 5,
              iconSize: _editApplicationPageStyles.validateIconSize,
              validateIconColor: _scholarshipColors.formValidatedColor,
              unValidateIconColor: _scholarshipColors.textColor,
              labelText: EditApplicationPageString.commentsLabel,
              hintText: EditApplicationPageString.commentsHintText,
              onChangeHandler: (input) => _scholarshipApplicationModel.comments,
              validatorHandler: (input) => (input.length < 10) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "10"}) : null,
              onSaveHandler: (input) => _scholarshipApplicationModel.comments = input,
            ),
            //////
            //////
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CustomRaisedButton(
                  width: _editApplicationPageStyles.buttonWidth,
                  height: _editApplicationPageStyles.textFontSize * 2,
                  color: _scholarshipColors.cancelButtonColor,
                  borderRadius: 0,
                  borderColor: _scholarshipColors.cancelButtonColor,
                  child: Text(
                    EditApplicationPageString.cancelButtonText,
                    style: TextStyle(fontSize: _editApplicationPageStyles.textFontSize, color: Colors.black),
                  ),
                  onPressed: () {},
                ),
                SizedBox(width: _editApplicationPageStyles.shareWidthDp * 20),
                CustomRaisedButton(
                  width: _editApplicationPageStyles.buttonWidth,
                  height: _editApplicationPageStyles.textFontSize * 2,
                  color: _scholarshipColors.secondaryColor,
                  borderRadius: 0,
                  borderColor: _scholarshipColors.secondaryColor,
                  child: Text(
                    EditApplicationPageString.submitButtonText,
                    style: TextStyle(fontSize: _editApplicationPageStyles.textFontSize, color: Colors.white),
                  ),
                  onPressed: () {
                    _submit(context);
                  },
                ),
              ],
            ),
            SizedBox(height: _editApplicationPageStyles.shareWidthDp * 15),
            (editApplicationState.saveState == -1)
                ? Text(
                    EditApplicationPageString.updateApplicationFail,
                    style: TextStyle(fontSize: _editApplicationPageStyles.textFontSize, color: Colors.red),
                  )
                : (editApplicationState.saveState == 2)
                    ? Text(
                        EditApplicationPageString.updateApplicationSuccess,
                        style: TextStyle(fontSize: _editApplicationPageStyles.textFontSize, color: Colors.green),
                      )
                    : SizedBox(),
          ],
        ),
      ),
    );
  }

  void _submit(BuildContext context) async {
    if (!_formkey.currentState.validate()) return;
    _formkey.currentState.save();
    try {
      await CustomProgressDialog.of(context: context).show();
      _editApplicationBloc.add(SaveStateForApplicationEvent(saveState: 1));
      _editApplicationBloc.add(UpdateApplicationEvent(updateScholarshipApplicationModel: _scholarshipApplicationModel));
    } catch (e) {
      print(e);
    }
  }
}
