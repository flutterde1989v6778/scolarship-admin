import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scholarship/blocs/index.dart';
import 'package:scholarship/models/scholarship_application_model.dart';
import 'package:scholarship/models/scholarship_model.dart';
import 'package:scholarship/pages/Scholarship/Styles/index.dart';
import 'package:scholarship/widgets/SettingMenu/index.dart';

import './index.dart';

class EditApplicationPage extends StatelessWidget {
  EditApplicationPage();

  ScholarshipApplicationModel initScholarshipApplicationModel;
  String scholarshipModelID;
  ScholarshipColors _scholarshipColors;

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> params = ModalRoute.of(context).settings.arguments;
    if (params["initScholarshipApplicationModel"] != null) initScholarshipApplicationModel = params["initScholarshipApplicationModel"];
    if (params["scholarshipModelID"] != null) scholarshipModelID = params["scholarshipModelID"];

    if (initScholarshipApplicationModel == null) initScholarshipApplicationModel = ScholarshipApplicationModel();

    return Material(
      color: Colors.transparent,
      child: BlocBuilder<ThemeBloc, ThemeState>(
        builder: (context, state) {
          if (state.themeMode == ThemeModeConstants.dark) {
            _scholarshipColors = ScholarshipDarkModeColors();
          } else {
            _scholarshipColors = ScholarshipLightModeColors();
          }
          return Scaffold(
            appBar: AppBar(
              title: Text('Scholarship Admin'),
              backgroundColor: Color(0xFF232323),
              actions: <Widget>[
                SettingMenu(),
              ],
            ),
            backgroundColor: _scholarshipColors.backgroundColor,
            body: MultiBlocProvider(
              providers: [
                BlocProvider<EditApplicationBloc>(
                  create: (context) => EditApplicationBloc(
                    initScholarshipApplicationModel: initScholarshipApplicationModel,
                    scholarshipModelID: scholarshipModelID,
                  ),
                ),
              ],
              child: EditApplicationView(scholarshipPageColors: _scholarshipColors),
            ),
          );
        },
      ),
    );
  }
}
