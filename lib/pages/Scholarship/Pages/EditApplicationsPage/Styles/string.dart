import 'package:easy_localization/easy_localization.dart' as EL;

class EditApplicationPageString {
  static String returnText = "EditApplicationPage.returnText".tr();
  static String ageLabel = "EditApplicationPage.ageLabel".tr();
  static String disciplineLabel = "EditApplicationPage.disciplineLabel".tr();
  static String emailLabel = "EditApplicationPage.emailLabel".tr();
  static String locationLabel = "EditApplicationPage.locationLabel".tr();
  static String phoneNumberLabel = "EditApplicationPage.phoneNumberLabel".tr();
  static String evaluationLabel = "EditApplicationPage.evaluationLabel".tr();
  static String evaluationView = "EditApplicationPage.evaluationView".tr();
  static String additionalFileLabel = "EditApplicationPage.additionalFileLabel".tr();
  static String downloadAllLabel = "EditApplicationPage.downloadAllLabel".tr();
  static String evaluatorNameLabel = "EditApplicationPage.evaluatorNameLabel".tr();
  static String evaluatorNameHintText = "EditApplicationPage.evaluatorNameHintText".tr();
  static String minimunQualificationsLabel = "EditApplicationPage.minimunQualificationsLabel".tr();
  static String minimunQualificationsHintText = "EditApplicationPage.minimunQualificationsHintText".tr();
  static String backgroundFitsLabel = "EditApplicationPage.backgroundFitsLabel".tr();
  static String backgroundFitsHintText = "EditApplicationPage.backgroundFitsHintText".tr();
  static String scoreLabel = "EditApplicationPage.scoreLabel".tr();
  static String scoreHintText = "EditApplicationPage.scoreHintText".tr();
  static String commentsLabel = "EditApplicationPage.commentsLabel".tr();
  static String commentsHintText = "EditApplicationPage.commentsHintText".tr();

  static String cancelButtonText = "EditApplicationPage.cancelButtonText".tr();
  static String submitButtonText = "EditApplicationPage.submitButtonText".tr();
  static String updateApplicationFail = "EditApplicationPage.updateApplicationFail".tr();
  static String updateApplicationSuccess = "EditApplicationPage.updateApplicationSuccess".tr();
}
