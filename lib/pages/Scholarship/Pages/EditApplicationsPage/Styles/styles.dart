import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:scholarship/Config/resposible_settings.dart';

class EditApplicationPageStyles {
  double devicePixelRatio;
  double deviceWidth;
  double deviceHeight;
  double fontUnit;
  double statusbarHeight;
  double bottombarHeight;
  double appbarHeight;
  double safeAreaHeight;
  double shareWidth;
  double shareHeight;
  double shareWidthDp;
  double shareHeightDp;

  double primaryHorizontalPadding;
  double primaryVerticalPadding;

  double applicationNameFontSize;
  double textFontSize;
  double fileNameFontSize;

  double evaluatorSize;
  double evaluatorLingHeight;

  double fileSelectFieldWidth;
  double fileIconSize;
  double fileSelectFieldHorizontalPadding;
  double fileSelectFieldVerticalPadding;

  double textFieldWidth;
  double validateIconSize;

  double buttonWidth;

  EditApplicationPageStyles(BuildContext context) {}
}

class EditApplicationPageDesktopStyles extends EditApplicationPageStyles {
  EditApplicationPageDesktopStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(190);
    primaryVerticalPadding = ScreenUtil().setWidth(10);

    applicationNameFontSize = ScreenUtil().setSp(20, allowFontScalingSelf: false);
    textFontSize = ScreenUtil().setSp(14, allowFontScalingSelf: false);
    fileNameFontSize = ScreenUtil().setSp(12, allowFontScalingSelf: false);

    evaluatorSize = ScreenUtil().setWidth(46);
    evaluatorLingHeight = ScreenUtil().setWidth(6);

    fileSelectFieldWidth = ScreenUtil().setWidth(400);
    fileIconSize = ScreenUtil().setWidth(25);
    fileSelectFieldHorizontalPadding = ScreenUtil().setWidth(15);
    fileSelectFieldVerticalPadding = ScreenUtil().setWidth(10);

    textFieldWidth = deviceWidth - primaryHorizontalPadding * 2;
    validateIconSize = ScreenUtil().setWidth(20);
    buttonWidth = ScreenUtil().setWidth(100);
  }
}

class EditApplicationPageTabletStyles extends EditApplicationPageStyles {
  EditApplicationPageTabletStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(100);
    primaryVerticalPadding = ScreenUtil().setWidth(10);

    applicationNameFontSize = ScreenUtil().setSp(20, allowFontScalingSelf: false);
    textFontSize = ScreenUtil().setSp(14, allowFontScalingSelf: false);
    fileNameFontSize = ScreenUtil().setSp(12, allowFontScalingSelf: false);

    evaluatorSize = ScreenUtil().setWidth(46);
    evaluatorLingHeight = ScreenUtil().setWidth(6);

    fileIconSize = ScreenUtil().setWidth(25);
    fileSelectFieldHorizontalPadding = ScreenUtil().setWidth(15);
    fileSelectFieldVerticalPadding = ScreenUtil().setWidth(10);

    textFieldWidth = deviceWidth - primaryHorizontalPadding * 2;
    validateIconSize = ScreenUtil().setWidth(20);
    buttonWidth = ScreenUtil().setWidth(100);
  }
}

class EditApplicationPageMobileStyles extends EditApplicationPageStyles {
  EditApplicationPageMobileStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.mobileDesignWidth,
      height: ResponsibleDesignSettings.mobileDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(16);
    primaryVerticalPadding = ScreenUtil().setWidth(5);

    applicationNameFontSize = ScreenUtil().setSp(20, allowFontScalingSelf: false);
    textFontSize = ScreenUtil().setSp(14, allowFontScalingSelf: false);
    fileNameFontSize = ScreenUtil().setSp(10, allowFontScalingSelf: false);

    evaluatorSize = ScreenUtil().setWidth(46);
    evaluatorLingHeight = ScreenUtil().setWidth(6);

    fileSelectFieldWidth = deviceWidth - primaryHorizontalPadding * 2;
    fileIconSize = ScreenUtil().setWidth(25);
    fileSelectFieldHorizontalPadding = ScreenUtil().setWidth(15);
    fileSelectFieldVerticalPadding = ScreenUtil().setWidth(10);

    textFieldWidth = deviceWidth - primaryHorizontalPadding * 2;
    validateIconSize = ScreenUtil().setWidth(20);
    buttonWidth = ScreenUtil().setWidth(100);
  }
}
