import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:scholarship/Config/resposible_settings.dart';

class EditScholarshipPageStyles {
  double devicePixelRatio;
  double deviceWidth;
  double deviceHeight;
  double fontUnit;
  double statusbarHeight;
  double bottombarHeight;
  double appbarHeight;
  double safeAreaHeight;
  double shareWidth;
  double shareHeight;
  double shareWidthDp;
  double shareHeightDp;

  double primaryHorizontalPadding;
  double primaryVerticalPadding;

  /// title style
  double titleHorizontalPadding;
  double titleFontSize;

  //// for styles
  double formHorizontalPadding;
  double formVerticalPadding;
  double textFieldWidth;
  double formTextFontSize;
  double iconSize;
  double buttonWidth;
  double imageSize;

  EditScholarshipPageStyles(BuildContext context) {}
}

class EditScholarshipPageDesktopStyles extends EditScholarshipPageStyles {
  EditScholarshipPageDesktopStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(0);
    primaryVerticalPadding = ScreenUtil().setWidth(10);

    /// title style
    titleHorizontalPadding = ScreenUtil().setWidth(33);
    titleFontSize = ScreenUtil().setSp(38);

    //// for styles
    formHorizontalPadding = ScreenUtil().setWidth(100);
    formVerticalPadding = ScreenUtil().setWidth(30);
    textFieldWidth = deviceWidth - formHorizontalPadding * 2;
    formTextFontSize = ScreenUtil().setSp(14);
    iconSize = ScreenUtil().setWidth(20);
    buttonWidth = ScreenUtil().setWidth(100);
    imageSize = ScreenUtil().setWidth(300);
  }
}

class EditScholarshipPageTabletStyles extends EditScholarshipPageStyles {
  EditScholarshipPageTabletStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(0);
    primaryVerticalPadding = ScreenUtil().setWidth(10);

    /// title style
    titleHorizontalPadding = ScreenUtil().setWidth(33);
    titleFontSize = ScreenUtil().setSp(38);

    //// for styles
    formHorizontalPadding = ScreenUtil().setWidth(100);
    formVerticalPadding = ScreenUtil().setWidth(30);
    textFieldWidth = deviceWidth - formHorizontalPadding * 2;
    formTextFontSize = ScreenUtil().setSp(14);
    iconSize = ScreenUtil().setWidth(20);
    buttonWidth = ScreenUtil().setWidth(100);
    imageSize = ScreenUtil().setWidth(300);
  }
}

class EditScholarshipPageMobileStyles extends EditScholarshipPageStyles {
  EditScholarshipPageMobileStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.mobileDesignWidth,
      height: ResponsibleDesignSettings.mobileDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(0);
    primaryVerticalPadding = ScreenUtil().setWidth(5);

    /// mainStyle
    titleHorizontalPadding = ScreenUtil().setWidth(16);
    titleFontSize = ScreenUtil().setSp(24);

    formHorizontalPadding = ScreenUtil().setWidth(20);
    formVerticalPadding = ScreenUtil().setWidth(20);
    textFieldWidth = deviceWidth - formHorizontalPadding * 2;
    formTextFontSize = ScreenUtil().setSp(14);
    iconSize = ScreenUtil().setWidth(20);
    buttonWidth = ScreenUtil().setWidth(100);
    imageSize = textFieldWidth - ScreenUtil().setWidth(50);
  }
}
