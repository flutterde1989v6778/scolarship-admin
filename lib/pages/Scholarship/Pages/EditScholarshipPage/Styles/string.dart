import 'package:easy_localization/easy_localization.dart' as EL;

class EditScholarshipPageString {
  static String newTitle = "EditScholarshipPage.newTitle".tr();
  static String updateTitle = "EditScholarshipPage.updateTitle".tr();
  static String nameLabel = "EditScholarshipPage.nameLabel".tr();
  static String nameHintText = "EditScholarshipPage.nameHintText".tr();
  static String descriptionLabel = "EditScholarshipPage.descriptionLabel".tr();
  static String descriptionHintText = "EditScholarshipPage.descriptionHintText".tr();
  static String deadlineLabel = "EditScholarshipPage.deadlineLabel".tr();
  static String deadlineHintText = "EditScholarshipPage.deadlineHintText".tr();
  static String subTitle1ForAboutLabel = "EditScholarshipPage.subTitle1ForAboutLabel".tr();
  static String subTitle1ForAboutHintText = "EditScholarshipPage.subTitle1ForAboutHintText".tr();
  static String subDescription1ForAboutLabel = "EditScholarshipPage.subDescription1ForAboutLabel".tr();
  static String subDescription1ForAboutHintText = "EditScholarshipPage.subDescription1ForAboutHintText".tr();
  static String subTitle2ForAboutLabel = "EditScholarshipPage.subTitle2ForAboutLabel".tr();
  static String subTitle2ForAboutHintText = "EditScholarshipPage.subTitle2ForAboutHintText".tr();
  static String subDescription2ForAboutLabel = "EditScholarshipPage.subDescription2ForAboutLabel".tr();
  static String subDescription2ForAboutHintText = "EditScholarshipPage.subDescription2ForAboutHintText".tr();
  static String awardBriefLabel = "EditScholarshipPage.awardBriefLabel".tr();
  static String awardBriefHintText = "EditScholarshipPage.awardBriefHintText".tr();
  static String awardDetailsLabel = "EditScholarshipPage.awardDetailsLabel".tr();
  static String awardDetailsHintText = "EditScholarshipPage.awardDetailsHintText".tr();
  static String requirementBriefLabel = "EditScholarshipPage.requirementBriefLabel".tr();
  static String requirementBriefHintText = "EditScholarshipPage.requirementBriefHintText".tr();
  static String requirementDetailsLabel = "EditScholarshipPage.requirementDetailsLabel".tr();
  static String requirementDetailsHintText = "EditScholarshipPage.requirementDetailsHintText".tr();
  static String dateTimesBriefLabel = "EditScholarshipPage.dateTimesBriefLabel".tr();
  static String dateTimesBriefHintText = "EditScholarshipPage.dateTimesBriefHintText".tr();
  static String dateTimesDetailsLabel = "EditScholarshipPage.dateTimesDetailsLabel".tr();
  static String dateTimesDetailsHintText = "EditScholarshipPage.dateTimesDetailsHintText".tr();
  static String videoUrlLabel = "EditScholarshipPage.videoUrlLabel".tr();
  static String videoUrlHintText = "EditScholarshipPage.videoUrlHintText".tr();
  static String imageLabel = "EditScholarshipPage.imageLabel".tr();
  static String addScholarshipFail = "EditScholarshipPage.addScholarshipFail".tr();
  static String addScholarshipSuccess = "EditScholarshipPage.addScholarshipSuccess".tr();

  static String cancelButtonText = "EditScholarshipPage.cancelButtonText".tr();
  static String submitButtonText = "EditScholarshipPage.submitButtonText".tr();
}
