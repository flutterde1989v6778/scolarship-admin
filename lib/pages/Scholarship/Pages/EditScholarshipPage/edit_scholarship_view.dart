import 'dart:typed_data';
import 'dart:ui';
import 'package:scholarship/pages/App/index.dart';
import "package:universal_html/html.dart" as html;
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart' as EL;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:scholarship/blocs/index.dart';
import 'package:scholarship/models/index.dart';
import 'package:scholarship/pages/Scholarship/Components/index.dart';
import 'package:scholarship/pages/Scholarship/Styles/index.dart';
import 'package:scholarship/utils/index.dart';
import 'package:scholarship/widgets/index.dart';

import 'index.dart';

class EditScholarshipView extends StatefulWidget {
  EditScholarshipView({@required this.scholarshipPageColors, @required this.category});

  ScholarshipColors scholarshipPageColors;
  String category;

  State<EditScholarshipView> createState() => _EditScholarshipViewState();
}

class _EditScholarshipViewState extends State<EditScholarshipView> with TickerProviderStateMixin {
  EditScholarshipPageStyles _editScholarshipPageStyles;
  ScholarshipColors _scholarshipColors;
  EditScholarshipBloc _editScholarshipBloc;
  GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  ScholarshipModel _scholarshipModel = ScholarshipModel();
  GlobalKey globalKey = GlobalKey();

  html.File _imageFile;
  Uint8List _imageFileByteData;

  @override
  void initState() {
    super.initState();
    _editScholarshipBloc = BlocProvider.of<EditScholarshipBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("___________EditScholarshipView__build______________");
    _scholarshipColors = widget.scholarshipPageColors;

    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth >= 900) {
          _editScholarshipPageStyles = EditScholarshipPageDesktopStyles(context);
        } else if (constraints.maxWidth >= 600 && constraints.maxWidth < 900) {
          ////  tablet
          _editScholarshipPageStyles = EditScholarshipPageTabletStyles(context);
        } else if (constraints.maxWidth < 600) {
          _editScholarshipPageStyles = EditScholarshipPageMobileStyles(context);
        }
        print("___EditScholarshipView__LayoutBuilder___${_editScholarshipPageStyles.deviceWidth}_______");

        return SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Topbar(loginState: true),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: _editScholarshipPageStyles.primaryHorizontalPadding,
                    vertical: _editScholarshipPageStyles.primaryVerticalPadding,
                  ),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: _editScholarshipPageStyles.titleHorizontalPadding),
                            child: Text(
                              (widget.category == "new") ? EditScholarshipPageString.newTitle : EditScholarshipPageString.updateTitle,
                              style: TextStyle(fontSize: _editScholarshipPageStyles.titleFontSize, color: _scholarshipColors.textColor),
                            ),
                          )
                        ],
                      ),
                      _containerBody(context),
                    ],
                  ),
                ),
                Footer(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _containerBody(BuildContext context) {
    return BlocConsumer<EditScholarshipBloc, EditScholarshipState>(
      listener: (context, state) async {
        if (state.saveState != 1) await CustomProgressDialog.of(context: context).hide();
        if (state.saveState == 2 && widget.category == "new") {
          Navigator.of(context).pushReplacementNamed(
            AppRoutes.newScholarship,
            arguments: {
              "category": "edit",
              "initScholarshipModel": state.scholarshipModel,
            },
          );
        }
      },
      builder: (context, state) {
        _scholarshipModel = state.scholarshipModel;
        _imageFile = state.scholarshipImageFile;
        _imageFileByteData = state.scholarshipmageFileByteData;

        return Form(
          key: _formkey,
          child: Container(
            width: _editScholarshipPageStyles.textFieldWidth,
            padding: EdgeInsets.symmetric(
              horizontal: _editScholarshipPageStyles.formHorizontalPadding,
              vertical: _editScholarshipPageStyles.formVerticalPadding,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                //// scholarship name
                ScholarshipTextFormField(
                  initialValue: _scholarshipModel.name,
                  width: _editScholarshipPageStyles.textFieldWidth,
                  height: _editScholarshipPageStyles.formTextFontSize,
                  autoHeight: true,
                  lineSpacing: _editScholarshipPageStyles.shareWidthDp * 3,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  maxLines: null,
                  fillColor: _scholarshipColors.backgroundColor,
                  labelFontSize: _editScholarshipPageStyles.formTextFontSize,
                  labelColor: _scholarshipColors.textColor,
                  textFontSize: _editScholarshipPageStyles.formTextFontSize,
                  textColor: _scholarshipColors.textColor,
                  borderType: 1,
                  borderColor: _scholarshipColors.textColor,
                  hintTextFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextColor: _scholarshipColors.hintTextColor,
                  labelSpacing: _editScholarshipPageStyles.shareWidthDp * 5,
                  iconSize: _editScholarshipPageStyles.iconSize,
                  validateIconColor: _scholarshipColors.formValidatedColor,
                  unValidateIconColor: _scholarshipColors.textColor,
                  labelText: EditScholarshipPageString.nameLabel,
                  hintText: EditScholarshipPageString.nameHintText,
                  onChangeHandler: (input) => _scholarshipModel.name = input,
                  validatorHandler: (input) => (input.length < 3) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "3"}) : null,
                  onSaveHandler: (input) => _scholarshipModel.name = input,
                ),

                //// scholarship description
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 10),
                ScholarshipTextFormField(
                  initialValue: _scholarshipModel.description,
                  width: _editScholarshipPageStyles.textFieldWidth,
                  height: _editScholarshipPageStyles.formTextFontSize * 2,
                  autoHeight: true,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  maxLines: null,
                  fillColor: _scholarshipColors.backgroundColor,
                  labelFontSize: _editScholarshipPageStyles.formTextFontSize,
                  labelColor: _scholarshipColors.textColor,
                  textFontSize: _editScholarshipPageStyles.formTextFontSize,
                  textColor: _scholarshipColors.textColor,
                  borderType: 1,
                  borderColor: _scholarshipColors.textColor,
                  hintTextFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextColor: _scholarshipColors.hintTextColor,
                  labelSpacing: _editScholarshipPageStyles.shareWidthDp * 5,
                  iconSize: _editScholarshipPageStyles.iconSize,
                  validateIconColor: _scholarshipColors.formValidatedColor,
                  unValidateIconColor: _scholarshipColors.textColor,
                  labelText: EditScholarshipPageString.descriptionLabel,
                  hintText: EditScholarshipPageString.descriptionHintText,
                  onChangeHandler: (input) => _scholarshipModel.description = input,
                  validatorHandler: (input) => (input.length < 10) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "10"}) : null,
                  onSaveHandler: (input) => _scholarshipModel.description = input,
                ),

                //// scholarship deadline
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 10),
                ScholarshipDateField(
                  initialValue: (_scholarshipModel.deadlines != 0 && _scholarshipModel.deadlines != null)
                      ? convertMillisecondsToDateTime(_scholarshipModel.deadlines)
                      : null,
                  width: _editScholarshipPageStyles.textFieldWidth,
                  height: _editScholarshipPageStyles.formTextFontSize * 2.5,
                  labelFontSize: _editScholarshipPageStyles.formTextFontSize,
                  labelColor: _scholarshipColors.textColor,
                  borderType: 1,
                  borderColor: _scholarshipColors.textColor,
                  borderRadius: 0,
                  textColor: _scholarshipColors.textColor,
                  textFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextColor: _scholarshipColors.hintTextColor,
                  fillColor: _scholarshipColors.backgroundColor,
                  fixedHeightState: true,
                  validateIconColor: _scholarshipColors.formValidatedColor,
                  unValidateIconColor: _scholarshipColors.textColor,
                  labelText: EditScholarshipPageString.deadlineLabel,
                  hintText: EditScholarshipPageString.deadlineHintText,
                  onChangeHandler: (value) => _scholarshipModel.deadlines = convertDateStringToMilliseconds(value),
                  onSaveHandler: (value) => _scholarshipModel.deadlines = convertDateStringToMilliseconds(value),
                ),

                //// scholarship subTitle1ForAbout
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 10),
                ScholarshipTextFormField(
                  initialValue: _scholarshipModel.subTitle1ForAbout,
                  width: _editScholarshipPageStyles.textFieldWidth,
                  height: _editScholarshipPageStyles.formTextFontSize * 2.5,
                  autoHeight: true,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  maxLines: null,
                  fillColor: _scholarshipColors.backgroundColor,
                  labelFontSize: _editScholarshipPageStyles.formTextFontSize,
                  labelColor: _scholarshipColors.textColor,
                  textFontSize: _editScholarshipPageStyles.formTextFontSize,
                  textColor: _scholarshipColors.textColor,
                  borderType: 1,
                  borderColor: _scholarshipColors.textColor,
                  hintTextFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextColor: _scholarshipColors.hintTextColor,
                  labelSpacing: _editScholarshipPageStyles.shareWidthDp * 5,
                  iconSize: _editScholarshipPageStyles.iconSize,
                  validateIconColor: _scholarshipColors.formValidatedColor,
                  unValidateIconColor: _scholarshipColors.textColor,
                  labelText: EditScholarshipPageString.subTitle1ForAboutLabel,
                  hintText: EditScholarshipPageString.subTitle1ForAboutHintText,
                  onChangeHandler: (input) => _scholarshipModel.subTitle1ForAbout = input,
                  validatorHandler: (input) => (input.length < 3) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "3"}) : null,
                  onSaveHandler: (input) => _scholarshipModel.subTitle1ForAbout = input,
                ),

                //// scholarship subDescription1ForAbout
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 10),
                ScholarshipTextFormField(
                  initialValue: _scholarshipModel.subDescription1ForAbout,
                  width: _editScholarshipPageStyles.textFieldWidth,
                  height: _editScholarshipPageStyles.formTextFontSize * 2.5,
                  autoHeight: true,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  maxLines: null,
                  fillColor: _scholarshipColors.backgroundColor,
                  labelFontSize: _editScholarshipPageStyles.formTextFontSize,
                  labelColor: _scholarshipColors.textColor,
                  textFontSize: _editScholarshipPageStyles.formTextFontSize,
                  textColor: _scholarshipColors.textColor,
                  borderType: 1,
                  borderColor: _scholarshipColors.textColor,
                  hintTextFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextColor: _scholarshipColors.hintTextColor,
                  labelSpacing: _editScholarshipPageStyles.shareWidthDp * 5,
                  iconSize: _editScholarshipPageStyles.iconSize,
                  validateIconColor: _scholarshipColors.formValidatedColor,
                  unValidateIconColor: _scholarshipColors.textColor,
                  labelText: EditScholarshipPageString.subDescription1ForAboutLabel,
                  hintText: EditScholarshipPageString.subDescription1ForAboutHintText,
                  onChangeHandler: (input) => _scholarshipModel.subDescription1ForAbout = input,
                  validatorHandler: (input) => (input.length < 10) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "10"}) : null,
                  onSaveHandler: (input) => _scholarshipModel.subDescription1ForAbout = input,
                ),

                //// scholarship subTitle2ForAbout
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 10),
                ScholarshipTextFormField(
                  initialValue: _scholarshipModel.subTitle2ForAbout,
                  width: _editScholarshipPageStyles.textFieldWidth,
                  height: _editScholarshipPageStyles.formTextFontSize * 2.5,
                  autoHeight: true,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  maxLines: null,
                  fillColor: _scholarshipColors.backgroundColor,
                  labelFontSize: _editScholarshipPageStyles.formTextFontSize,
                  labelColor: _scholarshipColors.textColor,
                  textFontSize: _editScholarshipPageStyles.formTextFontSize,
                  textColor: _scholarshipColors.textColor,
                  borderType: 1,
                  borderColor: _scholarshipColors.textColor,
                  hintTextFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextColor: _scholarshipColors.hintTextColor,
                  labelSpacing: _editScholarshipPageStyles.shareWidthDp * 5,
                  iconSize: _editScholarshipPageStyles.iconSize,
                  validateIconColor: _scholarshipColors.formValidatedColor,
                  unValidateIconColor: _scholarshipColors.textColor,
                  labelText: EditScholarshipPageString.subTitle2ForAboutLabel,
                  hintText: EditScholarshipPageString.subTitle2ForAboutHintText,
                  onChangeHandler: (input) => _scholarshipModel.subTitle2ForAbout = input,
                  validatorHandler: (input) => (input.length < 3) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "3"}) : null,
                  onSaveHandler: (input) => _scholarshipModel.subTitle2ForAbout = input,
                ),

                //// scholarship subDescription2ForAbout
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 10),
                ScholarshipTextFormField(
                  initialValue: _scholarshipModel.subDescription2ForAbout,
                  width: _editScholarshipPageStyles.textFieldWidth,
                  height: _editScholarshipPageStyles.formTextFontSize * 2.5,
                  autoHeight: true,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  maxLines: null,
                  fillColor: _scholarshipColors.backgroundColor,
                  labelFontSize: _editScholarshipPageStyles.formTextFontSize,
                  labelColor: _scholarshipColors.textColor,
                  textFontSize: _editScholarshipPageStyles.formTextFontSize,
                  textColor: _scholarshipColors.textColor,
                  borderType: 1,
                  borderColor: _scholarshipColors.textColor,
                  hintTextFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextColor: _scholarshipColors.hintTextColor,
                  labelSpacing: _editScholarshipPageStyles.shareWidthDp * 5,
                  iconSize: _editScholarshipPageStyles.iconSize,
                  validateIconColor: _scholarshipColors.formValidatedColor,
                  unValidateIconColor: _scholarshipColors.textColor,
                  labelText: EditScholarshipPageString.subDescription2ForAboutLabel,
                  hintText: EditScholarshipPageString.subDescription2ForAboutHintText,
                  onChangeHandler: (input) => _scholarshipModel.subDescription2ForAbout = input,
                  validatorHandler: (input) => (input.length < 10) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "10"}) : null,
                  onSaveHandler: (input) => _scholarshipModel.subDescription2ForAbout = input,
                ),

                //// scholarship awardBrief
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 10),
                ScholarshipTextFormField(
                  initialValue: _scholarshipModel.awardBrief,
                  width: _editScholarshipPageStyles.textFieldWidth,
                  height: _editScholarshipPageStyles.formTextFontSize * 2.5,
                  autoHeight: true,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  maxLines: null,
                  fillColor: _scholarshipColors.backgroundColor,
                  labelFontSize: _editScholarshipPageStyles.formTextFontSize,
                  labelColor: _scholarshipColors.textColor,
                  textFontSize: _editScholarshipPageStyles.formTextFontSize,
                  textColor: _scholarshipColors.textColor,
                  borderType: 1,
                  borderColor: _scholarshipColors.textColor,
                  hintTextFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextColor: _scholarshipColors.hintTextColor,
                  labelSpacing: _editScholarshipPageStyles.shareWidthDp * 5,
                  iconSize: _editScholarshipPageStyles.iconSize,
                  validateIconColor: _scholarshipColors.formValidatedColor,
                  unValidateIconColor: _scholarshipColors.textColor,
                  labelText: EditScholarshipPageString.awardBriefLabel,
                  hintText: EditScholarshipPageString.awardBriefHintText,
                  onChangeHandler: (input) => _scholarshipModel.awardBrief = input,
                  validatorHandler: (input) => (input.length < 3) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "3"}) : null,
                  onSaveHandler: (input) => _scholarshipModel.awardBrief = input,
                ),

                //// scholarship awardDetails
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 10),
                ScholarshipTextFormField(
                  initialValue: _scholarshipModel.awardDetails,
                  width: _editScholarshipPageStyles.textFieldWidth,
                  height: _editScholarshipPageStyles.formTextFontSize * 2.5,
                  autoHeight: true,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  maxLines: null,
                  fillColor: _scholarshipColors.backgroundColor,
                  labelFontSize: _editScholarshipPageStyles.formTextFontSize,
                  labelColor: _scholarshipColors.textColor,
                  textFontSize: _editScholarshipPageStyles.formTextFontSize,
                  textColor: _scholarshipColors.textColor,
                  borderType: 1,
                  borderColor: _scholarshipColors.textColor,
                  hintTextFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextColor: _scholarshipColors.hintTextColor,
                  labelSpacing: _editScholarshipPageStyles.shareWidthDp * 5,
                  iconSize: _editScholarshipPageStyles.iconSize,
                  validateIconColor: _scholarshipColors.formValidatedColor,
                  unValidateIconColor: _scholarshipColors.textColor,
                  labelText: EditScholarshipPageString.awardDetailsLabel,
                  hintText: EditScholarshipPageString.awardDetailsHintText,
                  onChangeHandler: (input) => _scholarshipModel.awardDetails = input,
                  validatorHandler: (input) => (input.length < 10) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "10"}) : null,
                  onSaveHandler: (input) => _scholarshipModel.awardDetails = input,
                ),

                //// scholarship requirementBrief
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 10),
                ScholarshipTextFormField(
                  initialValue: _scholarshipModel.requirementBrief,
                  width: _editScholarshipPageStyles.textFieldWidth,
                  height: _editScholarshipPageStyles.formTextFontSize * 2.5,
                  autoHeight: true,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  maxLines: null,
                  fillColor: _scholarshipColors.backgroundColor,
                  labelFontSize: _editScholarshipPageStyles.formTextFontSize,
                  labelColor: _scholarshipColors.textColor,
                  textFontSize: _editScholarshipPageStyles.formTextFontSize,
                  textColor: _scholarshipColors.textColor,
                  borderType: 1,
                  borderColor: _scholarshipColors.textColor,
                  hintTextFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextColor: _scholarshipColors.hintTextColor,
                  labelSpacing: _editScholarshipPageStyles.shareWidthDp * 5,
                  iconSize: _editScholarshipPageStyles.iconSize,
                  validateIconColor: _scholarshipColors.formValidatedColor,
                  unValidateIconColor: _scholarshipColors.textColor,
                  labelText: EditScholarshipPageString.requirementBriefLabel,
                  hintText: EditScholarshipPageString.requirementBriefHintText,
                  onChangeHandler: (input) => _scholarshipModel.requirementBrief = input,
                  validatorHandler: (input) => (input.length < 3) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "3"}) : null,
                  onSaveHandler: (input) => _scholarshipModel.requirementBrief = input,
                ),

                //// scholarship requirementDetails
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 10),
                ScholarshipTextFormField(
                  initialValue: _scholarshipModel.requirementDetails,
                  width: _editScholarshipPageStyles.textFieldWidth,
                  height: _editScholarshipPageStyles.formTextFontSize * 2.5,
                  autoHeight: true,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  maxLines: null,
                  fillColor: _scholarshipColors.backgroundColor,
                  labelFontSize: _editScholarshipPageStyles.formTextFontSize,
                  labelColor: _scholarshipColors.textColor,
                  textFontSize: _editScholarshipPageStyles.formTextFontSize,
                  textColor: _scholarshipColors.textColor,
                  borderType: 1,
                  borderColor: _scholarshipColors.textColor,
                  hintTextFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextColor: _scholarshipColors.hintTextColor,
                  labelSpacing: _editScholarshipPageStyles.shareWidthDp * 5,
                  iconSize: _editScholarshipPageStyles.iconSize,
                  validateIconColor: _scholarshipColors.formValidatedColor,
                  unValidateIconColor: _scholarshipColors.textColor,
                  labelText: EditScholarshipPageString.requirementDetailsLabel,
                  hintText: EditScholarshipPageString.requirementDetailsHintText,
                  onChangeHandler: (input) => _scholarshipModel.requirementDetails = input,
                  validatorHandler: (input) => (input.length < 10) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "10"}) : null,
                  onSaveHandler: (input) => _scholarshipModel.requirementDetails = input,
                ),

                //// scholarship dateTimesBrief
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 10),
                ScholarshipTextFormField(
                  initialValue: _scholarshipModel.dateTimesBrief,
                  width: _editScholarshipPageStyles.textFieldWidth,
                  height: _editScholarshipPageStyles.formTextFontSize * 2.5,
                  autoHeight: true,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  maxLines: null,
                  fillColor: _scholarshipColors.backgroundColor,
                  labelFontSize: _editScholarshipPageStyles.formTextFontSize,
                  labelColor: _scholarshipColors.textColor,
                  textFontSize: _editScholarshipPageStyles.formTextFontSize,
                  textColor: _scholarshipColors.textColor,
                  borderType: 1,
                  borderColor: _scholarshipColors.textColor,
                  hintTextFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextColor: _scholarshipColors.hintTextColor,
                  labelSpacing: _editScholarshipPageStyles.shareWidthDp * 5,
                  iconSize: _editScholarshipPageStyles.iconSize,
                  validateIconColor: _scholarshipColors.formValidatedColor,
                  unValidateIconColor: _scholarshipColors.textColor,
                  labelText: EditScholarshipPageString.dateTimesBriefLabel,
                  hintText: EditScholarshipPageString.dateTimesBriefHintText,
                  onChangeHandler: (input) => _scholarshipModel.dateTimesBrief = input,
                  validatorHandler: (input) => (input.length < 3) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "3"}) : null,
                  onSaveHandler: (input) => _scholarshipModel.dateTimesBrief = input,
                ),

                //// scholarship dateTimesDetials
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 10),
                ScholarshipTextFormField(
                  initialValue: _scholarshipModel.dateTimesDetails,
                  width: _editScholarshipPageStyles.textFieldWidth,
                  height: _editScholarshipPageStyles.formTextFontSize * 2.5,
                  autoHeight: true,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  maxLines: null,
                  fillColor: _scholarshipColors.backgroundColor,
                  labelFontSize: _editScholarshipPageStyles.formTextFontSize,
                  labelColor: _scholarshipColors.textColor,
                  textFontSize: _editScholarshipPageStyles.formTextFontSize,
                  textColor: _scholarshipColors.textColor,
                  borderType: 1,
                  borderColor: _scholarshipColors.textColor,
                  hintTextFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextColor: _scholarshipColors.hintTextColor,
                  labelSpacing: _editScholarshipPageStyles.shareWidthDp * 5,
                  iconSize: _editScholarshipPageStyles.iconSize,
                  validateIconColor: _scholarshipColors.formValidatedColor,
                  unValidateIconColor: _scholarshipColors.textColor,
                  labelText: EditScholarshipPageString.dateTimesDetailsLabel,
                  hintText: EditScholarshipPageString.dateTimesDetailsHintText,
                  onChangeHandler: (input) => _scholarshipModel.dateTimesDetails = input,
                  validatorHandler: (input) => (input.length < 10) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "10"}) : null,
                  onSaveHandler: (input) => _scholarshipModel.dateTimesDetails = input,
                ),

                //// scholarship videoUrlForAbout
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 10),
                ScholarshipTextFormField(
                  initialValue: _scholarshipModel.videoUrlForAbout,
                  width: _editScholarshipPageStyles.textFieldWidth,
                  height: _editScholarshipPageStyles.formTextFontSize,
                  autoHeight: true,
                  lineSpacing: _editScholarshipPageStyles.shareWidthDp * 3,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  maxLines: null,
                  fillColor: _scholarshipColors.backgroundColor,
                  labelFontSize: _editScholarshipPageStyles.formTextFontSize,
                  labelColor: _scholarshipColors.textColor,
                  textFontSize: _editScholarshipPageStyles.formTextFontSize,
                  textColor: _scholarshipColors.textColor,
                  borderType: 1,
                  borderColor: _scholarshipColors.textColor,
                  hintTextFontSize: _editScholarshipPageStyles.formTextFontSize,
                  hintTextColor: _scholarshipColors.hintTextColor,
                  labelSpacing: _editScholarshipPageStyles.shareWidthDp * 5,
                  iconSize: _editScholarshipPageStyles.iconSize,
                  validateIconColor: _scholarshipColors.formValidatedColor,
                  unValidateIconColor: _scholarshipColors.textColor,
                  labelText: EditScholarshipPageString.videoUrlLabel,
                  hintText: EditScholarshipPageString.videoUrlHintText,
                  onChangeHandler: (input) => _scholarshipModel.videoUrlForAbout = input,
                  validatorHandler: (input) => (input.length < 3) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "3"}) : null,
                  onSaveHandler: (input) => _scholarshipModel.videoUrlForAbout = input,
                ),

                //// scholarship image
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      EditScholarshipPageString.imageLabel,
                      style: TextStyle(fontSize: _editScholarshipPageStyles.formTextFontSize, color: _scholarshipColors.textColor),
                    ),
                    SizedBox(width: _editScholarshipPageStyles.shareWidthDp * 10),
                    Text("*", style: TextStyle(fontSize: _editScholarshipPageStyles.formTextFontSize, color: Colors.red)),
                  ],
                ),
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 5),
                Center(
                  child: CustomFilePickerForWeb(
                    context: context,
                    width: _editScholarshipPageStyles.imageSize,
                    height: _editScholarshipPageStyles.imageSize,
                    borderColor: _scholarshipColors.textColor,
                    selectWidget: Icon(Icons.add, size: _editScholarshipPageStyles.imageSize / 6, color: _scholarshipColors.textColor),
                    deleteWidget: Icon(Icons.delete_forever, size: _editScholarshipPageStyles.shareWidthDp * 20, color: _scholarshipColors.textColor),
                    validateTextFontSize: _editScholarshipPageStyles.formTextFontSize * 0.8,
                    file: _imageFile,
                    fileByteData: _imageFileByteData,
                    url: _scholarshipModel.imageUrl,
                    validator: (value) {
                      if (widget.category == "new")
                        return (!value) ? "Please choose file" : null;
                      else
                        return null;
                    },
                    onChangeHandler: (html.File file, Uint8List fileByteDate) {
                      _imageFile = file;
                      _imageFileByteData = fileByteDate;
                      _editScholarshipBloc.add(AddScholarshipImageDataEvent(file: file, fileByteData: fileByteDate));
                    },
                  ),
                ),

                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 30),
                Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CustomRaisedButton(
                          width: _editScholarshipPageStyles.buttonWidth,
                          height: _editScholarshipPageStyles.formTextFontSize * 2,
                          color: _scholarshipColors.cancelButtonColor,
                          borderRadius: 0,
                          borderColor: _scholarshipColors.cancelButtonColor,
                          child: Text(
                            EditScholarshipPageString.cancelButtonText,
                            style: TextStyle(fontSize: _editScholarshipPageStyles.formTextFontSize, color: Colors.black),
                          ),
                          onPressed: () {},
                        ),
                        SizedBox(width: _editScholarshipPageStyles.shareWidthDp * 20),
                        CustomRaisedButton(
                          width: _editScholarshipPageStyles.buttonWidth,
                          height: _editScholarshipPageStyles.formTextFontSize * 2,
                          color: _scholarshipColors.secondaryColor,
                          borderRadius: 0,
                          borderColor: _scholarshipColors.secondaryColor,
                          child: Text(
                            EditScholarshipPageString.submitButtonText,
                            style: TextStyle(fontSize: _editScholarshipPageStyles.formTextFontSize, color: Colors.white),
                          ),
                          onPressed: () {
                            if (state.saveState != 1) _submit(context);
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 15),
                    (state.saveState == -1)
                        ? Text(
                            EditScholarshipPageString.addScholarshipFail,
                            style: TextStyle(fontSize: _editScholarshipPageStyles.formTextFontSize, color: Colors.red),
                          )
                        : (state.saveState == 2)
                            ? Text(
                                EditScholarshipPageString.addScholarshipSuccess,
                                style: TextStyle(fontSize: _editScholarshipPageStyles.formTextFontSize, color: Colors.green),
                              )
                            : SizedBox(),
                  ],
                ),
                SizedBox(height: _editScholarshipPageStyles.shareWidthDp * 10),
              ],
            ),
          ),
        );
      },
    );
  }

  void _submit(BuildContext context) async {
    if (!_formkey.currentState.validate()) return;
    _formkey.currentState.save();
    try {
      await CustomProgressDialog.of(context: context).show();
      _editScholarshipBloc.add(SaveStateForScholarshipEvent(saveState: 1));
      if (widget.category == "new") {
        _editScholarshipBloc.add(AddScholarshipEvent(newScholarshipModel: _scholarshipModel, file: _imageFile));
      } else {
        _editScholarshipBloc.add(UpdateScholarshipEvent(updateScholarshipModel: _scholarshipModel, file: _imageFile));
      }
    } catch (e) {
      print(e);
    }
  }
}
