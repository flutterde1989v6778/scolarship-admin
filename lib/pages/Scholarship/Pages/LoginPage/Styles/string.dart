import 'package:easy_localization/easy_localization.dart' as EL;

class ScholarshipLoginPageString {
  static String returnText = "ScholarshipLoginPage.returnText".tr();
  static String title = "ScholarshipLoginPage.title".tr();
  static String emailLabel = "ScholarshipLoginPage.emailLabel".tr();
  static String emailHitText = "ScholarshipLoginPage.emailHitText".tr();
  static String passwordLabel = "ScholarshipLoginPage.passwordLabel".tr();
  static String passwordHintText = "ScholarshipLoginPage.passwordHintText".tr();
  static String signinButtonText = "ScholarshipLoginPage.signinButtonText".tr();
}
