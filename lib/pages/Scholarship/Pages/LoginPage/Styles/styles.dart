import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:scholarship/Config/resposible_settings.dart';

class ScholarshipLoginPageStyles {
  double devicePixelRatio;
  double deviceWidth;
  double deviceHeight;
  double fontUnit;
  double statusbarHeight;
  double bottombarHeight;
  double appbarHeight;
  double safeAreaHeight;
  double shareWidth;
  double shareHeight;
  double shareWidthDp;
  double shareHeightDp;

  double primaryHorizontalPadding;
  double primaryVerticalPadding;

  double textFormFieldWidth;
  double textFormFieldHeight;
  double titleFontSize;
  double textFontSize;
  double buttonWidth;
  double buttonHeight;

  ScholarshipLoginPageStyles(BuildContext context) {}
}

class ScholarshipLoginPageDesktopStyles extends ScholarshipLoginPageStyles {
  ScholarshipLoginPageDesktopStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(30);
    primaryVerticalPadding = ScreenUtil().setWidth(30);

    /// mainStyle
    titleFontSize = ScreenUtil().setSp(38, allowFontScalingSelf: false);
    textFontSize = ScreenUtil().setSp(14, allowFontScalingSelf: false);

    textFormFieldWidth = ScreenUtil().setWidth(400);
    textFormFieldHeight = ScreenUtil().setWidth(35);

    buttonWidth = ScreenUtil().setWidth(100);
    buttonHeight = ScreenUtil().setWidth(30);
  }
}

class ScholarshipLoginPageTabletStyles extends ScholarshipLoginPageStyles {
  ScholarshipLoginPageTabletStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(20);
    primaryVerticalPadding = ScreenUtil().setWidth(24);

    /// mainStyle
    textFormFieldWidth = ScreenUtil().setWidth(320);
    textFormFieldHeight = ScreenUtil().setWidth(35);

    titleFontSize = ScreenUtil().setSp(38, allowFontScalingSelf: false);
    textFontSize = ScreenUtil().setSp(14, allowFontScalingSelf: false);

    buttonWidth = ScreenUtil().setWidth(100);
    buttonHeight = ScreenUtil().setWidth(30);
  }
}

class ScholarshipLoginPageMobileStyles extends ScholarshipLoginPageStyles {
  ScholarshipLoginPageMobileStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.mobileDesignWidth,
      height: ResponsibleDesignSettings.mobileDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(22);
    primaryVerticalPadding = ScreenUtil().setWidth(24);

    /// mainStyle
    textFormFieldWidth = ScreenUtil().setWidth(250);
    textFormFieldHeight = ScreenUtil().setWidth(35);

    titleFontSize = ScreenUtil().setSp(24, allowFontScalingSelf: false);
    textFontSize = ScreenUtil().setSp(14, allowFontScalingSelf: false);

    buttonWidth = ScreenUtil().setWidth(100);
    buttonHeight = ScreenUtil().setWidth(30);
  }
}
