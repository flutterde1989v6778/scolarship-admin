import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart' as EL;
import 'package:scholarship/blocs/index.dart';
import 'package:scholarship/pages/App/Config/routes.dart';
import 'package:scholarship/pages/Scholarship/Components/index.dart';
import 'package:scholarship/pages/Scholarship/Styles/index.dart';
import 'package:scholarship/utils/index.dart';
import 'package:scholarship/widgets/index.dart';

import 'index.dart';

class ScholarshipLoginView extends StatefulWidget {
  ScholarshipLoginView({@required this.scholarshipPageColors});

  ScholarshipColors scholarshipPageColors;

  State<ScholarshipLoginView> createState() => _ScholarshipLoginViewState();
}

class _ScholarshipLoginViewState extends State<ScholarshipLoginView> with TickerProviderStateMixin {
  ScholarshipLoginPageStyles _scholarshipLoginPageStyles;
  ScholarshipColors _scholarshipColors;
  ScholarshipLoginBloc _scholarshipLoginBloc;
  GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  String _email;
  String _password;

  @override
  void initState() {
    super.initState();
    _scholarshipLoginBloc = BlocProvider.of<ScholarshipLoginBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("___________ScholarshipLoginView__build______________");
    _scholarshipColors = widget.scholarshipPageColors;

    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth >= 900) {
          _scholarshipLoginPageStyles = ScholarshipLoginPageDesktopStyles(context);
        } else if (constraints.maxWidth >= 600 && constraints.maxWidth < 900) {
          ////  tablet
          _scholarshipLoginPageStyles = ScholarshipLoginPageTabletStyles(context);
        } else if (constraints.maxWidth < 600) {
          _scholarshipLoginPageStyles = ScholarshipLoginPageMobileStyles(context);
        }
        print("___ScholarshipLoginView__LayoutBuilder___${_scholarshipLoginPageStyles.deviceWidth}_______");

        return SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Topbar(returnText: ScholarshipLoginPageString.returnText),
                _containerBody(context),
                Footer(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _containerBody(BuildContext context) {
    return BlocBuilder<ScholarshipLoginBloc, ScholarshipLoginState>(
      builder: (context, state) {
        return Form(
          key: _formkey,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: _scholarshipLoginPageStyles.primaryHorizontalPadding),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: _scholarshipLoginPageStyles.shareWidthDp * 30),
                Text(
                  ScholarshipLoginPageString.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: _scholarshipLoginPageStyles.titleFontSize, color: _scholarshipColors.textColor),
                ),
                SizedBox(height: _scholarshipLoginPageStyles.shareWidthDp * 30),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      ScholarshipLoginPageString.emailLabel,
                      style: TextStyle(fontSize: _scholarshipLoginPageStyles.textFontSize, color: _scholarshipColors.textColor),
                    ),
                    CustomTextFormField(
                      width: _scholarshipLoginPageStyles.textFormFieldWidth,
                      height: _scholarshipLoginPageStyles.textFormFieldHeight,
                      borderType: 1,
                      borderColor: _scholarshipColors.secondaryColor,
                      textFontSize: _scholarshipLoginPageStyles.textFontSize,
                      textColor: _scholarshipColors.textColor,
                      fillColor: _scholarshipColors.backgroundColor,
                      hintText: ScholarshipLoginPageString.emailHitText,
                      hintTextColor: _scholarshipColors.hintTextColor,
                      hintTextFontSize: _scholarshipLoginPageStyles.textFontSize,
                      fixedHeightState: true,
                      onChangeHandler: (input) => _email = input.trim(),
                      validatorHandler: (input) => (!Validators.emailRegExp.hasMatch(input)) ? ScholarshipString.emailValidateString : null,
                      onSaveHandler: (input) => _email = input.trim(),
                    )
                  ],
                ),
                SizedBox(height: _scholarshipLoginPageStyles.shareWidthDp * 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      ScholarshipLoginPageString.passwordLabel,
                      style: TextStyle(fontSize: _scholarshipLoginPageStyles.textFontSize, color: _scholarshipColors.textColor),
                    ),
                    CustomTextFormField(
                      width: _scholarshipLoginPageStyles.textFormFieldWidth,
                      height: _scholarshipLoginPageStyles.textFormFieldHeight,
                      borderType: 1,
                      borderColor: _scholarshipColors.secondaryColor,
                      textFontSize: _scholarshipLoginPageStyles.textFontSize,
                      textColor: _scholarshipColors.textColor,
                      fillColor: _scholarshipColors.backgroundColor,
                      hintText: ScholarshipLoginPageString.passwordHintText,
                      hintTextColor: _scholarshipColors.hintTextColor,
                      hintTextFontSize: _scholarshipLoginPageStyles.textFontSize,
                      fixedHeightState: true,
                      obscureText: true,
                      onChangeHandler: (input) => _password = input.trim(),
                      validatorHandler: (input) => (input.length < 6) ? ScholarshipString.textValidateString.tr(namedArgs: {"length": "6"}) : null,
                      onSaveHandler: (input) => _password = input.trim(),
                    )
                  ],
                ),
                SizedBox(height: _scholarshipLoginPageStyles.shareWidthDp * 30),
                CustomRaisedButton(
                  width: _scholarshipLoginPageStyles.buttonWidth,
                  height: _scholarshipLoginPageStyles.buttonHeight,
                  color: _scholarshipColors.secondaryColor,
                  borderRadius: 4,
                  borderColor: _scholarshipColors.secondaryColor,
                  child: Text(
                    ScholarshipLoginPageString.signinButtonText,
                    style: TextStyle(fontSize: _scholarshipLoginPageStyles.textFontSize, color: Colors.white),
                  ),
                  onPressed: () {
                    login(context);
                  },
                ),
                SizedBox(height: _scholarshipLoginPageStyles.shareWidthDp * 60),
              ],
            ),
          ),
        );
      },
    );
  }

  void login(BuildContext context) async {
    if (!_formkey.currentState.validate()) return;
    _formkey.currentState.save();
    AppRoutes.router.navigateTo(context, AppRoutes.manageApplications, transition: TransitionType.fadeIn, replace: true);
  }
}
