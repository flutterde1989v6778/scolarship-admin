import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scholarship/blocs/index.dart';
import 'package:scholarship/pages/Scholarship/Styles/index.dart';
import 'package:scholarship/widgets/SettingMenu/index.dart';

import './index.dart';

class ScholarshipLoginPage extends StatelessWidget {
  ScholarshipColors _scholarshipColors;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: BlocBuilder<ThemeBloc, ThemeState>(
        builder: (context, state) {
          if (state.themeMode == ThemeModeConstants.dark) {
            _scholarshipColors = ScholarshipDarkModeColors();
          } else {
            _scholarshipColors = ScholarshipLightModeColors();
          }
          return Scaffold(
            appBar: AppBar(
              title: Text('Scholarship Admin'),
              backgroundColor: Color(0xFF232323),
              actions: <Widget>[
                SettingMenu(),
              ],
            ),
            backgroundColor: _scholarshipColors.backgroundColor,
            body: MultiBlocProvider(
              providers: [
                BlocProvider<ScholarshipLoginBloc>(create: (context) => ScholarshipLoginBloc()),
              ],
              child: ScholarshipLoginView(scholarshipPageColors: _scholarshipColors),
            ),
          );
        },
      ),
    );
  }
}
