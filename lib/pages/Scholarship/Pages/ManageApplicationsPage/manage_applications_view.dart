import 'dart:ui';

import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart' as EL;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:scholarship/blocs/index.dart';
import 'package:scholarship/models/index.dart';
import 'package:scholarship/pages/App/index.dart';
import 'package:scholarship/pages/Scholarship/Components/index.dart';
import 'package:scholarship/pages/Scholarship/Styles/index.dart';
import 'package:scholarship/repositories/scholarship_repository.dart';
import 'package:scholarship/utils/index.dart';
import 'package:scholarship/widgets/index.dart';

import 'index.dart';

class ManageApplicationsView extends StatefulWidget {
  ManageApplicationsView({@required this.scholarshipPageColors});

  ScholarshipColors scholarshipPageColors;

  State<ManageApplicationsView> createState() => _ManageApplicationsViewState();
}

class _ManageApplicationsViewState extends State<ManageApplicationsView> with TickerProviderStateMixin {
  ManageApplicationsPageStyles _manageApplicationsPageStyles;
  ScholarshipColors _scholarshipColors;
  ManageApplicationsBloc _manageApplicationsBloc;
  GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  ScholarshipRepository _scholarshipRepository = ScholarshipRepository();

  String _email;
  String _password;

  @override
  void initState() {
    super.initState();
    _manageApplicationsBloc = BlocProvider.of<ManageApplicationsBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("___________ManageApplicationsView__build______________");
    _scholarshipColors = widget.scholarshipPageColors;

    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth >= 900) {
          _manageApplicationsPageStyles = ManageApplicationsPageDesktopStyles(context);
        } else if (constraints.maxWidth >= 600 && constraints.maxWidth < 900) {
          ////  tablet
          _manageApplicationsPageStyles = ManageApplicationsPageTabletStyles(context);
        } else if (constraints.maxWidth < 600) {
          _manageApplicationsPageStyles = ManageApplicationsPageMobileStyles(context);
        }
        print("___ManageApplicationsView__LayoutBuilder___${_manageApplicationsPageStyles.deviceWidth}_______");

        return SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Topbar(
                  loginState: true,
                  returnText: ManageApplicationsPageString.returnText,
                ),
                _containerBody(context),
                Footer(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _containerBody(BuildContext context) {
    return BlocBuilder<ManageApplicationsBloc, ManageApplicationsState>(
      builder: (context, state) {
        if (state.loadingStateForApplictionListStream == 0) _manageApplicationsBloc.add(GetApplicationListStreamEvent());

        return Container(
            padding: EdgeInsets.symmetric(
              horizontal: _manageApplicationsPageStyles.primaryHorizontalPadding,
              vertical: _manageApplicationsPageStyles.primaryVerticalPadding,
            ),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: _manageApplicationsPageStyles.titleHorizontalPadding),
                      child: Text(
                        ManageApplicationsPageString.title,
                        style: TextStyle(fontSize: _manageApplicationsPageStyles.titleFontSize, color: _scholarshipColors.textColor),
                      ),
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: _manageApplicationsPageStyles.applicationListHorizontalPading,
                    vertical: _manageApplicationsPageStyles.applicationListVerticalPadding,
                  ),
                  child: Column(
                    children: <Widget>[
                      (_manageApplicationsPageStyles.runtimeType == ManageApplicationsPageMobileStyles)
                          ? _containerApplicationListFilterBarForMobile(context, state)
                          : _containerApplicationListFilterBarForDesktop(context, state),
                      (_manageApplicationsPageStyles.runtimeType != ManageApplicationsPageMobileStyles)
                          ? _containerListHeader(context, state)
                          : SizedBox(),
                      _containerApplicationList(context, state),
                    ],
                  ),
                )
              ],
            ));
      },
    );
  }

  Widget _containerApplicationListFilterBarForDesktop(BuildContext contex, ManageApplicationsState manageApplicationsState) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: _manageApplicationsPageStyles.rowHorizontalPadding),
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: _scholarshipColors.secondaryColor))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  _manageApplicationsBloc.add(SelectFilterEvent(selectedFilter: 0));
                },
                child: Container(
                  padding: EdgeInsets.all(_manageApplicationsPageStyles.shareWidthDp * 5),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: (manageApplicationsState.selectedFilter == 0) ? _scholarshipColors.secondaryColor : _scholarshipColors.backgroundColor,
                        width: _manageApplicationsPageStyles.shareWidthDp * 3,
                      ),
                    ),
                  ),
                  child: Text(
                    ManageApplicationsPageString.filter1,
                    style: TextStyle(fontSize: _manageApplicationsPageStyles.filterFontSize, color: _scholarshipColors.textColor),
                  ),
                ),
              ),
              SizedBox(width: _manageApplicationsPageStyles.shareWidthDp * 15),
              GestureDetector(
                onTap: () {
                  _manageApplicationsBloc.add(SelectFilterEvent(selectedFilter: 1));
                },
                child: Container(
                  padding: EdgeInsets.all(_manageApplicationsPageStyles.shareWidthDp * 5),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: (manageApplicationsState.selectedFilter == 1) ? _scholarshipColors.secondaryColor : _scholarshipColors.backgroundColor,
                        width: _manageApplicationsPageStyles.shareWidthDp * 3,
                      ),
                    ),
                  ),
                  child: Text(
                    ManageApplicationsPageString.filter2,
                    style: TextStyle(fontSize: _manageApplicationsPageStyles.filterFontSize, color: _scholarshipColors.textColor),
                  ),
                ),
              ),
              SizedBox(width: _manageApplicationsPageStyles.shareWidthDp * 15),
              GestureDetector(
                onTap: () {
                  _manageApplicationsBloc.add(SelectFilterEvent(selectedFilter: 2));
                },
                child: Container(
                  padding: EdgeInsets.all(_manageApplicationsPageStyles.shareWidthDp * 5),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: (manageApplicationsState.selectedFilter == 2) ? _scholarshipColors.secondaryColor : _scholarshipColors.backgroundColor,
                        width: _manageApplicationsPageStyles.shareWidthDp * 3,
                      ),
                    ),
                  ),
                  child: Text(
                    ManageApplicationsPageString.filter3,
                    style: TextStyle(fontSize: _manageApplicationsPageStyles.filterFontSize, color: _scholarshipColors.textColor),
                  ),
                ),
              ),
              SizedBox(width: _manageApplicationsPageStyles.shareWidthDp * 15),
              GestureDetector(
                onTap: () {
                  _manageApplicationsBloc.add(SelectFilterEvent(selectedFilter: 3));
                },
                child: Container(
                  padding: EdgeInsets.all(_manageApplicationsPageStyles.shareWidthDp * 5),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: (manageApplicationsState.selectedFilter == 3) ? _scholarshipColors.secondaryColor : _scholarshipColors.backgroundColor,
                        width: _manageApplicationsPageStyles.shareWidthDp * 3,
                      ),
                    ),
                  ),
                  child: Text(
                    ManageApplicationsPageString.filter4,
                    style: TextStyle(fontSize: _manageApplicationsPageStyles.filterFontSize, color: _scholarshipColors.textColor),
                  ),
                ),
              ),
            ],
          ),
          SettingButtonForManageApplications(),
        ],
      ),
    );
  }

  Widget _containerApplicationListFilterBarForMobile(BuildContext contex, ManageApplicationsState manageApplicationsState) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: _manageApplicationsPageStyles.rowHorizontalPadding,
        vertical: _manageApplicationsPageStyles.shareWidthDp * 5,
      ),
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: _scholarshipColors.secondaryColor))),
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                ManageApplicationsPageString.filter,
                style: TextStyle(fontSize: _manageApplicationsPageStyles.filterFontSize, color: _scholarshipColors.secondaryColor),
              ),
              CustomDropDown(
                width: _manageApplicationsPageStyles.deviceWidth * 0.6,
                height: _manageApplicationsPageStyles.filterFontSize * 2.5,
                borderType: 1,
                borderColor: _scholarshipColors.backgroundColor,
                fillColor: _scholarshipColors.backgroundColor,
                dropdownColor: _scholarshipColors.panelBackgroundColor,
                menuItems: [
                  {
                    "value": 0,
                    "text": ManageApplicationsPageString.filter1,
                  },
                  {
                    "value": 1,
                    "text": ManageApplicationsPageString.filter2,
                  },
                  {
                    "value": 2,
                    "text": ManageApplicationsPageString.filter3,
                  },
                  {
                    "value": 3,
                    "text": ManageApplicationsPageString.filter4,
                  },
                ],
                iconSize: _manageApplicationsPageStyles.filterFontSize * 0.7,
                itemTextStyle: TextStyle(
                  fontSize: _manageApplicationsPageStyles.filterFontSize,
                  color: _scholarshipColors.secondaryColor,
                ),
                value: manageApplicationsState.selectedFilter,
                onChangeHandler: (value) {
                  _manageApplicationsBloc.add(SelectFilterEvent(selectedFilter: value));
                },
              ),
            ],
          ),
          SettingButtonForManageApplications(),
        ],
      ),
    );
  }

  Widget _containerListHeader(BuildContext context, ManageApplicationsState manageApplicationsState) {
    return Container(
      height: _manageApplicationsPageStyles.rowHeight,
      padding: EdgeInsets.symmetric(
        horizontal: _manageApplicationsPageStyles.rowHorizontalPadding,
        vertical: _manageApplicationsPageStyles.shareWidthDp * 10,
      ),
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: _scholarshipColors.secondaryColor))),
      alignment: Alignment.bottomCenter,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: _manageApplicationsPageStyles.applicantColumnFlex,
            child: GestureDetector(
              onTap: () {
                Map<String, dynamic> orderBy = manageApplicationsState.orderby;
                orderBy["available"] = "applicant";
                orderBy["applicant"] = !orderBy["applicant"];

                _manageApplicationsBloc.add(ChangeOrderByEvent(orderby: orderBy));
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    ManageApplicationsPageString.applicantColumnHeader,
                    style: TextStyle(
                      fontSize: _manageApplicationsPageStyles.textFontSize,
                      color: _scholarshipColors.textColor,
                    ),
                  ),
                  SizedBox(width: _manageApplicationsPageStyles.shareWidthDp * 5),
                  FaIcon(
                    (!manageApplicationsState.orderby["applicant"]) ? FontAwesomeIcons.sortAlphaUp : FontAwesomeIcons.sortAlphaDown,
                    color: _scholarshipColors.secondaryColor,
                    size: _manageApplicationsPageStyles.textFontSize * 1.2,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: _manageApplicationsPageStyles.submissionDateColumnFlex,
            child: GestureDetector(
              onTap: () {
                Map<String, dynamic> orderBy = manageApplicationsState.orderby;
                orderBy["available"] = "submission";
                orderBy["submission"] = !orderBy["submission"];

                _manageApplicationsBloc.add(ChangeOrderByEvent(orderby: orderBy));
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    ManageApplicationsPageString.submissionDateColumnHeader,
                    style: TextStyle(
                      fontSize: _manageApplicationsPageStyles.textFontSize,
                      color: _scholarshipColors.textColor,
                    ),
                  ),
                  SizedBox(width: _manageApplicationsPageStyles.shareWidthDp * 5),
                  FaIcon(
                    (!manageApplicationsState.orderby["submission"]) ? FontAwesomeIcons.sortAmountUpAlt : FontAwesomeIcons.sortAmountDownAlt,
                    color: _scholarshipColors.secondaryColor,
                    size: _manageApplicationsPageStyles.textFontSize * 1.2,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: _manageApplicationsPageStyles.scoreColumnFlex,
            child: GestureDetector(
              onTap: () {
                Map<String, dynamic> orderBy = manageApplicationsState.orderby;
                orderBy["available"] = "score";
                orderBy["score"] = !orderBy["score"];

                _manageApplicationsBloc.add(ChangeOrderByEvent(orderby: orderBy));
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    ManageApplicationsPageString.scoreColumnHeader,
                    style: TextStyle(
                      fontSize: _manageApplicationsPageStyles.textFontSize,
                      color: _scholarshipColors.textColor,
                    ),
                  ),
                  SizedBox(width: _manageApplicationsPageStyles.shareWidthDp * 5),
                  FaIcon(
                    (!manageApplicationsState.orderby["score"]) ? FontAwesomeIcons.sortAmountUpAlt : FontAwesomeIcons.sortAmountDownAlt,
                    color: _scholarshipColors.secondaryColor,
                    size: _manageApplicationsPageStyles.textFontSize * 1.2,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: _manageApplicationsPageStyles.categoryColumnFlex,
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    ManageApplicationsPageString.categoryColumnHeader,
                    style: TextStyle(
                      fontSize: _manageApplicationsPageStyles.textFontSize,
                      color: _scholarshipColors.textColor,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: _manageApplicationsPageStyles.evaluationColumnFlex,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  ManageApplicationsPageString.evaluationsColumnHeader,
                  style: TextStyle(
                    fontSize: _manageApplicationsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              CustomRaisedButton(
                width: _manageApplicationsPageStyles.shareWidthDp * 100,
                height: _manageApplicationsPageStyles.textFontSize * 2,
                color: _scholarshipColors.backgroundColor,
                borderColor: _scholarshipColors.backgroundColor,
                borderRadius: 0,
                child: Text(
                  ManageApplicationsPageString.reviewButtonText,
                  style: TextStyle(fontSize: _manageApplicationsPageStyles.textFontSize, color: _scholarshipColors.backgroundColor),
                ),
                elevation: 0,
              ),
              SizedBox(width: _manageApplicationsPageStyles.shareWidthDp * 15),
              Icon(Icons.more_horiz, size: _manageApplicationsPageStyles.textFontSize * 1.2, color: _scholarshipColors.backgroundColor),
            ],
          )
        ],
      ),
    );
  }

  Widget _containerApplicationRowForDesktop(
    BuildContext context,
    ManageApplicationsState manageApplicationsState,
    ScholarshipModel scholarshipModel,
    ScholarshipApplicationModel scholarshipApplicationModel,
  ) {
    return Container(
      height: _manageApplicationsPageStyles.rowHeight,
      padding: EdgeInsets.symmetric(
        horizontal: _manageApplicationsPageStyles.rowHorizontalPadding,
        vertical: _manageApplicationsPageStyles.shareWidthDp * 10,
      ),
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: _scholarshipColors.secondaryColor))),
      alignment: Alignment.bottomCenter,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: _manageApplicationsPageStyles.applicantColumnFlex,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  scholarshipApplicationModel.name,
                  style: TextStyle(
                    fontSize: _manageApplicationsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: _manageApplicationsPageStyles.submissionDateColumnFlex,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  convertMillisecondsToDateString(scholarshipApplicationModel.ts, formats: [yyyy, '-', mm, '-', dd]),
                  style: TextStyle(
                    fontSize: _manageApplicationsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: _manageApplicationsPageStyles.scoreColumnFlex,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  (scholarshipApplicationModel.score == null) ? "0" : scholarshipApplicationModel.score.toString(),
                  style: TextStyle(
                    fontSize: _manageApplicationsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: _manageApplicationsPageStyles.categoryColumnFlex,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  scholarshipModel.name,
                  style: TextStyle(
                    fontSize: _manageApplicationsPageStyles.descriptionFontSize,
                    color: _scholarshipColors.descriptioinColor,
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
          Expanded(
            flex: _manageApplicationsPageStyles.evaluationColumnFlex,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                ScholarshipRatingBar(
                  rating: scholarshipApplicationModel.evaluation,
                  spacing: _manageApplicationsPageStyles.shareWidthDp * 8,
                  evaluatorSize: _manageApplicationsPageStyles.evaluatorSize,
                  borderWidth: _manageApplicationsPageStyles.shareWidthDp * 2,
                  defaultColor: _scholarshipColors.secondaryColor.withAlpha(50),
                  defaultBackgroundColor: _scholarshipColors.panelBackgroundColor,
                  selectedColor: _scholarshipColors.secondaryColor,
                  isReadOnly: true,
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              CustomRaisedButton(
                width: _manageApplicationsPageStyles.shareWidthDp * 100,
                height: _manageApplicationsPageStyles.textFontSize * 2,
                color: _scholarshipColors.secondaryColor,
                borderColor: _scholarshipColors.secondaryColor,
                borderRadius: 0,
                child: Text(
                  (scholarshipApplicationModel.isReviewed)
                      ? ManageApplicationsPageString.editButtonText
                      : (scholarshipApplicationModel.isRead)
                          ? ManageApplicationsPageString.reviewButtonText
                          : ManageApplicationsPageString.readButtonText,
                  style: TextStyle(fontSize: _manageApplicationsPageStyles.textFontSize, color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).pushNamed(
                    AppRoutes.updateApplication,
                    arguments: {
                      "initScholarshipApplicationModel": scholarshipApplicationModel,
                      "scholarshipModelID": scholarshipModel.id,
                    },
                  );
                },
              ),
              SizedBox(width: _manageApplicationsPageStyles.shareWidthDp * 15),
              SettingButtonForApplication(
                child: Icon(Icons.more_horiz, size: _manageApplicationsPageStyles.textFontSize * 1.2, color: _scholarshipColors.textColor),
                scholarshipModelID: scholarshipModel.id,
                initScholarshipApplicationModel: scholarshipApplicationModel,
              ),
              // ,
            ],
          )
        ],
      ),
    );
  }

  Widget _containerApplicationRowForMobile(
    BuildContext context,
    ManageApplicationsState manageApplicationsState,
    ScholarshipModel scholarshipModel,
    ScholarshipApplicationModel scholarshipApplicationModel,
  ) {
    return Container(
      height: _manageApplicationsPageStyles.rowHeight,
      padding: EdgeInsets.symmetric(
        horizontal: _manageApplicationsPageStyles.rowHorizontalPadding,
        vertical: _manageApplicationsPageStyles.shareWidthDp * 10,
      ),
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: _scholarshipColors.secondaryColor))),
      alignment: Alignment.bottomCenter,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  scholarshipApplicationModel.name,
                  style: TextStyle(fontSize: _manageApplicationsPageStyles.textFontSize, color: _scholarshipColors.textColor),
                ),
                Text(
                  scholarshipModel.name,
                  style: TextStyle(fontSize: _manageApplicationsPageStyles.descriptionFontSize, color: _scholarshipColors.descriptioinColor),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  convertMillisecondsToDateString(scholarshipApplicationModel.ts, formats: [yyyy, '-', mm, '-', dd]),
                  style: TextStyle(fontSize: _manageApplicationsPageStyles.descriptionFontSize, color: _scholarshipColors.textColor),
                ),
              ],
            ),
          ),
          SizedBox(width: _manageApplicationsPageStyles.shareWidthDp * 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: _manageApplicationsPageStyles.evaluatorSize,
                height: _manageApplicationsPageStyles.evaluatorSize,
                decoration: BoxDecoration(
                    border: Border.all(width: 1, color: _scholarshipColors.secondaryColor),
                    borderRadius: BorderRadius.all(Radius.circular(4)),
                    color: (scholarshipApplicationModel.evaluation == 0) ? Colors.transparent : _scholarshipColors.secondaryColor),
                child: (scholarshipApplicationModel.evaluation != 0)
                    ? Center(
                        child: Text(
                          scholarshipApplicationModel.evaluation.toString(),
                          style: TextStyle(fontSize: _manageApplicationsPageStyles.descriptionFontSize, color: Colors.white),
                        ),
                      )
                    : SizedBox(),
              )
            ],
          ),
          SizedBox(width: _manageApplicationsPageStyles.shareWidthDp * 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SettingButtonForApplication(
                child: Icon(Icons.more_vert, size: _manageApplicationsPageStyles.textFontSize * 1.5, color: _scholarshipColors.textColor),
                scholarshipModelID: scholarshipModel.id,
                initScholarshipApplicationModel: scholarshipApplicationModel,
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _containerApplicationList(BuildContext context, ManageApplicationsState manageApplicationsState) {
    try {
      return StreamBuilder<List<ScholarshipApplicationModel>>(
        stream: manageApplicationsState.applictionListStream,
        builder: (context, applicationSnapshot) {
          if (manageApplicationsState.loadingStateForApplictionListStream == -1)
            return Container(
              height: _manageApplicationsPageStyles.shareWidthDp * 200,
              child: Center(
                child: Text(
                  "Failed. Please try it",
                  style: TextStyle(
                    fontSize: _manageApplicationsPageStyles.filterFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
              ),
            );
          if (!applicationSnapshot.hasData ||
              manageApplicationsState.loadingStateForApplictionListStream == 1 ||
              manageApplicationsState.loadingStateForApplictionListStream == 0)
            return Container(
              height: _manageApplicationsPageStyles.shareWidthDp * 200,
              child: Center(
                child: CustomCupertinoIndicator(
                  brightness: _scholarshipColors.brightness,
                  size: _manageApplicationsPageStyles.shareWidthDp * 15,
                ),
              ),
            );
          if (applicationSnapshot.hasData && manageApplicationsState.loadingStateForApplictionListStream == 2 && applicationSnapshot.data.length == 0)
            return Container(
              height: _manageApplicationsPageStyles.shareWidthDp * 200,
              child: Center(
                child: Text(
                  "No Data",
                  style: TextStyle(
                    fontSize: _manageApplicationsPageStyles.filterFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
              ),
            );

          List<ScholarshipApplicationModel> applicationModelList = _applicationOrder(manageApplicationsState, applicationSnapshot.data);
          print("_________${applicationModelList.length}_____________");
          List<Widget> listWidget = _applicationFilter(manageApplicationsState, applicationModelList);
          if (listWidget.length == 0)
            return Container(
              height: _manageApplicationsPageStyles.shareWidthDp * 200,
              child: Center(
                child: Text(
                  "No Data",
                  style: TextStyle(
                    fontSize: _manageApplicationsPageStyles.filterFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
              ),
            );
          return Column(
            children: listWidget,
          );
        },
      );
    } catch (e) {
      print(e);
      return SizedBox();
    }
  }

  _applicationOrder(ManageApplicationsState manageApplicationsState, List<ScholarshipApplicationModel> applicationModelList) {
    try {
      applicationModelList.sort((a, b) {
        switch (manageApplicationsState.orderby["available"]) {
          case "applicant":
            bool desc = manageApplicationsState.orderby["applicant"];
            if (desc)
              return a.name.toLowerCase().compareTo(b.name.toLowerCase()) * -1;
            else
              return a.name.toLowerCase().compareTo(b.name.toLowerCase());

            break;
          case "submission":
            bool desc = manageApplicationsState.orderby["submission"];
            if (desc)
              return a.ts.compareTo(b.ts) * -1;
            else
              return a.ts.compareTo(b.ts);
            break;
          case "score":
            bool desc = manageApplicationsState.orderby["score"];
            if (a.score == null) a.score = 0;
            if (b.score == null) b.score = 0;
            if (desc) {
              return a.score.compareTo(b.score) * -1;
            } else {
              return a.score.compareTo(b.score);
            }
            break;
          default:
        }
        return 1;
      });

      return applicationModelList;
    } catch (e) {
      print(e);
      return applicationModelList;
    }
  }

  List<Widget> _applicationFilter(ManageApplicationsState manageApplicationsState, List<ScholarshipApplicationModel> applicationModelList) {
    List<Widget> listWidget = [];
    for (var i = 0; i < applicationModelList.length; i++) {
      bool isShown = false;
      switch (manageApplicationsState.selectedFilter) {
        case 0: //// all applicant
          isShown = true;
          break;
        case 1: //// unread
          if (applicationModelList[i].isRead == null || !applicationModelList[i].isRead) {
            isShown = true;
          }
          break;
        case 2: //// reviewd
          if (applicationModelList[i].isReviewed) {
            isShown = true;
          }
          break;
        default:
          isShown = true;
          break;
      }

      if (isShown) {
        Stream<ScholarshipModel> scholarshipStream = _scholarshipRepository.getScholarshipStreamByID(id: applicationModelList[i].scholarshipID);
        listWidget.add(StreamBuilder(
          stream: scholarshipStream,
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return Container(
                height: _manageApplicationsPageStyles.rowHeight,
                child: Center(
                  child: CustomCupertinoIndicator(
                    brightness: _scholarshipColors.brightness,
                    size: _manageApplicationsPageStyles.shareWidthDp * 15,
                  ),
                ),
              );
            return (_manageApplicationsPageStyles.runtimeType == ManageApplicationsPageMobileStyles)
                ? _containerApplicationRowForMobile(context, manageApplicationsState, snapshot.data, applicationModelList[i])
                : _containerApplicationRowForDesktop(context, manageApplicationsState, snapshot.data, applicationModelList[i]);
          },
        ));
      }
    }

    return listWidget;
  }
}
