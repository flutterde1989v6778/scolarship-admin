import 'package:easy_localization/easy_localization.dart' as EL;

class ManageApplicationsPageString {
  static String returnText = "ManageApplicationsPage.returnText".tr();
  static String title = "ManageApplicationsPage.title".tr();
  static String filter = "ManageApplicationsPage.filter".tr();
  static String filter1 = "ManageApplicationsPage.filter1".tr();
  static String filter2 = "ManageApplicationsPage.filter2".tr();
  static String filter3 = "ManageApplicationsPage.filter3".tr();
  static String filter4 = "ManageApplicationsPage.filter4".tr();
  static String applicantColumnHeader = "ManageApplicationsPage.applicantColumnHeader".tr();
  static String submissionDateColumnHeader = "ManageApplicationsPage.submissionDateColumnHeader".tr();
  static String scoreColumnHeader = "ManageApplicationsPage.scoreColumnHeader".tr();
  static String categoryColumnHeader = "ManageApplicationsPage.categoryColumnHeader".tr();
  static String evaluationsColumnHeader = "ManageApplicationsPage.evaluationsColumnHeader".tr();
  static String readButtonText = "ManageApplicationsPage.readButtonText".tr();
  static String reviewButtonText = "ManageApplicationsPage.reviewButtonText".tr();
  static String editButtonText = "ManageApplicationsPage.editButtonText".tr();
}
