import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:scholarship/Config/resposible_settings.dart';

class ManageApplicationsPageStyles {
  double devicePixelRatio;
  double deviceWidth;
  double deviceHeight;
  double fontUnit;
  double statusbarHeight;
  double bottombarHeight;
  double appbarHeight;
  double safeAreaHeight;
  double shareWidth;
  double shareHeight;
  double shareWidthDp;
  double shareHeightDp;

  double primaryHorizontalPadding;
  double primaryVerticalPadding;

  double titleHorizontalPadding;
  double titleFontSize;
  double filterFontSize;

  double applicationListHorizontalPading;
  double applicationListVerticalPadding;
  double rowHeight;
  double rowHorizontalPadding;
  double rowTopPadding;
  double rowBottomPadding;
  double textFontSize;
  double descriptionFontSize;
  double evaluatorSize;
  double buttonWidth;

  int applicantColumnFlex;
  int submissionDateColumnFlex;
  int scoreColumnFlex;
  int categoryColumnFlex;
  int evaluationColumnFlex;

  ManageApplicationsPageStyles(BuildContext context) {}
}

class ManageApplicationsPageDesktopStyles extends ManageApplicationsPageStyles {
  ManageApplicationsPageDesktopStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(0);
    primaryVerticalPadding = ScreenUtil().setWidth(10);

    /// mainStyle
    titleHorizontalPadding = ScreenUtil().setWidth(33);
    titleFontSize = ScreenUtil().setSp(38);
    filterFontSize = ScreenUtil().setSp(18);

    applicationListHorizontalPading = ScreenUtil().setWidth(47);
    applicationListVerticalPadding = ScreenUtil().setWidth(20);
    rowHeight = ScreenUtil().setWidth(75);
    rowHorizontalPadding = 0;
    rowTopPadding = ScreenUtil().setWidth(15);
    rowBottomPadding = ScreenUtil().setWidth(15);
    textFontSize = ScreenUtil().setSp(18);
    descriptionFontSize = ScreenUtil().setSp(11);
    evaluatorSize = ScreenUtil().setWidth(35);
    buttonWidth = ScreenUtil().setWidth(90);

    applicantColumnFlex = 4;
    submissionDateColumnFlex = 3;
    scoreColumnFlex = 2;
    categoryColumnFlex = 4;
    evaluationColumnFlex = 4;
  }
}

class ManageApplicationsPageTabletStyles extends ManageApplicationsPageStyles {
  ManageApplicationsPageTabletStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(0);
    primaryVerticalPadding = ScreenUtil().setWidth(10);

    /// mainStyle
    titleHorizontalPadding = ScreenUtil().setWidth(33);
    titleFontSize = ScreenUtil().setSp(29);
    filterFontSize = ScreenUtil().setSp(18);

    applicationListHorizontalPading = ScreenUtil().setWidth(30);
    applicationListVerticalPadding = ScreenUtil().setWidth(20);
    rowHeight = ScreenUtil().setWidth(75);
    rowHorizontalPadding = 0;
    rowTopPadding = ScreenUtil().setWidth(15);
    rowBottomPadding = ScreenUtil().setWidth(15);
    textFontSize = ScreenUtil().setSp(18);
    descriptionFontSize = ScreenUtil().setSp(11);
    evaluatorSize = ScreenUtil().setWidth(35);
    buttonWidth = ScreenUtil().setWidth(90);

    applicantColumnFlex = 4;
    submissionDateColumnFlex = 3;
    scoreColumnFlex = 2;
    categoryColumnFlex = 4;
    evaluationColumnFlex = 4;
  }
}

class ManageApplicationsPageMobileStyles extends ManageApplicationsPageStyles {
  ManageApplicationsPageMobileStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.mobileDesignWidth,
      height: ResponsibleDesignSettings.mobileDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(0);
    primaryVerticalPadding = ScreenUtil().setWidth(5);

    /// mainStyle
    titleHorizontalPadding = ScreenUtil().setWidth(16);
    titleFontSize = ScreenUtil().setSp(24);
    filterFontSize = ScreenUtil().setSp(16);

    applicationListHorizontalPading = ScreenUtil().setWidth(0);
    applicationListVerticalPadding = ScreenUtil().setWidth(20);
    rowHeight = ScreenUtil().setWidth(75);
    rowHorizontalPadding = ScreenUtil().setWidth(19);
    rowTopPadding = ScreenUtil().setWidth(15);
    rowBottomPadding = ScreenUtil().setWidth(7);
    textFontSize = ScreenUtil().setSp(14);
    descriptionFontSize = ScreenUtil().setSp(9);
    evaluatorSize = ScreenUtil().setWidth(21);
  }
}
