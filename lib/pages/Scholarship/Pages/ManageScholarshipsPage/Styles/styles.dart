import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:scholarship/Config/resposible_settings.dart';

class ManageScholarshipsPageStyles {
  double devicePixelRatio;
  double deviceWidth;
  double deviceHeight;
  double fontUnit;
  double statusbarHeight;
  double bottombarHeight;
  double appbarHeight;
  double safeAreaHeight;
  double shareWidth;
  double shareHeight;
  double shareWidthDp;
  double shareHeightDp;

  double primaryHorizontalPadding;
  double primaryVerticalPadding;

  double titleHorizontalPadding;
  double titleFontSize;

  double scholarshipListHorizontalPading;
  double scholarshipListVerticalPadding;
  double rowHeight;
  double rowHorizontalPadding;
  double rowTopPadding;
  double rowBottomPadding;
  double textFontSize;
  double descriptionFontSize;
  double buttonWidth;

  double newButtonWidth;
  double newButtonHeight;
  double newButtonFontSize;

  int nameColumnFlex;
  int statusColumnFlex;
  int deadlineColumnFlex;
  int briefColumnFlex;

  ManageScholarshipsPageStyles(BuildContext context) {}
}

class ManageScholarshipsPageDesktopStyles extends ManageScholarshipsPageStyles {
  ManageScholarshipsPageDesktopStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(0);
    primaryVerticalPadding = ScreenUtil().setWidth(10);

    /// mainStyle
    titleHorizontalPadding = ScreenUtil().setWidth(33);
    titleFontSize = ScreenUtil().setSp(38);

    scholarshipListHorizontalPading = ScreenUtil().setWidth(47);
    scholarshipListVerticalPadding = ScreenUtil().setWidth(20);
    rowHeight = ScreenUtil().setWidth(75);
    rowHorizontalPadding = 0;
    rowTopPadding = ScreenUtil().setWidth(15);
    rowBottomPadding = ScreenUtil().setWidth(15);
    textFontSize = ScreenUtil().setSp(18);
    descriptionFontSize = ScreenUtil().setSp(11);
    buttonWidth = ScreenUtil().setWidth(90);

    newButtonWidth = ScreenUtil().setWidth(100);
    newButtonHeight = ScreenUtil().setWidth(40);
    newButtonFontSize = ScreenUtil().setSp(18);

    nameColumnFlex = 4;
    statusColumnFlex = 1;
    deadlineColumnFlex = 2;
    briefColumnFlex = 4;
  }
}

class ManageScholarshipsPageTabletStyles extends ManageScholarshipsPageStyles {
  ManageScholarshipsPageTabletStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(0);
    primaryVerticalPadding = ScreenUtil().setWidth(10);

    /// mainStyle
    titleHorizontalPadding = ScreenUtil().setWidth(33);
    titleFontSize = ScreenUtil().setSp(29);

    scholarshipListHorizontalPading = ScreenUtil().setWidth(30);
    scholarshipListVerticalPadding = ScreenUtil().setWidth(20);
    rowHeight = ScreenUtil().setWidth(75);
    rowHorizontalPadding = 0;
    rowTopPadding = ScreenUtil().setWidth(15);
    rowBottomPadding = ScreenUtil().setWidth(15);
    textFontSize = ScreenUtil().setSp(18);
    descriptionFontSize = ScreenUtil().setSp(11);
    buttonWidth = ScreenUtil().setWidth(90);

    newButtonWidth = ScreenUtil().setWidth(100);
    newButtonHeight = ScreenUtil().setWidth(40);
    newButtonFontSize = ScreenUtil().setSp(18);

    nameColumnFlex = 4;
    statusColumnFlex = 1;
    deadlineColumnFlex = 2;
    briefColumnFlex = 4;
  }
}

class ManageScholarshipsPageMobileStyles extends ManageScholarshipsPageStyles {
  ManageScholarshipsPageMobileStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.mobileDesignWidth,
      height: ResponsibleDesignSettings.mobileDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(0);
    primaryVerticalPadding = ScreenUtil().setWidth(5);

    /// mainStyle
    titleHorizontalPadding = ScreenUtil().setWidth(16);
    titleFontSize = ScreenUtil().setSp(24);

    scholarshipListHorizontalPading = ScreenUtil().setWidth(0);
    scholarshipListVerticalPadding = ScreenUtil().setWidth(20);
    rowHeight = ScreenUtil().setWidth(75);
    rowHorizontalPadding = ScreenUtil().setWidth(19);
    rowTopPadding = ScreenUtil().setWidth(15);
    rowBottomPadding = ScreenUtil().setWidth(7);
    textFontSize = ScreenUtil().setSp(14);
    descriptionFontSize = ScreenUtil().setSp(9);

    newButtonWidth = ScreenUtil().setWidth(32);
    newButtonHeight = ScreenUtil().setWidth(32);
    newButtonFontSize = ScreenUtil().setSp(14);
  }
}
