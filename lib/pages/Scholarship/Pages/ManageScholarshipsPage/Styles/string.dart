import 'package:easy_localization/easy_localization.dart' as EL;

class ManageScholarshipsPageString {
  static String title = "ManageScholarshipPage.title".tr();
  static String nameColumnHeader = "ManageScholarshipPage.nameColumnHeader".tr();
  static String statusColumnHeader = "ManageScholarshipPage.statusColumnHeader".tr();
  static String deadlineColumnHeader = "ManageScholarshipPage.deadlineColumnHeader".tr();
  static String briefColumnHeader = "ManageScholarshipPage.briefColumnHeader".tr();
  static String actionColumnHeader = "ManageScholarshipPage.actionColumnHeader".tr();
  static String openString = "ManageScholarshipPage.openString".tr();
  static String closeString = "ManageScholarshipPage.closeString".tr();
  static String editButtonText = "ManageScholarshipPage.editButtonText".tr();
  static String newButtonText = "ManageScholarshipPage.newButtonText".tr();
}
