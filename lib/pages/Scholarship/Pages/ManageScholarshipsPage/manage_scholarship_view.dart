import 'dart:ui';

import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart' as EL;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:scholarship/blocs/index.dart';
import 'package:scholarship/models/index.dart';
import 'package:scholarship/pages/App/index.dart';
import 'package:scholarship/pages/Scholarship/Components/index.dart';
import 'package:scholarship/pages/Scholarship/Styles/index.dart';
import 'package:scholarship/utils/index.dart';
import 'package:scholarship/widgets/index.dart';

import 'index.dart';

class ManageScholarshipsView extends StatefulWidget {
  ManageScholarshipsView({@required this.scholarshipPageColors});

  ScholarshipColors scholarshipPageColors;

  State<ManageScholarshipsView> createState() => _ManageScholarshipsViewState();
}

class _ManageScholarshipsViewState extends State<ManageScholarshipsView> with TickerProviderStateMixin {
  ManageScholarshipsPageStyles _manageScholarshipsPageStyles;
  ScholarshipColors _scholarshipColors;
  ManageScholarshipsBloc _manageScholarshipsBloc;
  GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  String _email;
  String _password;

  @override
  void initState() {
    super.initState();
    _manageScholarshipsBloc = BlocProvider.of<ManageScholarshipsBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("___________ManageScholarshipsView__build______________");
    _scholarshipColors = widget.scholarshipPageColors;

    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth >= 900) {
          _manageScholarshipsPageStyles = ManageScholarshipsPageDesktopStyles(context);
        } else if (constraints.maxWidth >= 600 && constraints.maxWidth < 900) {
          ////  tablet
          _manageScholarshipsPageStyles = ManageScholarshipsPageTabletStyles(context);
        } else if (constraints.maxWidth < 600) {
          _manageScholarshipsPageStyles = ManageScholarshipsPageMobileStyles(context);
        }
        print("___ManageScholarshipsView__LayoutBuilder___${_manageScholarshipsPageStyles.deviceWidth}_______");

        return SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Topbar(loginState: true),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: _manageScholarshipsPageStyles.primaryHorizontalPadding,
                    vertical: _manageScholarshipsPageStyles.primaryVerticalPadding,
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: _manageScholarshipsPageStyles.titleHorizontalPadding),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              ManageScholarshipsPageString.title,
                              style: TextStyle(fontSize: _manageScholarshipsPageStyles.titleFontSize, color: _scholarshipColors.textColor),
                            ),
                            (_manageScholarshipsPageStyles.runtimeType != ManageScholarshipsPageMobileStyles)
                                ? CustomRaisedButton(
                                    width: _manageScholarshipsPageStyles.newButtonWidth,
                                    height: _manageScholarshipsPageStyles.newButtonHeight,
                                    color: _scholarshipColors.backgroundColor,
                                    borderColor: _scholarshipColors.secondaryColor,
                                    borderRadius: _manageScholarshipsPageStyles.newButtonHeight / 2,
                                    borderWidth: 3,
                                    child: Text(
                                      "+ ${ManageScholarshipsPageString.newButtonText}",
                                      style: TextStyle(
                                        fontSize: _manageScholarshipsPageStyles.newButtonFontSize,
                                        color: _scholarshipColors.textColor,
                                      ),
                                    ),
                                    onPressed: () {
                                      Navigator.of(context).pushNamed(AppRoutes.newScholarship,
                                          arguments: {"category": "new", "initScholarshipModel": ScholarshipModel()});
                                    },
                                  )
                                : GestureDetector(
                                    child: Icon(
                                      Icons.add_circle_outline,
                                      size: _manageScholarshipsPageStyles.newButtonWidth,
                                      color: _scholarshipColors.textColor,
                                    ),
                                    onTap: () {
                                      Navigator.of(context).pushNamed(AppRoutes.newScholarship,
                                          arguments: {"category": "new", "initScholarshipModel": ScholarshipModel()});
                                    },
                                  ),
                          ],
                        ),
                      ),
                      _containerBody(context),
                    ],
                  ),
                ),
                Footer(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _containerBody(BuildContext context) {
    return BlocBuilder<ManageScholarshipsBloc, ManageScholarshipsState>(
      condition: (previous, next) {
        return true;
      },
      builder: (context, state) {
        if (state.loadingState == 0) _manageScholarshipsBloc.add(GetScholarshipListStreamEvent());

        return Container(
            padding: EdgeInsets.symmetric(
              horizontal: _manageScholarshipsPageStyles.scholarshipListHorizontalPading,
              vertical: _manageScholarshipsPageStyles.scholarshipListVerticalPadding,
            ),
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: _manageScholarshipsPageStyles.scholarshipListHorizontalPading,
                    vertical: _manageScholarshipsPageStyles.scholarshipListVerticalPadding,
                  ),
                  child: Column(
                    children: <Widget>[
                      (_manageScholarshipsPageStyles.runtimeType != ManageScholarshipsPageMobileStyles)
                          ? _containerListHeader(context, state)
                          : Divider(height: 1, thickness: 1, color: _scholarshipColors.secondaryColor),
                      _containerApplicationList(context, state),
                    ],
                  ),
                )
              ],
            ));
      },
    );
  }

  Widget _containerListHeader(BuildContext context, ManageScholarshipsState manageScholarshipsState) {
    return Container(
      height: _manageScholarshipsPageStyles.rowHeight,
      padding: EdgeInsets.symmetric(
        horizontal: _manageScholarshipsPageStyles.rowHorizontalPadding,
        vertical: _manageScholarshipsPageStyles.shareWidthDp * 10,
      ),
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: _scholarshipColors.secondaryColor))),
      alignment: Alignment.bottomCenter,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: _manageScholarshipsPageStyles.nameColumnFlex,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  ManageScholarshipsPageString.nameColumnHeader,
                  style: TextStyle(
                    fontSize: _manageScholarshipsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
                SizedBox(width: _manageScholarshipsPageStyles.shareWidthDp * 5),
                FaIcon(
                  FontAwesomeIcons.sortAlphaDown,
                  color: _scholarshipColors.secondaryColor,
                  size: _manageScholarshipsPageStyles.textFontSize * 1.2,
                )
              ],
            ),
          ),
          Expanded(
            flex: _manageScholarshipsPageStyles.statusColumnFlex,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  ManageScholarshipsPageString.statusColumnHeader,
                  style: TextStyle(
                    fontSize: _manageScholarshipsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
                SizedBox(width: _manageScholarshipsPageStyles.shareWidthDp * 5),
                FaIcon(
                  FontAwesomeIcons.sortAmountUpAlt,
                  color: _scholarshipColors.secondaryColor,
                  size: _manageScholarshipsPageStyles.textFontSize * 1.2,
                ),
              ],
            ),
          ),
          Expanded(
            flex: _manageScholarshipsPageStyles.deadlineColumnFlex,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  ManageScholarshipsPageString.deadlineColumnHeader,
                  style: TextStyle(
                    fontSize: _manageScholarshipsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
                SizedBox(width: _manageScholarshipsPageStyles.shareWidthDp * 5),
                FaIcon(
                  FontAwesomeIcons.sortAmountUpAlt,
                  color: _scholarshipColors.secondaryColor,
                  size: _manageScholarshipsPageStyles.textFontSize * 1.2,
                ),
              ],
            ),
          ),
          Expanded(
            flex: _manageScholarshipsPageStyles.briefColumnFlex,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  ManageScholarshipsPageString.briefColumnHeader,
                  style: TextStyle(
                    fontSize: _manageScholarshipsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              CustomRaisedButton(
                width: _manageScholarshipsPageStyles.shareWidthDp * 100,
                height: _manageScholarshipsPageStyles.textFontSize * 2,
                color: _scholarshipColors.backgroundColor,
                borderColor: _scholarshipColors.backgroundColor,
                borderRadius: 0,
                elevation: 0,
                child: Text(
                  ManageScholarshipsPageString.editButtonText,
                  style: TextStyle(fontSize: _manageScholarshipsPageStyles.textFontSize, color: _scholarshipColors.backgroundColor),
                ),
              ),
              SizedBox(width: _manageScholarshipsPageStyles.shareWidthDp * 15),
              Icon(Icons.more_horiz, size: _manageScholarshipsPageStyles.textFontSize * 1.2, color: _scholarshipColors.backgroundColor),
            ],
          ),
        ],
      ),
    );
  }

  Widget _containerScholarshipRowForDesktop(
    BuildContext context,
    ManageScholarshipsState manageScholarshipsState,
    ScholarshipModel scholarshipModel,
  ) {
    return Container(
      height: _manageScholarshipsPageStyles.rowHeight,
      padding: EdgeInsets.symmetric(
        horizontal: _manageScholarshipsPageStyles.rowHorizontalPadding,
        vertical: _manageScholarshipsPageStyles.shareWidthDp * 10,
      ),
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: _scholarshipColors.secondaryColor))),
      alignment: Alignment.bottomCenter,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: _manageScholarshipsPageStyles.nameColumnFlex,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  scholarshipModel.name,
                  style: TextStyle(
                    fontSize: _manageScholarshipsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: _manageScholarshipsPageStyles.statusColumnFlex,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  (scholarshipModel.isOpened ? ManageScholarshipsPageString.openString : ManageScholarshipsPageString.closeString),
                  style: TextStyle(
                    fontSize: _manageScholarshipsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: _manageScholarshipsPageStyles.deadlineColumnFlex,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  convertMillisecondsToDateString(scholarshipModel.deadlines, formats: [yyyy, '-', mm, '-', dd]),
                  style: TextStyle(
                    fontSize: _manageScholarshipsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: _manageScholarshipsPageStyles.briefColumnFlex,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  scholarshipModel.description,
                  style: TextStyle(
                    fontSize: _manageScholarshipsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              CustomRaisedButton(
                width: _manageScholarshipsPageStyles.shareWidthDp * 100,
                height: _manageScholarshipsPageStyles.textFontSize * 2,
                color: _scholarshipColors.secondaryColor,
                borderColor: _scholarshipColors.secondaryColor,
                borderRadius: 0,
                child: Text(
                  ManageScholarshipsPageString.editButtonText,
                  style: TextStyle(fontSize: _manageScholarshipsPageStyles.textFontSize, color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).pushNamed(
                    AppRoutes.newScholarship,
                    arguments: {"category": "edit", "initScholarshipModel": scholarshipModel},
                  );
                },
              ),
              SizedBox(width: _manageScholarshipsPageStyles.shareWidthDp * 15),
              SettingButtonForScholarship(
                category: "edit",
                initScholarshipModel: scholarshipModel,
                child: Icon(Icons.more_horiz, size: _manageScholarshipsPageStyles.textFontSize * 1.2, color: _scholarshipColors.textColor),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _containerScholarshipRowForMobile(
    BuildContext context,
    ManageScholarshipsState manageScholarshipsState,
    ScholarshipModel scholarshipModel,
  ) {
    return Container(
      height: _manageScholarshipsPageStyles.rowHeight,
      padding: EdgeInsets.symmetric(
        horizontal: _manageScholarshipsPageStyles.rowHorizontalPadding,
        vertical: _manageScholarshipsPageStyles.shareWidthDp * 10,
      ),
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: _scholarshipColors.secondaryColor))),
      alignment: Alignment.bottomCenter,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  scholarshipModel.name,
                  style: TextStyle(
                    fontSize: _manageScholarshipsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
                Row(
                  children: <Widget>[
                    Text(
                      (scholarshipModel.isOpened ? ManageScholarshipsPageString.openString : ManageScholarshipsPageString.closeString),
                      style: TextStyle(
                        fontSize: _manageScholarshipsPageStyles.descriptionFontSize,
                        color: _scholarshipColors.descriptioinColor,
                      ),
                    ),
                    Text(
                      "  |  ${ManageScholarshipsPageString.deadlineColumnHeader}:  ",
                      style: TextStyle(
                        fontSize: _manageScholarshipsPageStyles.descriptionFontSize,
                        color: _scholarshipColors.descriptioinColor,
                      ),
                    ),
                    Text(
                      convertMillisecondsToDateString(scholarshipModel.deadlines, formats: [yyyy, '-', mm, '-', dd]),
                      style: TextStyle(
                        fontSize: _manageScholarshipsPageStyles.descriptionFontSize,
                        color: _scholarshipColors.descriptioinColor,
                      ),
                    ),
                  ],
                ),
                Text(
                  scholarshipModel.description,
                  style: TextStyle(
                    fontSize: _manageScholarshipsPageStyles.descriptionFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                )
              ],
            ),
          ),
          SizedBox(width: _manageScholarshipsPageStyles.shareWidthDp * 10),
          SettingButtonForScholarship(
            category: "edit",
            initScholarshipModel: scholarshipModel,
            child: Icon(Icons.more_vert, size: _manageScholarshipsPageStyles.textFontSize * 1.2, color: _scholarshipColors.textColor),
          ),
        ],
      ),
    );
  }

  Widget _containerApplicationList(BuildContext context, ManageScholarshipsState manageScholarshipsState) {
    try {
      return StreamBuilder<List<ScholarshipModel>>(
        stream: manageScholarshipsState.scholarshipListStream,
        builder: (context, snapshot) {
          if (manageScholarshipsState.loadingState == -1)
            return Container(
              height: _manageScholarshipsPageStyles.shareWidthDp * 100,
              child: Center(
                child: Text(
                  "Failed. Please try it",
                  style: TextStyle(
                    fontSize: _manageScholarshipsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
              ),
            );
          if (!snapshot.hasData || manageScholarshipsState.loadingState == 1 || manageScholarshipsState.loadingState == 0)
            return Container(
              height: _manageScholarshipsPageStyles.shareWidthDp * 100,
              child: Center(
                child: CustomCupertinoIndicator(
                  brightness: _scholarshipColors.brightness,
                  size: _manageScholarshipsPageStyles.shareWidthDp * 15,
                ),
              ),
            );
          if (snapshot.hasData && manageScholarshipsState.loadingState == 2 && snapshot.data.length == 0)
            return Container(
              height: _manageScholarshipsPageStyles.shareWidthDp * 100,
              child: Center(
                child: Text(
                  "No Data",
                  style: TextStyle(
                    fontSize: _manageScholarshipsPageStyles.textFontSize,
                    color: _scholarshipColors.textColor,
                  ),
                ),
              ),
            );

          List<ScholarshipModel> scholarshipItems = snapshot.data;

          return Column(
            children: scholarshipItems.map((scholarshipModel) {
              return (_manageScholarshipsPageStyles.runtimeType == ManageScholarshipsPageMobileStyles)
                  ? _containerScholarshipRowForMobile(context, manageScholarshipsState, scholarshipModel)
                  : _containerScholarshipRowForDesktop(context, manageScholarshipsState, scholarshipModel);
            }).toList(),
          );
        },
      );
    } catch (e) {
      print(e);
      return SizedBox();
    }
  }
}
