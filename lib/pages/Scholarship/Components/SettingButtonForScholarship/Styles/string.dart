import 'package:easy_localization/easy_localization.dart' as EL;

class SettingButtonForScholarshipString {
  static String actionsItem = "SettingButtonForScholarship.actionsItem".tr();
  static String editApplication = "SettingButtonForScholarship.editApplication".tr();
  static String deleteApplication = "SettingButtonForScholarship.deleteApplication".tr();
  static String closeApplication = "SettingButtonForScholarship.closeApplication".tr();
}
