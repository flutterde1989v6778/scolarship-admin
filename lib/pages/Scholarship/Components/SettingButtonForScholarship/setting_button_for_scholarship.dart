import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart' as EL;
import 'package:scholarship/blocs/index.dart';
import 'package:scholarship/models/index.dart';
import 'package:scholarship/pages/App/index.dart';
import 'package:scholarship/pages/Scholarship/Styles/index.dart';

import 'index.dart';

class SettingButtonForScholarship extends StatelessWidget {
  SettingButtonForScholarship({
    @required this.initScholarshipModel,
    @required this.category,
    @required this.child,
  });
  ScholarshipColors _scholarshipColors;
  SettingButtonForScholarshipStyles _settingButtonForScholarshipStyles;
  Widget child;
  ScholarshipModel initScholarshipModel;
  String category;

  @override
  Widget build(BuildContext context) {
    _scholarshipColors = ScholarshipColors();
    _settingButtonForScholarshipStyles = SettingButtonForScholarshipStyles(context);
    return Container(
      child: PopupMenuButton(
        color: Color(0xFF181717),
        padding: EdgeInsets.zero,
        offset: Offset(0, 20),
        onSelected: (int index) async {
          switch (index) {
            case 1:
              break;
            case 2:
              Navigator.of(context).pushNamed(
                AppRoutes.newScholarship,
                arguments: {"category": "edit", "initScholarshipModel": initScholarshipModel},
              );
              break;
            case 3:
              break;
            default:
          }
        },
        child: child,
        itemBuilder: (BuildContext context) => [
          PopupMenuItem(
            height: 30,
            value: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  SettingButtonForScholarshipString.actionsItem,
                  style: TextStyle(fontSize: 15, color: Colors.white),
                ),
                Icon(Icons.cancel, size: 15, color: Colors.white)
              ],
            ),
          ),
          PopupMenuItem(
            height: 30,
            value: 2,
            child: Text(
              SettingButtonForScholarshipString.editApplication,
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
          ),
          PopupMenuItem(
            height: 30,
            value: 3,
            child: Text(
              SettingButtonForScholarshipString.deleteApplication,
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
          ),
          PopupMenuItem(
            height: 30,
            value: 4,
            child: Text(
              SettingButtonForScholarshipString.closeApplication,
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}
