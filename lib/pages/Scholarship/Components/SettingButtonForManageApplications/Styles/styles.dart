import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:meta/meta.dart';
import 'package:scholarship/Config/resposible_settings.dart';

class SettingButtonForManageApplicationsStyles {
  double devicePixelRatio;
  double deviceWidth;
  double deviceHeight;
  double fontUnit;
  double statusbarHeight;
  double bottombarHeight;
  double appbarHeight;
  double safeAreaHeight;
  double shareWidth;
  double shareHeight;
  double shareWidthDp;
  double shareHeightDp;

  double primaryHorizontalPadding;
  double primaryVerticalPadding;

  double iconSize;

  SettingButtonForManageApplicationsStyles(BuildContext context) {}
}

class SettingButtonForManageApplicationsDesktopStyles extends SettingButtonForManageApplicationsStyles {
  SettingButtonForManageApplicationsDesktopStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(13);
    primaryVerticalPadding = ScreenUtil().setWidth(13);

    iconSize = ScreenUtil().setWidth(20);
  }
}

class SettingButtonForManageApplicationsTabletStyles extends SettingButtonForManageApplicationsStyles {
  SettingButtonForManageApplicationsTabletStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(13);
    primaryVerticalPadding = ScreenUtil().setWidth(13);

    iconSize = ScreenUtil().setWidth(20);
  }
}

class SettingButtonForManageApplicationsMobileStyles extends SettingButtonForManageApplicationsStyles {
  SettingButtonForManageApplicationsMobileStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.mobileDesignWidth,
      height: ResponsibleDesignSettings.mobileDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(13);
    primaryVerticalPadding = ScreenUtil().setWidth(13);

    iconSize = ScreenUtil().setWidth(20);
  }
}
