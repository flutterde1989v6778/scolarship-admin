import 'package:easy_localization/easy_localization.dart' as EL;

class SettingButtonForManageApplicationsString {
  static String actionsItem = "SettingButtonForManageApplications.actionsItem".tr();
  static String newScholarshipItem = "SettingButtonForManageApplications.newScholarshipItem".tr();
  static String modifyScholarshipsItem = "SettingButtonForManageApplications.modifyScholarshipsItem".tr();
}
