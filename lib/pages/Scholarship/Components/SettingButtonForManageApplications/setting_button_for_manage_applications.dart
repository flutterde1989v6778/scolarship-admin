import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart' as EL;
import 'package:scholarship/blocs/index.dart';
import 'package:scholarship/models/index.dart';
import 'package:scholarship/pages/App/index.dart';
import 'package:scholarship/pages/Scholarship/Styles/index.dart';

import 'index.dart';

class SettingButtonForManageApplications extends StatelessWidget {
  ScholarshipColors _scholarshipColors;
  SettingButtonForManageApplicationsStyles _settingButtonForManageApplicationsStyles;
  @override
  Widget build(BuildContext context) {
    _scholarshipColors = ScholarshipColors();
    _settingButtonForManageApplicationsStyles = SettingButtonForManageApplicationsStyles(context);
    return Container(
      child: PopupMenuButton(
        color: Color(0xFF181717),
        padding: EdgeInsets.zero,
        offset: Offset(0, 20),
        onSelected: (int index) async {
          switch (index) {
            case 1:
              break;
            case 2:
              Navigator.of(context).pushNamed(AppRoutes.newScholarship, arguments: {"category": "new", "initScholarshipModel": ScholarshipModel()});
              break;
            case 3:
              AppRoutes.router.navigateTo(context, AppRoutes.manageScholarships, transition: TransitionType.fadeIn);
              break;
            default:
          }
        },
        child: Icon(
          Icons.settings,
          size: _settingButtonForManageApplicationsStyles.iconSize,
          color: _scholarshipColors.secondaryColor,
        ),
        itemBuilder: (BuildContext context) => [
          PopupMenuItem(
            height: 30,
            value: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  SettingButtonForManageApplicationsString.actionsItem,
                  style: TextStyle(fontSize: 15, color: Colors.white),
                ),
                Icon(Icons.cancel, size: 15, color: Colors.white)
              ],
            ),
          ),
          PopupMenuItem(
            height: 30,
            value: 2,
            child: Text(
              SettingButtonForManageApplicationsString.newScholarshipItem,
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
          ),
          PopupMenuItem(
            height: 30,
            value: 3,
            child: Text(
              SettingButtonForManageApplicationsString.modifyScholarshipsItem,
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}
