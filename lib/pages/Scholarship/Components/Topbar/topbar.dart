import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart' as EL;
import 'package:scholarship/blocs/index.dart';
import 'package:scholarship/pages/Scholarship/Styles/index.dart';

import 'index.dart';

class Topbar extends StatelessWidget {
  Topbar({this.returnText, this.loginState});

  bool loginState = false;
  String returnText = "";
  TopbarStyles _topbarStyles;
  ScholarshipColors _scholarshipColors;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeBloc, ThemeState>(builder: (context, state) {
      if (state.themeMode == ThemeModeConstants.dark) {
        _scholarshipColors = ScholarshipDarkModeColors();
      } else {
        _scholarshipColors = ScholarshipLightModeColors();
      }

      if (returnText == null) returnText = "";
      return LayoutBuilder(
        builder: (context, constraints) {
          if (constraints.maxWidth >= 900) {
            _topbarStyles = TopbarDesktopStyles(context);
          } else if (constraints.maxWidth >= 600 && constraints.maxWidth < 900) {
            ////  tablet
            _topbarStyles = TopbarTabletStyles(context);
          } else if (constraints.maxWidth < 600) {
            _topbarStyles = TopbarMobileStyles(context);
          }

          return Container(
            padding: EdgeInsets.symmetric(
              horizontal: _topbarStyles.primaryHorizontalPadding,
              vertical: _topbarStyles.primaryVerticalPadding,
            ),
            color: _scholarshipColors.backgroundColor,
            child: Container(
              height: _topbarStyles.topHeight,
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.arrow_back, color: _scholarshipColors.secondaryColor, size: _topbarStyles.returnTextFontSize * 1.5),
                        SizedBox(width: _topbarStyles.shareWidthDp * 10),
                        Text(
                          returnText,
                          style: TextStyle(fontSize: _topbarStyles.returnTextFontSize, color: _scholarshipColors.textColor),
                        )
                      ],
                    ),
                  ),
                  (loginState != null && loginState)
                      ? Row(
                          children: <Widget>[
                            Container(
                              width: _topbarStyles.avatarSize,
                              height: _topbarStyles.avatarSize,
                              decoration: BoxDecoration(
                                border: Border.all(width: 1, color: _scholarshipColors.secondaryColor),
                                borderRadius: BorderRadius.all(Radius.circular(_topbarStyles.avatarSize / 2)),
                              ),
                            ),
                            SizedBox(width: _topbarStyles.shareWidthDp * 10),
                            Text(
                              "User Name",
                              style: TextStyle(fontSize: _topbarStyles.userNametextFontSize, color: _scholarshipColors.textColor),
                            )
                          ],
                        )
                      : SizedBox(),
                ],
              ),
            ),
          );
        },
      );
    });
  }
}
