import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:meta/meta.dart';
import 'package:scholarship/Config/resposible_settings.dart';

class TopbarStyles {
  double devicePixelRatio;
  double deviceWidth;
  double deviceHeight;
  double fontUnit;
  double statusbarHeight;
  double bottombarHeight;
  double appbarHeight;
  double safeAreaHeight;
  double shareWidth;
  double shareHeight;
  double shareWidthDp;
  double shareHeightDp;

  double primaryHorizontalPadding;
  double primaryVerticalPadding;
  double topHeight;

  double returnTextFontSize;
  double userNametextFontSize;
  double avatarSize;

  TopbarStyles(BuildContext context) {}
}

class TopbarDesktopStyles extends TopbarStyles {
  TopbarDesktopStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(45);
    primaryVerticalPadding = ScreenUtil().setWidth(30);
    topHeight = ScreenUtil().setWidth(48);

    returnTextFontSize = ScreenUtil().setSp(24, allowFontScalingSelf: false);
    userNametextFontSize = ScreenUtil().setSp(18, allowFontScalingSelf: false);
    avatarSize = ScreenUtil().setWidth(48);
  }
}

class TopbarTabletStyles extends TopbarStyles {
  TopbarTabletStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(45);
    primaryVerticalPadding = ScreenUtil().setWidth(30);
    topHeight = ScreenUtil().setWidth(48);

    returnTextFontSize = ScreenUtil().setSp(24, allowFontScalingSelf: false);
    userNametextFontSize = ScreenUtil().setSp(18, allowFontScalingSelf: false);
    avatarSize = ScreenUtil().setWidth(48);
  }
}

class TopbarMobileStyles extends TopbarStyles {
  TopbarMobileStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.mobileDesignWidth,
      height: ResponsibleDesignSettings.mobileDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(15);
    primaryVerticalPadding = ScreenUtil().setWidth(17);
    topHeight = ScreenUtil().setWidth(26);

    returnTextFontSize = ScreenUtil().setSp(10, allowFontScalingSelf: false);
    userNametextFontSize = ScreenUtil().setSp(10, allowFontScalingSelf: false);
    avatarSize = ScreenUtil().setWidth(26);
  }
}
