import 'package:easy_localization/easy_localization.dart' as EL;

class SettingButtonForApplicationString {
  static String actionsItem = "SettingButtonForApplication.actionsItem".tr();
  static String editApplication = "SettingButtonForApplication.editApplication".tr();
  static String messageApplication = "SettingButtonForApplication.messageApplication".tr();
  static String deleteApplication = "SettingButtonForApplication.deleteApplication".tr();
  static String closeApplication = "SettingButtonForApplication.closeApplication".tr();
}
