import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart' as EL;
import 'package:scholarship/blocs/index.dart';
import 'package:scholarship/models/index.dart';
import 'package:scholarship/pages/App/index.dart';
import 'package:scholarship/pages/Scholarship/Styles/index.dart';

import 'index.dart';

class SettingButtonForApplication extends StatelessWidget {
  SettingButtonForApplication({
    @required this.initScholarshipApplicationModel,
    @required this.scholarshipModelID,
    @required this.child,
  });
  ScholarshipColors _scholarshipColors;
  SettingButtonForApplicationStyles _settingButtonForApplicationStyles;
  Widget child;
  ScholarshipApplicationModel initScholarshipApplicationModel;
  String scholarshipModelID;

  @override
  Widget build(BuildContext context) {
    _scholarshipColors = ScholarshipColors();
    _settingButtonForApplicationStyles = SettingButtonForApplicationStyles(context);
    return Container(
      child: PopupMenuButton(
        color: Color(0xFF181717),
        padding: EdgeInsets.zero,
        offset: Offset(0, 20),
        onSelected: (int index) async {
          switch (index) {
            case 1:
              break;
            case 2:
              Navigator.of(context).pushNamed(
                AppRoutes.updateApplication,
                arguments: {
                  "initScholarshipApplicationModel": initScholarshipApplicationModel,
                  "scholarshipModelID": scholarshipModelID,
                },
              );
              break;
            case 3:
              break;
            default:
          }
        },
        child: child,
        itemBuilder: (BuildContext context) => [
          PopupMenuItem(
            height: 30,
            value: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  SettingButtonForApplicationString.actionsItem,
                  style: TextStyle(fontSize: 15, color: Colors.white),
                ),
                Icon(Icons.cancel, size: 15, color: Colors.white)
              ],
            ),
          ),
          PopupMenuItem(
            height: 30,
            value: 2,
            child: Text(
              SettingButtonForApplicationString.editApplication,
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
          ),
          PopupMenuItem(
            height: 30,
            value: 3,
            child: Text(
              SettingButtonForApplicationString.messageApplication,
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
          ),
          PopupMenuItem(
            height: 30,
            value: 4,
            child: Text(
              SettingButtonForApplicationString.deleteApplication,
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
          ),
          PopupMenuItem(
            height: 30,
            value: 5,
            child: Text(
              SettingButtonForApplicationString.closeApplication,
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}
