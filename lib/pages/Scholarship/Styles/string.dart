import 'package:easy_localization/easy_localization.dart' as EL;

class ScholarshipString {
  static String textValidateString = "ValidateString.textWithLength".tr();
  static String normalValidateString = "ValidateString.normalText".tr();
  static String emailValidateString = "ValidateString.email".tr();
  static List<Map<String, String>> disciplineList = [
    {"value": "discipline 1", "text": "discipline 1"},
    {"value": "discipline 2", "text": "discipline 2"},
    {"value": "discipline 3", "text": "discipline 3"},
    {"value": "discipline 4", "text": "discipline 4"},
    {"value": "discipline 5", "text": "discipline 5"},
  ];
}
