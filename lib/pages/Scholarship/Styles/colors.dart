import 'package:flutter/material.dart';

class ScholarshipColors {
  Color secondaryColor;
  Color formValidatedColor;
  Color cancelButtonColor;

  Color backgroundColor;
  Color textColor;
  Color hintTextColor;
  Color panelBackgroundColor;
  Brightness brightness;
  Color descriptioinColor;

  ScholarshipColors() {
    secondaryColor = Color(0xFFB58970);
    formValidatedColor = Color(0xFF2CAB00);
    cancelButtonColor = Color(0xFFC4C4C4);
  }
}

class ScholarshipDarkModeColors extends ScholarshipColors {
  ScholarshipDarkModeColors() {
    backgroundColor = Colors.black;
    textColor = Colors.white;
    hintTextColor = Colors.white24;
    panelBackgroundColor = Color(0xFF232323);
    brightness = Brightness.light;
    descriptioinColor = Color.fromRGBO(158, 185, 180, 0.87);
  }
}

class ScholarshipLightModeColors extends ScholarshipColors {
  ScholarshipLightModeColors() {
    backgroundColor = Colors.white;
    textColor = Colors.black;
    hintTextColor = Color(0x99232323);
    panelBackgroundColor = Colors.white;
    brightness = Brightness.dark;
    descriptioinColor = Color(0xFF9EB9B4);
  }
}
