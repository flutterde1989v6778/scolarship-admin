import 'package:flutter/material.dart';
import 'package:scholarship/models/index.dart';
import 'package:scholarship/providers/index.dart';

import 'index.dart';

class ScholarshipRepository {
  ScholarshipProvider scholarshipProvider = ScholarshipProvider();

  Future<ScholarshipModel> addScholarship({@required ScholarshipModel scholarshipModel}) async {
    return await scholarshipProvider.addScholarship(scholarshipModel: scholarshipModel);
  }

  Future<bool> updateScholarship({@required ScholarshipModel scholarshipModel}) async {
    return await scholarshipProvider.updateScholarship(scholarshipModel: scholarshipModel);
  }

  Future<dynamic> deleteScholarship({@required ScholarshipModel scholarshipModel}) async {
    return await scholarshipProvider.deleteScholarship(scholarshipModel: scholarshipModel);
  }

  Future<List<ScholarshipModel>> getScholarshipList({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) async {
    try {
      List<ScholarshipModel> list = await scholarshipProvider.getScholarshipList(wheres: wheres, orderby: orderby, limit: limit);
      return list;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Stream<List<ScholarshipModel>> getScholarshipListStream({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) {
    return scholarshipProvider.getScholarshipListStream(wheres: wheres, orderby: orderby, limit: limit);
  }

  Future<bool> isScholarshipExist({@required String id}) async {
    return await scholarshipProvider.isScholarshipExist(id: id);
  }

  Future<ScholarshipModel> getScholarshipByID({@required String id}) async {
    return await scholarshipProvider.getScholarshipByID(id: id);
  }

  Stream<ScholarshipModel> getScholarshipStreamByID({@required String id}) {
    return scholarshipProvider.getScholarshipStreamByID(id: id);
  }
}
