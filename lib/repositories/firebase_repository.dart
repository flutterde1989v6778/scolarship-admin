import 'package:flutter/material.dart';
import 'package:scholarship/providers/index.dart';

import './index.dart';

class FirebaseRepository extends BaseRepository {
  FirebaseProvider firebaseProvider = FirebaseProvider();

  Future<dynamic> addDocument({@required String path, @required Map<String, dynamic> data}) => firebaseProvider.addDocument(path: path, data: data);

  Future<dynamic> updateDocument({@required String path, @required String id, @required Map<String, dynamic> data}) =>
      firebaseProvider.updateDocument(path: path, id: id, data: data);

  Future<dynamic> deleteDocument({@required String path, @required String id}) => firebaseProvider.deleteDocument(path: path, id: id);

  Stream<List<Map<String, dynamic>>> getDocumentsStream({
    @required String path,
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) =>
      firebaseProvider.getDocumentsStream(path: path, wheres: wheres, orderby: orderby, limit: limit);

  Future getDocumentData({
    @required String path,
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) =>
      firebaseProvider.getDocumentData(path: path, wheres: wheres, orderby: orderby, limit: limit);

  @override
  void dispose() {
    firebaseProvider.dispose();
  }

  Stream<List<Stream<List<Map<String, dynamic>>>>> getDocumentsStreamWithChildCollection({
    @required String parentCollectionName,
    @required String childCollectionName,
    List<Map<String, dynamic>> parentWheres,
    List<Map<String, dynamic>> parentOrderby,
    int parentLimit,
    List<Map<String, dynamic>> childWheres,
    List<Map<String, dynamic>> childOrderby,
    int childLimit,
  }) =>
      firebaseProvider.getDocumentsStreamWithChildCollection(
        parentCollectionName: parentCollectionName,
        childCollectionName: childCollectionName,
        parentWheres: parentWheres,
        parentOrderby: parentOrderby,
        parentLimit: parentLimit,
        childWheres: childWheres,
        childOrderby: childOrderby,
        childLimit: childLimit,
      );

  Future getDocumentDataWithChilCollection({
    @required String parentCollectionName,
    @required String childCollectionName,
    List<Map<String, dynamic>> parentWheres,
    List<Map<String, dynamic>> parentOrderby,
    int parentLimit,
    List<Map<String, dynamic>> childWheres,
    List<Map<String, dynamic>> childOrderby,
    int childLimit,
  }) =>
      firebaseProvider.getDocumentDataWithChilCollection(
        parentCollectionName: parentCollectionName,
        childCollectionName: childCollectionName,
        parentWheres: parentWheres,
        parentOrderby: parentOrderby,
        parentLimit: parentLimit,
        childWheres: childWheres,
        childOrderby: childOrderby,
        childLimit: childLimit,
      );
}
