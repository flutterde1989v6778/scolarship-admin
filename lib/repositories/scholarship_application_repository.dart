import 'package:flutter/material.dart';
import 'package:scholarship/models/index.dart';
import 'package:scholarship/providers/index.dart';

import 'index.dart';

class ScholarshipApplicationRepository {
  ScholarshipApplicationProvider scholarshipApplicationProvider = ScholarshipApplicationProvider();

  Future<ScholarshipApplicationModel> addScholarshipApplication({
    @required ScholarshipApplicationModel scholarshipApplicationModel,
  }) async {
    return await scholarshipApplicationProvider.addScholarshipApplication(
      scholarshipApplicationModel: scholarshipApplicationModel,
    );
  }

  Future<bool> updateScholarshipApplication({
    @required ScholarshipApplicationModel scholarshipApplicationModel,
  }) async {
    return await scholarshipApplicationProvider.updateScholarshipApplication(
      scholarshipApplicationModel: scholarshipApplicationModel,
    );
  }

  Future<dynamic> deleteScholarshipApplication({
    @required ScholarshipApplicationModel scholarshipApplicationModel,
  }) async {
    return await scholarshipApplicationProvider.deleteScholarshipApplication(
      scholarshipApplicationModel: scholarshipApplicationModel,
    );
  }

  Future<List<ScholarshipApplicationModel>> getScholarshipApplicationList({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) async {
    try {
      List<ScholarshipApplicationModel> list = await scholarshipApplicationProvider.getScholarshipApplicationList(
        wheres: wheres,
        orderby: orderby,
        limit: limit,
      );
      return list;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Stream<List<ScholarshipApplicationModel>> getScholarshipApplicationListStream({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) {
    return scholarshipApplicationProvider.getScholarshipApplicationListStream(
      wheres: wheres,
      orderby: orderby,
      limit: limit,
    );
  }

  Future<bool> isScholarshipExist({@required String id}) async {
    return await scholarshipApplicationProvider.isScholarshipApplicationExist(id: id);
  }

  Future<ScholarshipApplicationModel> getScholarshipApplicationByID({@required String id}) async {
    return await scholarshipApplicationProvider.getScholarshipApplicationByID(id: id);
  }

  Stream<ScholarshipApplicationModel> getScholarshipApplicationStreamByID({@required String id}) {
    return scholarshipApplicationProvider.getScholarshipApplicationStreamByID(id: id);
  }

  Stream<List<Stream<Map<String, Object>>>> getApplicationListWithScholarshipStream({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) {
    return scholarshipApplicationProvider.getApplicationListWithScholarshipStream(
      wheres: wheres,
      orderby: orderby,
      limit: limit,
    );
  }
}
