import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:scholarship/models/index.dart';
import 'package:universal_html/html.dart' as html;

@immutable
class ScholarshipLoginState {
  final int loginState;

  ScholarshipLoginState({
    @required this.loginState, //// 0: init, 1: doing login, -1: fail, 2: success
  });

  factory ScholarshipLoginState.init() {
    return ScholarshipLoginState(
      loginState: 0,
    );
  }

  ScholarshipLoginState update({
    int loginState,
  }) {
    return copyWith(
      loginState: loginState,
    );
  }

  ScholarshipLoginState copyWith({
    int loginState,
  }) {
    return ScholarshipLoginState(
      loginState: loginState ?? this.loginState,
    );
  }

  @override
  String toString() {
    return '''ScholarshipLoginState {
      loginState: $loginState,
    }''';
  }
}
