import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:universal_html/html.dart' as html;

import 'package:scholarship/models/index.dart';
import 'package:scholarship/models/scholarship_application_model.dart';
import 'package:scholarship/providers/index.dart';
import 'package:scholarship/repositories/index.dart';

import './index.dart';

class ScholarshipLoginBloc extends Bloc<ScholarshipLoginEvent, ScholarshipLoginState> {
  @override
  ScholarshipLoginState get initialState => ScholarshipLoginState.init();

  @override
  Stream<Transition<ScholarshipLoginEvent, ScholarshipLoginState>> transformEvents(events, transitionFn) {
    return super.transformEvents(events, transitionFn);
  }

  @override
  Stream<ScholarshipLoginState> mapEventToState(ScholarshipLoginEvent event) async* {
    // if (event is DetailBodyIsOpenedEvent) {
    //   yield* _mapDetailBodyIsOpenedToState(event.id, event.isOpened);
    // }
  }

  // Stream<ScholarshipLoginState> _mapReturnScholarshipLoginPageEventToState() async* {
  //   yield state.update(submissionSuccessViewState: false);
  // }

}
