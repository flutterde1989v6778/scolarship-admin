import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:universal_html/html.dart' as html;

import 'package:scholarship/models/scholarship_application_model.dart';

abstract class ScholarshipLoginEvent extends Equatable {
  const ScholarshipLoginEvent();

  @override
  List<Object> get props => [];
}

class LoadingScholarshipLoginListEvent extends ScholarshipLoginEvent {
  final int loadingStateForScholarshipLoginList;

  const LoadingScholarshipLoginListEvent({@required this.loadingStateForScholarshipLoginList});

  @override
  List<Object> get props => [loadingStateForScholarshipLoginList];

  @override
  String toString() => 'DetailBodyIsOpenedEvent { isOpened :${[loadingStateForScholarshipLoginList]} }';
}
