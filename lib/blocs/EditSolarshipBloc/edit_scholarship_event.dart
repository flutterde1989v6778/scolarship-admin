import 'dart:typed_data';

import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:scholarship/models/index.dart';
import 'package:universal_html/html.dart' as html;

import 'package:scholarship/models/scholarship_application_model.dart';

abstract class EditScholarshipEvent extends Equatable {
  const EditScholarshipEvent();

  @override
  List<Object> get props => [];
}

class SaveStateForScholarshipEvent extends EditScholarshipEvent {
  final int saveState;

  const SaveStateForScholarshipEvent({@required this.saveState});

  @override
  List<Object> get props => [saveState];

  @override
  String toString() => 'SaveStateEvent { isOpened :${[saveState]} }';
}

class AddScholarshipImageDataEvent extends EditScholarshipEvent {
  final html.File file;
  final Uint8List fileByteData;

  const AddScholarshipImageDataEvent({@required this.file, @required this.fileByteData});

  @override
  List<Object> get props => [file, fileByteData];

  @override
  String toString() => 'AddScholarshipImageDataEvent { file :${[file.name]} }';
}

class AddScholarshipEvent extends EditScholarshipEvent {
  final ScholarshipModel newScholarshipModel;
  final html.File file;

  const AddScholarshipEvent({@required this.newScholarshipModel, @required this.file});

  @override
  List<Object> get props => [newScholarshipModel, file];

  @override
  String toString() => 'DetailBodyIsOpenedEvent { newScholarshipModel :${[newScholarshipModel]} }';
}

class UpdateScholarshipEvent extends EditScholarshipEvent {
  final ScholarshipModel updateScholarshipModel;
  final html.File file;

  const UpdateScholarshipEvent({@required this.updateScholarshipModel, @required this.file});

  @override
  List<Object> get props => [updateScholarshipModel, file];

  @override
  String toString() => 'DetailBodyIsOpenedEvent { newScholarshipModel :${[updateScholarshipModel]} }';
}
