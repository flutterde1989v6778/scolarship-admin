import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:scholarship/models/index.dart';
import 'package:universal_html/html.dart' as html;

@immutable
class EditScholarshipState {
  final int saveState;
  final ScholarshipModel scholarshipModel;
  final html.File scholarshipImageFile;
  final Uint8List scholarshipmageFileByteData;

  EditScholarshipState({
    @required this.saveState, //// 0: init, 1: saving, -1: fail, 2: success
    @required this.scholarshipModel,
    @required this.scholarshipImageFile,
    @required this.scholarshipmageFileByteData,
  });

  factory EditScholarshipState.init({@required ScholarshipModel initScholarshipModel}) {
    return EditScholarshipState(
      saveState: 0,
      scholarshipModel: initScholarshipModel,
      scholarshipImageFile: null,
      scholarshipmageFileByteData: null,
    );
  }

  EditScholarshipState update({
    int saveState,
    ScholarshipModel scholarshipModel,
    html.File scholarshipImageFile,
    Uint8List scholarshipmageFileByteData,
  }) {
    return copyWith(
      saveState: saveState,
      scholarshipModel: scholarshipModel,
      scholarshipImageFile: scholarshipImageFile,
      scholarshipmageFileByteData: scholarshipmageFileByteData,
    );
  }

  EditScholarshipState copyWith({
    int saveState,
    ScholarshipModel scholarshipModel,
    html.File scholarshipImageFile,
    Uint8List scholarshipmageFileByteData,
  }) {
    return EditScholarshipState(
      saveState: saveState ?? this.saveState,
      scholarshipModel: scholarshipModel ?? this.scholarshipModel,
      scholarshipImageFile: scholarshipImageFile ?? this.scholarshipImageFile,
      scholarshipmageFileByteData: scholarshipmageFileByteData ?? this.scholarshipmageFileByteData,
    );
  }

  @override
  String toString() {
    return '''EditScholarshipState {
      saveState: $saveState,
    }''';
  }
}
