import 'dart:async';
import 'dart:typed_data';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:universal_html/html.dart' as html;

import 'package:scholarship/models/index.dart';
import 'package:scholarship/models/scholarship_application_model.dart';
import 'package:scholarship/providers/index.dart';
import 'package:scholarship/repositories/index.dart';

import './index.dart';

class EditScholarshipBloc extends Bloc<EditScholarshipEvent, EditScholarshipState> {
  EditScholarshipBloc({@required this.initScholarshipModel});
  ScholarshipModel initScholarshipModel;
  ScholarshipRepository _scholarshipRepository = ScholarshipRepository();

  @override
  EditScholarshipState get initialState => EditScholarshipState.init(initScholarshipModel: initScholarshipModel);

  @override
  Stream<Transition<EditScholarshipEvent, EditScholarshipState>> transformEvents(events, transitionFn) {
    return super.transformEvents(events, transitionFn);
  }

  @override
  Stream<EditScholarshipState> mapEventToState(EditScholarshipEvent event) async* {
    if (event is SaveStateForScholarshipEvent) {
      yield* _mapSaveStateEventToState(event.saveState);
    } else if (event is AddScholarshipImageDataEvent) {
      yield* _mapAddScholarshipImageDataEventToState(event.file, event.fileByteData);
    } else if (event is AddScholarshipEvent) {
      yield* _mapAddScholarshipEventToState(event.file, event.newScholarshipModel);
    } else if (event is UpdateScholarshipEvent) {
      yield* _mapUpdateScholarshipEventToState(event.file, event.updateScholarshipModel);
    }
  }

  Stream<EditScholarshipState> _mapSaveStateEventToState(int saveState) async* {
    yield state.update(saveState: saveState);
  }

  Stream<EditScholarshipState> _mapAddScholarshipImageDataEventToState(html.File file, Uint8List fileByteData) async* {
    yield state.update(scholarshipImageFile: file, scholarshipmageFileByteData: fileByteData);
  }

  Stream<EditScholarshipState> _mapAddScholarshipEventToState(html.File file, ScholarshipModel scholarshipModel) async* {
    try {
      scholarshipModel.imageUrl = await StorageProvider.uploadHtmlFileObject(path: "ScholarshipImage/", file: file);
      scholarshipModel.ts = DateTime.now().millisecondsSinceEpoch;
      // scholarshipModel.userID = "user ID";   //// you can input login user id
      ScholarshipModel newScholarshipModel = await _scholarshipRepository.addScholarship(scholarshipModel: scholarshipModel);
      if (newScholarshipModel != null) {
        yield state.update(scholarshipModel: newScholarshipModel, scholarshipImageFile: null, scholarshipmageFileByteData: null, saveState: 2);
      } else {
        yield state.update(
          scholarshipModel: scholarshipModel,
          saveState: -1,
        );
      }
    } catch (e) {
      yield state.update(
        scholarshipModel: scholarshipModel,
        saveState: -1,
      );
    }
  }

  Stream<EditScholarshipState> _mapUpdateScholarshipEventToState(html.File file, ScholarshipModel scholarshipModel) async* {
    try {
      if (file != null) {
        scholarshipModel.imageUrl = await StorageProvider.uploadHtmlFileObject(path: "ScholarshipImage/", file: file);
      }
      scholarshipModel.ts = DateTime.now().millisecondsSinceEpoch;
      var result = await _scholarshipRepository.updateScholarship(scholarshipModel: scholarshipModel);
      if (result) {
        yield state.update(scholarshipModel: scholarshipModel, scholarshipImageFile: null, scholarshipmageFileByteData: null, saveState: 2);
      } else {
        yield state.update(
          scholarshipModel: scholarshipModel,
          saveState: -1,
        );
      }
    } catch (e) {
      yield state.update(
        scholarshipModel: scholarshipModel,
        saveState: -1,
      );
    }
  }
}
