import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:universal_html/html.dart' as html;

import 'package:scholarship/models/index.dart';
import 'package:scholarship/models/scholarship_application_model.dart';
import 'package:scholarship/providers/index.dart';
import 'package:scholarship/repositories/index.dart';

import './index.dart';

class ManageScholarshipsBloc extends Bloc<ManageScholarshipsEvent, ManageScholarshipsState> {
  ScholarshipRepository _scholarshipRepository = ScholarshipRepository();
  @override
  ManageScholarshipsState get initialState => ManageScholarshipsState.init();

  @override
  Stream<Transition<ManageScholarshipsEvent, ManageScholarshipsState>> transformEvents(events, transitionFn) {
    return super.transformEvents(events, transitionFn);
  }

  @override
  Stream<ManageScholarshipsState> mapEventToState(ManageScholarshipsEvent event) async* {
    if (event is LoadingStateEvent) {
      yield* _mapLoadingStateEventToState(event.loadingState);
    } else if (event is GetScholarshipListStreamEvent) {
      yield* _mapGetScholarshipListStreamEventToState();
    }
  }

  Stream<ManageScholarshipsState> _mapLoadingStateEventToState(int loadingState) async* {
    yield state.update(loadingState: loadingState);
  }

  Stream<ManageScholarshipsState> _mapGetScholarshipListStreamEventToState() async* {
    yield state.update(loadingState: 1);
    try {
      Stream<List<ScholarshipModel>> scholarshipListStream = _scholarshipRepository.getScholarshipListStream();
      if (scholarshipListStream != null) {
        yield state.update(scholarshipListStream: scholarshipListStream, loadingState: 2);
      } else {
        yield state.update(scholarshipListStream: null, loadingState: -1);
      }
    } catch (e) {
      yield state.update(scholarshipListStream: null, loadingState: -1);
    }
  }
}
