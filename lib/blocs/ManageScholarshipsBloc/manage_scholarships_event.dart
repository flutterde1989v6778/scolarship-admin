import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:universal_html/html.dart' as html;

import 'package:scholarship/models/scholarship_application_model.dart';

abstract class ManageScholarshipsEvent extends Equatable {
  const ManageScholarshipsEvent();

  @override
  List<Object> get props => [];
}

class LoadingStateEvent extends ManageScholarshipsEvent {
  final int loadingState;

  const LoadingStateEvent({@required this.loadingState});

  @override
  List<Object> get props => [loadingState];

  @override
  String toString() => 'LoadingStateEvent { isOpened :${[loadingState]} }';
}

class GetScholarshipListStreamEvent extends ManageScholarshipsEvent {
  const GetScholarshipListStreamEvent();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetScholarshipListStreamEvent { isOpened :${[]} }';
}
