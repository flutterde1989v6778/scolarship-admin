import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:scholarship/models/index.dart';
import 'package:universal_html/html.dart' as html;

@immutable
class ManageScholarshipsState {
  final int loadingState;
  final Stream<List<ScholarshipModel>> scholarshipListStream;

  ManageScholarshipsState({
    @required this.loadingState,
    @required this.scholarshipListStream,
  });

  factory ManageScholarshipsState.init() {
    return ManageScholarshipsState(
      loadingState: 0,
      scholarshipListStream: null,
    );
  }

  ManageScholarshipsState update({
    int loadingState,
    Stream<List<ScholarshipModel>> scholarshipListStream,
  }) {
    return copyWith(
      loadingState: loadingState,
      scholarshipListStream: scholarshipListStream,
    );
  }

  ManageScholarshipsState copyWith({
    int loadingState,
    Stream<List<ScholarshipModel>> scholarshipListStream,
  }) {
    return ManageScholarshipsState(
      loadingState: loadingState ?? this.loadingState,
      scholarshipListStream: scholarshipListStream ?? this.scholarshipListStream,
    );
  }

  @override
  String toString() {
    return '''ManageScholarshipsState {
      loadingState: $loadingState,
      scholarshipListStream: $scholarshipListStream,
    }''';
  }
}
