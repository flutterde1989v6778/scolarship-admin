import 'dart:typed_data';

import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:scholarship/models/index.dart';
import 'package:universal_html/html.dart' as html;

import 'package:scholarship/models/scholarship_application_model.dart';

abstract class EditApplicationEvent extends Equatable {
  const EditApplicationEvent();

  @override
  List<Object> get props => [];
}

class SaveStateForApplicationEvent extends EditApplicationEvent {
  final int saveState;

  const SaveStateForApplicationEvent({@required this.saveState});

  @override
  List<Object> get props => [saveState];

  @override
  String toString() => 'SaveStateEvent { isOpened :${[saveState]} }';
}

class ReadApplicationEvent extends EditApplicationEvent {
  const ReadApplicationEvent();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'DetailBodyIsOpenedEvent { newScholarshipModel :${[]} }';
}

class UpdateApplicationEvent extends EditApplicationEvent {
  final ScholarshipApplicationModel updateScholarshipApplicationModel;

  const UpdateApplicationEvent({@required this.updateScholarshipApplicationModel});

  @override
  List<Object> get props => [updateScholarshipApplicationModel];

  @override
  String toString() => 'DetailBodyIsOpenedEvent { newScholarshipModel :${[updateScholarshipApplicationModel]} }';
}
