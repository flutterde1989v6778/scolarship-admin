import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:scholarship/models/index.dart';
import 'package:universal_html/html.dart' as html;

@immutable
class EditApplicationState {
  final int saveState;
  final ScholarshipApplicationModel scholarshipApplicationModel;
  final String scholarshipModelID;

  EditApplicationState({
    @required this.saveState, //// 0: init, 1: saving, -1: fail, 2: success
    @required this.scholarshipApplicationModel,
    @required this.scholarshipModelID,
  });

  factory EditApplicationState.init({
    @required ScholarshipApplicationModel initScholarshipApplicationModel,
    @required String scholarshipModelID,
  }) {
    return EditApplicationState(
      saveState: 0,
      scholarshipApplicationModel: initScholarshipApplicationModel,
      scholarshipModelID: scholarshipModelID,
    );
  }

  EditApplicationState update({
    int saveState,
    ScholarshipApplicationModel scholarshipApplicationModel,
    String scholarshipModelID,
  }) {
    return copyWith(
      saveState: saveState,
      scholarshipApplicationModel: scholarshipApplicationModel,
      scholarshipModelID: scholarshipModelID,
    );
  }

  EditApplicationState copyWith({
    int saveState,
    ScholarshipApplicationModel scholarshipApplicationModel,
    String scholarshipModelID,
  }) {
    return EditApplicationState(
      saveState: saveState ?? this.saveState,
      scholarshipApplicationModel: scholarshipApplicationModel ?? this.scholarshipApplicationModel,
      scholarshipModelID: scholarshipModelID ?? this.scholarshipModelID,
    );
  }

  @override
  String toString() {
    return '''EditApplicationState {
      saveState: $saveState,
    }''';
  }
}
