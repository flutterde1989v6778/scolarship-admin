import 'dart:async';
import 'dart:typed_data';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:universal_html/html.dart' as html;

import 'package:scholarship/models/index.dart';
import 'package:scholarship/models/scholarship_application_model.dart';
import 'package:scholarship/providers/index.dart';
import 'package:scholarship/repositories/index.dart';

import './index.dart';

class EditApplicationBloc extends Bloc<EditApplicationEvent, EditApplicationState> {
  EditApplicationBloc({@required this.initScholarshipApplicationModel, @required this.scholarshipModelID});
  ScholarshipApplicationModel initScholarshipApplicationModel;
  String scholarshipModelID;

  ScholarshipApplicationRepository _scholarshipApplicationRepository = ScholarshipApplicationRepository();

  @override
  EditApplicationState get initialState =>
      EditApplicationState.init(initScholarshipApplicationModel: initScholarshipApplicationModel, scholarshipModelID: scholarshipModelID);

  @override
  Stream<Transition<EditApplicationEvent, EditApplicationState>> transformEvents(events, transitionFn) {
    return super.transformEvents(events, transitionFn);
  }

  @override
  Stream<EditApplicationState> mapEventToState(EditApplicationEvent event) async* {
    if (event is SaveStateForApplicationEvent) {
      yield* _mapSaveStateEventToState(event.saveState);
    } else if (event is UpdateApplicationEvent) {
      yield* _mapUpdateApplicationEventToState(event.updateScholarshipApplicationModel);
    } else if (event is ReadApplicationEvent) {
      yield* _mapReadApplicationEventToState();
    }
  }

  Stream<EditApplicationState> _mapSaveStateEventToState(int saveState) async* {
    yield state.update(saveState: saveState);
  }

  Stream<EditApplicationState> _mapUpdateApplicationEventToState(ScholarshipApplicationModel scholarshipApplicationModel) async* {
    try {
      scholarshipApplicationModel.isReviewed = true;
      scholarshipApplicationModel.isRead = true;
      var result = await _scholarshipApplicationRepository.updateScholarshipApplication(
        scholarshipApplicationModel: scholarshipApplicationModel,
      );
      if (result) {
        yield state.update(scholarshipApplicationModel: scholarshipApplicationModel, saveState: 2);
      } else {
        scholarshipApplicationModel.isReviewed = false;
        scholarshipApplicationModel.isRead = true;
        yield state.update(
          scholarshipApplicationModel: scholarshipApplicationModel,
          saveState: -1,
        );
      }
    } catch (e) {
      print(e);
      scholarshipApplicationModel.isReviewed = false;
      scholarshipApplicationModel.isRead = true;
      yield state.update(
        scholarshipApplicationModel: scholarshipApplicationModel,
        saveState: -1,
      );
    }
  }

  Stream<EditApplicationState> _mapReadApplicationEventToState() async* {
    ScholarshipApplicationModel scholarshipApplicationModel = state.scholarshipApplicationModel;
    try {
      scholarshipApplicationModel.isRead = true;
      var result = await _scholarshipApplicationRepository.updateScholarshipApplication(
        scholarshipApplicationModel: scholarshipApplicationModel,
      );
      // if (result) {
      //   yield state.update(scholarshipApplicationModel: scholarshipApplicationModel);
      // } else {
      //   scholarshipApplicationModel.isRead = false;
      //   yield state.update(
      //     scholarshipApplicationModel: scholarshipApplicationModel,
      //   );
      // }
    } catch (e) {
      // print(e);
      // scholarshipApplicationModel.isRead = false;
      // yield state.update(
      //   scholarshipApplicationModel: scholarshipApplicationModel,
      // );
    }
  }
}
