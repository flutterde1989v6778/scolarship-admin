import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:scholarship/models/index.dart';
import 'package:universal_html/html.dart' as html;

@immutable
class ManageApplicationsState {
  final int selectedFilter;
  final int loadingStateForApplictionListStream;
  final Stream<List<ScholarshipApplicationModel>> applictionListStream;
  final Map<String, dynamic> orderby;

  ManageApplicationsState({
    @required this.selectedFilter,
    @required this.loadingStateForApplictionListStream,
    @required this.applictionListStream,
    @required this.orderby,
  });

  factory ManageApplicationsState.init() {
    return ManageApplicationsState(
      selectedFilter: 0,
      loadingStateForApplictionListStream: 0,
      applictionListStream: null,
      orderby: {
        "applicant": false,
        "submission": false,
        "score": false,
        "available": "applicant",
      },
    );
  }

  ManageApplicationsState update({
    int selectedFilter,
    int loadingStateForApplictionListStream,
    Stream<List<ScholarshipApplicationModel>> applictionListStream,
    Map<String, dynamic> orderby,
  }) {
    return copyWith(
      selectedFilter: selectedFilter,
      loadingStateForApplictionListStream: loadingStateForApplictionListStream,
      applictionListStream: applictionListStream,
      orderby: orderby,
    );
  }

  ManageApplicationsState copyWith({
    int selectedFilter,
    int loadingStateForApplictionListStream,
    Stream<List<ScholarshipApplicationModel>> applictionListStream,
    Map<String, dynamic> orderby,
  }) {
    return ManageApplicationsState(
      selectedFilter: selectedFilter ?? this.selectedFilter,
      loadingStateForApplictionListStream: loadingStateForApplictionListStream ?? this.loadingStateForApplictionListStream,
      applictionListStream: applictionListStream ?? this.applictionListStream,
      orderby: orderby ?? this.orderby,
    );
  }

  @override
  String toString() {
    return '''ManageApplicationsState {
      selectedFilter: $selectedFilter,
      loadingStateForApplictionListStream: $loadingStateForApplictionListStream,
      applictionListStream: $applictionListStream,
    }''';
  }
}
