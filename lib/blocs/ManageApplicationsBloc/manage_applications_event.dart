import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:universal_html/html.dart' as html;

import 'package:scholarship/models/scholarship_application_model.dart';

abstract class ManageApplicationsEvent extends Equatable {
  const ManageApplicationsEvent();

  @override
  List<Object> get props => [];
}

class SelectFilterEvent extends ManageApplicationsEvent {
  final int selectedFilter;

  const SelectFilterEvent({@required this.selectedFilter});

  @override
  List<Object> get props => [selectedFilter];

  @override
  String toString() => 'DetailBodyIsOpenedEvent { isOpened :${[selectedFilter]} }';
}

class GetApplicationListStreamEvent extends ManageApplicationsEvent {
  const GetApplicationListStreamEvent();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'DetailBodyIsOpenedEvent { isOpened :${[]} }';
}

class ChangeFilterEvent extends ManageApplicationsEvent {
  final int filter;
  const ChangeFilterEvent({@required this.filter});

  @override
  List<Object> get props => [];

  @override
  String toString() => 'DetailBodyIsOpenedEvent { isOpened :${[]} }';
}

class ChangeOrderByEvent extends ManageApplicationsEvent {
  final Map<String, dynamic> orderby;
  const ChangeOrderByEvent({@required this.orderby});

  @override
  List<Object> get props => [];

  @override
  String toString() => 'DetailBodyIsOpenedEvent { isOpened :${[]} }';
}
