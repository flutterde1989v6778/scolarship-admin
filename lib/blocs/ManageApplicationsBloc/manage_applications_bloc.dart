import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:universal_html/html.dart' as html;

import 'package:scholarship/models/index.dart';
import 'package:scholarship/models/scholarship_application_model.dart';
import 'package:scholarship/providers/index.dart';
import 'package:scholarship/repositories/index.dart';

import './index.dart';

class ManageApplicationsBloc extends Bloc<ManageApplicationsEvent, ManageApplicationsState> {
  ScholarshipApplicationRepository _scholarshipApplicationRepository = ScholarshipApplicationRepository();
  @override
  ManageApplicationsState get initialState => ManageApplicationsState.init();

  @override
  Stream<Transition<ManageApplicationsEvent, ManageApplicationsState>> transformEvents(events, transitionFn) {
    return super.transformEvents(events, transitionFn);
  }

  @override
  Stream<ManageApplicationsState> mapEventToState(ManageApplicationsEvent event) async* {
    if (event is SelectFilterEvent) {
      yield* _mapSelectFilterEventToState(event.selectedFilter);
    } else if (event is GetApplicationListStreamEvent) {
      yield* _mapGetApplicationListStreamEventToState();
    } else if (event is ChangeOrderByEvent) {
      yield* _mapChangeOrderByEventEventToState(event.orderby);
    }
  }

  Stream<ManageApplicationsState> _mapChangeOrderByEventEventToState(Map<String, dynamic> orderby) async* {
    yield state.update(orderby: orderby);
  }

  Stream<ManageApplicationsState> _mapSelectFilterEventToState(int selectedFilter) async* {
    yield state.update(selectedFilter: selectedFilter);
  }

  Stream<ManageApplicationsState> _mapGetApplicationListStreamEventToState({Map<String, dynamic> orderby}) async* {
    yield state.update(loadingStateForApplictionListStream: 1);

    try {
      Stream<List<ScholarshipApplicationModel>> applictionListStream = _scholarshipApplicationRepository.getScholarshipApplicationListStream(
          // wheres: [{"key": "userID", "val": "User ID"}],  /// you can input login user id
          );
      if (applictionListStream != null) {
        yield state.update(applictionListStream: applictionListStream, loadingStateForApplictionListStream: 2, orderby: orderby);
      } else {
        yield state.update(applictionListStream: null, loadingStateForApplictionListStream: -1, orderby: orderby);
      }
    } catch (e) {
      print(e);
      yield state.update(applictionListStream: null, loadingStateForApplictionListStream: -1, orderby: orderby);
    }
  }
}
