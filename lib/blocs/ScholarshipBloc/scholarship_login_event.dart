import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:universal_html/html.dart' as html;

import 'package:scholarship/models/scholarship_application_model.dart';

abstract class ScholarshipEvent extends Equatable {
  const ScholarshipEvent();

  @override
  List<Object> get props => [];
}

class LoadingScholarshipListEvent extends ScholarshipEvent {
  final int loadingStateForScholarshipList;

  const LoadingScholarshipListEvent({@required this.loadingStateForScholarshipList});

  @override
  List<Object> get props => [loadingStateForScholarshipList];

  @override
  String toString() => 'DetailBodyIsOpenedEvent { isOpened :${[loadingStateForScholarshipList]} }';
}

class GetScholarshipListStreamEvent extends ScholarshipEvent {
  const GetScholarshipListStreamEvent();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'DetailBodyIsOpenedEvent { isOpened :${[]} }';
}

class DetailBodyIsOpenedEvent extends ScholarshipEvent {
  final String id;
  final bool isOpened;

  const DetailBodyIsOpenedEvent({@required this.id, @required this.isOpened});

  @override
  List<Object> get props => [id, isOpened];

  @override
  String toString() => 'DetailBodyIsOpenedEvent { isOpened :${[id, isOpened].toList()} }';
}

class InitializeStateEvent extends ScholarshipEvent {
  const InitializeStateEvent();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'InitializeStateEvent { isOpened  }';
}

class SelectDetailTabEvent extends ScholarshipEvent {
  final String id;
  final int tapIndex;

  const SelectDetailTabEvent({@required this.id, @required this.tapIndex});

  @override
  List<Object> get props => [id, tapIndex];

  @override
  String toString() => 'SelectDetailTabEvent { tapIndex :${[id, tapIndex].toString()} }';
}

class SelectEligibilityEvent extends ScholarshipEvent {
  final String id;
  final Map<int, bool> isOpened;

  const SelectEligibilityEvent({@required this.id, @required this.isOpened});

  @override
  List<Object> get props => [id, isOpened];

  @override
  String toString() => 'SelectEligibilityEvent { isOpened :${[id, isOpened].toString()} }';
}

class UpdatingApplicationDataEvent extends ScholarshipEvent {
  final Map<String, dynamic> scholarshipApplicationData;
  final String scholarshipID;

  const UpdatingApplicationDataEvent({@required this.scholarshipID, @required this.scholarshipApplicationData});

  @override
  List<Object> get props => [scholarshipApplicationData];

  @override
  String toString() => 'FormValidateForSubmissionEvent { isOpened :${[scholarshipApplicationData].toString()} }';
}

class SubmitApplicationDataEvent extends ScholarshipEvent {
  final String scholarshipModelID;
  final Map<String, dynamic> scholarshipApplicationData;
  const SubmitApplicationDataEvent({@required this.scholarshipModelID, @required this.scholarshipApplicationData});

  @override
  List<Object> get props => [];

  @override
  String toString() => 'SelectEligibilityEvent  ';
}

class ReturnScholarshipPageEvent extends ScholarshipEvent {
  const ReturnScholarshipPageEvent();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'FormValidateForSubmissionEvent { isOpened }';
}
