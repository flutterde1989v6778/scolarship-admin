import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:universal_html/html.dart' as html;

import 'package:scholarship/models/index.dart';
import 'package:scholarship/models/scholarship_application_model.dart';
import 'package:scholarship/providers/index.dart';
import 'package:scholarship/repositories/index.dart';

import './index.dart';

class ScholarshipBloc extends Bloc<ScholarshipEvent, ScholarshipState> {
  final ScholarshipApplicationRepository _scholarshipApplicationRepository = ScholarshipApplicationRepository();
  final ScholarshipRepository _scholarshipRepository = ScholarshipRepository();

  @override
  ScholarshipState get initialState => ScholarshipState.init();

  @override
  Stream<Transition<ScholarshipEvent, ScholarshipState>> transformEvents(events, transitionFn) {
    return super.transformEvents(events, transitionFn);
  }

  @override
  Stream<ScholarshipState> mapEventToState(ScholarshipEvent event) async* {
    if (event is DetailBodyIsOpenedEvent) {
      yield* _mapDetailBodyIsOpenedToState(event.id, event.isOpened);
    } else if (event is SelectDetailTabEvent) {
      yield* _mapSelectedDetialTabToState(event.id, event.tapIndex);
    } else if (event is SelectEligibilityEvent) {
      yield* _mapSelectedEligibilityToState(event.id, event.isOpened);
    } else if (event is LoadingScholarshipListEvent) {
      yield* _mapLoadingScholarshipListEventToState(event.loadingStateForScholarshipList);
    } else if (event is GetScholarshipListStreamEvent) {
      yield* _mapGetScholarshipListStreamEventToState();
    } else if (event is UpdatingApplicationDataEvent) {
      yield* _mapUpdatingApplicationDataEventToState(event.scholarshipID, event.scholarshipApplicationData);
    } else if (event is SubmitApplicationDataEvent) {
      yield* _mapSubmitApplicationDataEventToState(event.scholarshipModelID, event.scholarshipApplicationData);
    } else if (event is ReturnScholarshipPageEvent) {
      yield* _mapReturnScholarshipPageEventToState();
    }
  }

  Stream<ScholarshipState> _mapReturnScholarshipPageEventToState() async* {
    yield state.update(submissionSuccessViewState: false);
  }

  Stream<ScholarshipState> _mapSubmitApplicationDataEventToState(String scholarshipModelID, Map<String, dynamic> scholarshipApplicationData) async* {
    Map<String, dynamic> _scholarshipApplicationData = state.scholarshipApplicationData;
    _scholarshipApplicationData[scholarshipModelID] = scholarshipApplicationData;
    try {
      yield state.update(scholarshipApplicationData: _scholarshipApplicationData);

      scholarshipApplicationData["appplicationModel"].videoUrl =
          await StorageProvider.uploadHtmlFileObject(path: "Videos/", file: scholarshipApplicationData["videoFile"]);
      scholarshipApplicationData["appplicationModel"].letter1Url =
          await StorageProvider.uploadHtmlFileObject(path: "Letters/", file: scholarshipApplicationData["letter1File"]);
      scholarshipApplicationData["appplicationModel"].letter2Url =
          await StorageProvider.uploadHtmlFileObject(path: "Letters/", file: scholarshipApplicationData["letter2File"]);
      scholarshipApplicationData["appplicationModel"].scholarshipID = scholarshipModelID;
      scholarshipApplicationData["appplicationModel"].ts = DateTime.now().millisecondsSinceEpoch;
      ScholarshipApplicationModel newScholarshipApplicationModel = await _scholarshipApplicationRepository.addScholarshipApplication(
        scholarshipApplicationModel: scholarshipApplicationData["appplicationModel"],
      );
      if (newScholarshipApplicationModel != null) {
        _scholarshipApplicationData[scholarshipModelID]["submittingStateForApplication"] = 2;
        _scholarshipApplicationData[scholarshipModelID]["appplicationModel"] = newScholarshipApplicationModel;
        yield state.update(
          scholarshipApplicationData: _scholarshipApplicationData,
          submissionSuccessViewState: true,
        );
      } else {
        _scholarshipApplicationData[scholarshipModelID]["submittingStateForApplication"] = -1;
        yield state.update(scholarshipApplicationData: _scholarshipApplicationData);
      }
    } catch (e) {
      _scholarshipApplicationData[scholarshipModelID]["submittingStateForApplication"] = -1;
      yield state.update(scholarshipApplicationData: _scholarshipApplicationData);
    }
  }

  Stream<ScholarshipState> _mapUpdatingApplicationDataEventToState(String scholarshipID, Map<String, dynamic> scholarshipApplicationData) async* {
    Map<String, dynamic> _scholarshipApplicationData = state.scholarshipApplicationData;
    _scholarshipApplicationData[scholarshipID] = scholarshipApplicationData;
    yield state.update(scholarshipApplicationData: _scholarshipApplicationData);
  }

  Stream<ScholarshipState> _mapLoadingScholarshipListEventToState(int loadingStateForScholarshipList) async* {
    yield state.update(loadingStateForScholarshipList: loadingStateForScholarshipList);
  }

  Stream<ScholarshipState> _mapGetScholarshipListStreamEventToState() async* {
    Stream<List<ScholarshipModel>> scholarshipListStream = _scholarshipRepository.getScholarshipListStream(orderby: [
      {"key": "ts", "desc": true}
    ]);

    if (scholarshipListStream != null) {
      yield state.update(loadingStateForScholarshipList: 2, scholarshipListStream: scholarshipListStream);
    } else {
      yield* _mapLoadingScholarshipListEventToState(-1);
    }
  }

  Stream<ScholarshipState> _mapDetailBodyIsOpenedToState(String id, bool isOpened) async* {
    Map<String, bool> isOpenedDetailBody = state.isOpenedDetailBody;
    Map<String, int> selectDetailTab = state.selectDetailTab;
    Map<String, Map<int, bool>> selectEligibility = state.selectEligibility;

    isOpenedDetailBody[id] = isOpened;

    if (selectDetailTab[id] == null) selectDetailTab[id] = 1;
    if (selectEligibility[id] == null) selectEligibility[id] = {0: false, 1: false, 2: false};

    if (!isOpened) {
      selectDetailTab[id] = 1;
      selectEligibility[id] = {0: false, 1: false, 2: false};
    }

    yield state.update(
      isOpenedDetailBody: isOpenedDetailBody,
      selectDetailTab: selectDetailTab,
      selectEligibility: selectEligibility,
    );
  }

  Stream<ScholarshipState> _mapSelectedDetialTabToState(String id, int tapIndex) async* {
    Map<String, int> selectDetailTab = state.selectDetailTab;
    selectDetailTab[id] = tapIndex;
    yield state.update(
      selectDetailTab: selectDetailTab,
    );
  }

  Stream<ScholarshipState> _mapSelectedEligibilityToState(String id, Map<int, bool> isOpened) async* {
    Map<String, Map<int, bool>> selectEligibility = state.selectEligibility;
    selectEligibility[id] = isOpened;
    yield state.update(
      selectEligibility: selectEligibility,
    );
  }
}
