import 'package:url_launcher/url_launcher.dart';

openURL(url) async {
  if (await canLaunch(url)) {
    await launch(url);
    return "1";
  } else {
    return "-1";
  }
}
