import 'dart:async';

import 'package:flutter/material.dart';

import 'package:scholarship/models/index.dart';
import 'index.dart';

class ScholarshipProvider extends FirebaseProvider {
  final String path = "/Scholarships";
  FirebaseProvider firebaseProvider = FirebaseProvider();

  Future<ScholarshipModel> addScholarship({@required ScholarshipModel scholarshipModel}) async {
    try {
      var result = await firebaseProvider.addDocument(path: path, data: scholarshipModel.toJson());
      if (result != "-1")
        return ScholarshipModel.fromJson(result);
      else
        return null;
    } catch (e) {
      print("addScholarship error");
      print(e);
      return null;
    }
  }

  Future<bool> updateScholarship({@required ScholarshipModel scholarshipModel}) async {
    return await firebaseProvider.updateDocument(path: path, id: scholarshipModel.id, data: scholarshipModel.toJson());
  }

  Future<bool> deleteScholarship({@required ScholarshipModel scholarshipModel}) async {
    return await firebaseProvider.deleteDocument(path: path, id: scholarshipModel.id);
  }

  Future<bool> isScholarshipExist({@required String id}) async {
    return await firebaseProvider.isDocExist(path, id);
  }

  Future<ScholarshipModel> getScholarshipByID({@required String id}) async {
    return ScholarshipModel.fromJson(await firebaseProvider.getDocumentByID(path: path, id: id));
  }

  Stream<ScholarshipModel> getScholarshipStreamByID({@required String id}) {
    return firebaseProvider.getDocumentStreamByID(path: path, id: id).map((data) {
      return ScholarshipModel.fromJson(data);
    });
  }

  Stream<List<ScholarshipModel>> getScholarshipListStream({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) {
    try {
      Stream<List<Map<String, dynamic>>> stream = firebaseProvider.getDocumentsStream(path: path, wheres: wheres, orderby: orderby, limit: limit);
      return stream.map((dataList) {
        return dataList.map((data) {
          return ScholarshipModel.fromJson(data);
        }).toList();
      });
    } catch (e) {
      return null;
    }
  }

  Future<List<ScholarshipModel>> getScholarshipList({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) async {
    List<ScholarshipModel> scholarshipModelList = [];
    var result = await firebaseProvider.getDocumentData(path: path, wheres: wheres, orderby: orderby, limit: limit);
    if (result != "-1") {
      result.forEach((data) {
        scholarshipModelList.add(ScholarshipModel.fromJson(data));
      });
      return scholarshipModelList;
    } else {
      return null;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
  }
}
