import 'dart:async';
import 'package:flutter/material.dart';

import 'package:scholarship/models/index.dart';
import 'index.dart';

class ScholarshipApplicationProvider extends FirebaseProvider {
  final String path = "/Applications";

  FirebaseProvider firebaseProvider = FirebaseProvider();
  ScholarshipProvider scholarshipProvider = ScholarshipProvider();

  Future<ScholarshipApplicationModel> addScholarshipApplication({@required ScholarshipApplicationModel scholarshipApplicationModel}) async {
    try {
      var result = await firebaseProvider.addDocument(path: path, data: scholarshipApplicationModel.toJson());
      if (result != "-1")
        return ScholarshipApplicationModel.fromJson(result);
      else
        return null;
    } catch (e) {
      print("addScholarshipApplication error");
      print(e);
      return null;
    }
  }

  Future<bool> updateScholarshipApplication({@required ScholarshipApplicationModel scholarshipApplicationModel}) async {
    return await firebaseProvider.updateDocument(
      path: path,
      id: scholarshipApplicationModel.id,
      data: scholarshipApplicationModel.toJson(),
    );
  }

  Future<bool> deleteScholarshipApplication({@required ScholarshipApplicationModel scholarshipApplicationModel}) async {
    return await firebaseProvider.deleteDocument(path: path, id: scholarshipApplicationModel.id);
  }

  Stream<List<ScholarshipApplicationModel>> getScholarshipApplicationListStream({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) {
    Stream<List<Map<String, dynamic>>> stream = firebaseProvider.getDocumentsStream(
      path: path,
      wheres: wheres,
      orderby: orderby,
      limit: limit,
    );
    return stream.map((dataList) {
      return dataList.map((data) {
        return ScholarshipApplicationModel.fromJson(data);
      }).toList();
    });
  }

  Future<List<ScholarshipApplicationModel>> getScholarshipApplicationList({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) async {
    List<ScholarshipApplicationModel> scholarshipApplicationModelList = [];
    var result = await firebaseProvider.getDocumentData(
      path: path,
      wheres: wheres,
      orderby: orderby,
      limit: limit,
    );
    if (result != "-1") {
      result.forEach((data) {
        scholarshipApplicationModelList.add(ScholarshipApplicationModel.fromJson(data));
      });
      return scholarshipApplicationModelList;
    } else {
      return null;
    }
  }

  Future<ScholarshipApplicationModel> getScholarshipApplicationByID({@required String id}) async {
    return ScholarshipApplicationModel.fromJson(await firebaseProvider.getDocumentByID(path: path, id: id));
  }

  Stream<ScholarshipApplicationModel> getScholarshipApplicationStreamByID({@required String id}) {
    return firebaseProvider.getDocumentStreamByID(path: path, id: id).map((data) {
      return ScholarshipApplicationModel.fromJson(data);
    });
  }

  Future<bool> isScholarshipApplicationExist({@required String id}) async {
    return await firebaseProvider.isDocExist(path, id);
  }

  Stream<List<Stream<Map<String, Object>>>> getApplicationListWithScholarshipStream({
    @required List<Map<String, dynamic>> wheres,
    @required List<Map<String, dynamic>> orderby,
    @required int limit,
  }) {
    Stream<List<Map<String, dynamic>>> applicationStream = firebaseProvider.getDocumentsStream(
      path: "Applications",
      wheres: wheres,
      orderby: orderby,
      limit: limit,
    );
    int i = 0;
    return applicationStream.map((applicationDataList) {
      return applicationDataList.map((applicationData) {
        i++;
        ScholarshipApplicationModel scholarshipApplicationModel = ScholarshipApplicationModel.fromJson(applicationData);
        int j = 0;
        return scholarshipProvider.getScholarshipStreamByID(id: scholarshipApplicationModel.scholarshipID).map((scholarhipModel) {
          j++;
          return {"scholarshipModel": scholarhipModel, "scholarshipApplicationModel": scholarshipApplicationModel};
        });
      }).toList();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
  }
}
